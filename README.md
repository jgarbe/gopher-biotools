![UMN Gopher](images/goldy.png) Gopher-Biotools
===============

Easy-to-use tools that ease the processing of high-throughput sequence data.

INSTALLATION
------------

Gopher-Biotools is distributed as a collection of Perl scripts compatible with Linux and MacOS. Download the sourcecode of the latest release and uncompress it. Copy the contents of the bin folder to a folder in your path.

**Dependencies**

Scripts that generate plots require R and the R library plotly. Other dependencies:
- fasterqc.pl: GD.pm
- fastq-species-blast.pl: ncbi blast+ 2.2.28 or newer, Fastx-Toolkit
- insertsize.pl: bowtie2, samtools 1.1 or newer, Picard Tools 1.126 or newer

USAGE
-----

Use the "-h" option to get usage information for any Gopher-Biotools script. 

Gopher-Biotools documentation is located in the associated Bitbucket wiki https://bitbucket.org/jgarbe/gopher-biotools/wiki/Home 

ISSUES
------

Use the associated github issue track to report issues: https://github.umn.edu/jgarbe/gopher-biotools/issues

AKNOWLEDGEMENTS
---------------

Gopher-Biotools is developed by the University of Minnesota Genomics Center in collaboration with the University of Minnesota Informatics Institute and the Minnesota Supercomputing Institute. It is released under the terms of the GNU General Public License.

Copyright 2012-2020 Regents of the University of Minnesota

![UMN Logo](images/umnlogo.gif)