#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

picardwgsmetricsplot.pl - Generate plots from Picard CollectWgsMetrics output files

=head1 SYNOPSIS

picardwgsmetricsplot.pl metrics1.txt [metrics2.txt ...]
picardwgsmetricsplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing whole-genome-shotgun sequence metrics stats from multiple samples

Options:
    -f filelist.txt : Provide a file with a list of metrics files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: picardwgsmetricsplot.pl metrics1.txt [metrics2.txt ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

$pe = 1; # this script only works for paire-end data

# Get list of summary files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# run through the summary files, saving stats
$coveragefile = "tmp.coverage.dat";
open OFILE, ">$coveragefile" or die "Cannot open output file $coveragefile: $!\n";
print OFILE "Value\tCount\tSample\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    $line = <IFILE>;
	    chomp $line; # header line
	    @keys = split /\t/, $line;
	    $line = <IFILE>; # data line
	    chomp $line;
	    @values = split /\t/, $line;
	    for $i (0..$#keys) {
		$values[$i] = "" unless (defined($values[$i])); # fill in for empty values
		$data{$ifile}{$keys[$i]} = $values[$i];
	    }
	}
	if ($line =~ /^coverage/) {
#    print "File $files[$i] columns: $line";
	    while (<IFILE>) {
		chomp;
		next unless length;
		my ($size, $count)  = split;
		print OFILE "$size\t$count\t$samples[$i]\n";
	    }
	}
    }
}
close OFILE;

### Print out summary stats and data to a file ###
$ofile = "tmp.dat";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
print "STATS Start\n";
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $sum = 0;
#    foreach $key ("PCT_TARGET_BASES_100X", "PCT_TARGET_BASES_50X", "PCT_TARGET_BASES_40X", "PCT_TARGET_BASES_30X", "PCT_TARGET_BASES_20X", "PCT_TARGET_BASES_10X", "PCT_TARGET_BASES_2X") {
    foreach $key ("PCT_100X", "PCT_90X", "PCT_80X", "PCT_70X", "PCT_60X", "PCT_50X", "PCT_40X", "PCT_30X", "PCT_25X", "PCT_20X", "PCT_15X", "PCT_10X", "PCT_5X") {
	$value = ($data{$ifile}{$key} * 100) - $sum; # scale from fraction to percent
	$sum += $value;
	print OFILE "$sample\t$key\t$value\n";
	print "$sample\t$key\t$value\n";
    }
    print "$sample\tmean_coverage\t$data{$ifile}{MEAN_COVERAGE}\n";
}
print "STATS End\n";
close OFILE;

### Plotting ###
$ofile = "tmp.dat";
$rfile = "tmp.r";
$height = 480;
$width = 480;
$numsamples = $#files;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}

### coverage distribution plot ###
print "Generating picard-coverage-distribution.png\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"picard-coverage-distribution.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data))
#  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"% of all target bases achieving coverage level\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#8c510a\",\"#d8b365\", \"#f6e8c3\", \"#f5f5f5\", \"#c7eae5\", \"#5ab4ac\", \"#01665e\"), name=\"\", breaks=c(\"PCT_TARGET_BASES_100X\", \"PCT_TARGET_BASES_50X\", \"PCT_TARGET_BASES_40X\", \"PCT_TARGET_BASES_30X\", \"PCT_TARGET_BASES_20X\", \"PCT_TARGET_BASES_10X\", \"PCT_TARGET_BASES_2X\"), labels=c(\"100X\", \"50X\", \"40X\", \"30X\", \"20X\", \"10X\", \"2X\")) + scale_y_continuous(limits = c(0, 100));

#  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"% of all target bases achieving coverage level\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(drop=FALSE, values=c(\"PCT_TARGET_BASES_100X\"=\"#8c510a\", \"PCT_TARGET_BASES_50X\"=\"#d8b365\", \"PCT_TARGET_BASES_40X\"=\"#f6e8c3\", \"PCT_TARGET_BASES_30X\"=\"#f5f5f5\", \"PCT_TARGET_BASES_20X\"=\"#c7eae5\", \"PCT_TARGET_BASES_10X\"=\"#5ab4ac\", \"PCT_TARGET_BASES_2X\"=\"#01665e\"), labels=c(\"100X\", \"50X\", \"40X\", \"30X\", \"20X\", \"10X\", \"2X\"), name=\"\") + scale_y_continuous(limits = c(0, 100));

  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"% of all genome bases achieving coverage level\") + theme(legend.title=element_blank()) + scale_fill_manual(drop=FALSE, values=c(\"#4d4d4d\", \"#ef8a62\", \"#fddbc7\", \"#b2182b\", \"#ffffff\", \"#e0e0e0\", \"#999999\", \"#abcdef\", \"#abcdef\", \"#abcdef\", \"#abcdef\", \"#abcdef\", \"#abcdef\"), breaks=c(\"PCT_5X\", \"PCT_10X\", \"PCT_15X\", \"PCT_20X\", \"PCT_25X\", \"PCT_30X\", \"PCT_40X\", \"PCT_50X\", \"PCT_60X\", \"PCT_70X\", \"PCT_80X\", \"PCT_90X\", \"PCT_100X\"), labels=c(\"5X\", \"10X\", \"15X\", \"20X\", \"25X\", \"30X\", \"40X\", \"50X\", \"60X\", \"70X\", \"80X\", \"90X\", \"100X\")) + scale_y_continuous(limits = c(0, 100));


#values=c(\"#b2182b\", \"#ef8a62\", \"#fddbc7\", \"#ffffff\", \"#e0e0e0\", \"#999999\", \"#4d4d4d\"), 


  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");



### Print out  data to a file ###
$ofile = "tmp.dat";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print OFILE "$sample\tmean_coverage\t$data{$ifile}{MEAN_COVERAGE}\n";
}
close OFILE;

### mean-coverage plot ###
print "Generating picard-mean-coverage.png\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"picard-mean-coverage.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value))

  p + geom_bar(stat=\"identity\") + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Mean coverage level\") + theme(legend.title=element_blank()) 

#+ scale_fill_manual(drop=FALSE, values=c(\"#4d4d4d\"), breaks=c(\"meadn_coverage\"), labels=c(\"Mean Coverage\")) + scale_y_continuous(limits = c(0, 100));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### coverage violin plots, based off of insertplot.pl
print "Generating picard-coverage.png\n";
$r_script = "coverageplot.r";
$width = 480;
$numsamples = $#samples + 1;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}
open(Rcmd,">", $r_script) or die "Cannot open $r_script \n\n";
print Rcmd "options(warn=-1)"; # unless ($verbose);
print Rcmd "
  library(\"ggplot2\");
  png(filename=\"picard-coverage.png\",width=$width);
  datat <- read.table(\"$coveragefile\", header=T, colClasses=c(\"Sample\"=\"factor\"));
  p <- ggplot(datat, aes(x=Sample, y=Value, weight=Count, fill=Sample));
  #p + geom_violin(scale=\"count\", adjust=.5) + theme(axis.text.x = element_text(angle = 90, hjust = 1));
#  p + geom_violin(scale=\"count\") + geom_boxplot(width=.2, outlier.size=0) + theme(axis.text.x = element_text(angle = 90, hjust = 1)) + xlab(\"Sample\") + ylab(\"Coverage\") + theme(legend.position=\"none\");
  p + geom_violin(adjust=.1) + theme(axis.text.x = element_text(angle = 90, hjust = 1)) + xlab(\"Sample\") + ylab(\"X Coverage\") + theme(legend.position=\"none\");
  dev.off();
  #eof" . "\n";

close Rcmd;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

exit;

################# end of functional code ##############




### run through stats, calculate more stats ###
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];

    $total_count = $data{$ifile}{"TOTAL_READS"} / 2; 
    $aligned = $data{$ifile}{"PF_READS_ALIGNED"} / 2; 
    $concord_count = $data{$ifile}{"READS_ALIGNED_IN_PAIRS"} / 2;
    $discord_count = $aligned - $concord_count;

    $data{$ifile}{concord_count} = $concord_count;
    $data{$ifile}{concord_pct} = $concord_count / $total_count * 100;	
    $data{$ifile}{discord_count} = $discord_count;
    $data{$ifile}{discord_pct} = $discord_count / $total_count * 100;
    $data{$ifile}{unmapped_count} = $total_count;
    $data{$ifile}{"\%alignedreads"} = $aligned / $total_count * 100;
    $data{$ifile}{total_count} = $total_count;

}

### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print "$sample\t\%alignedreads\t$data{$ifile}{\"\%alignedreads\"}\n";
    print "$sample\t#concordant\t$data{$ifile}{concord_count}\n" if ($pe);
    print "$sample\t\%concordant\t$data{$ifile}{concord_pct}\n" if ($pe);
    print "$sample\t\#totalreads\t$data{$ifile}{total_count}\n" if ($pe);
    
#    print "$sample\t#spike\t$data{$ifile}{spike_count}\n" if ($spike);
#    print "$sample\t\%spike\t$data{$ifile}{spike_pct}\n" if ($spike);
}
print "STATS End\n";

### Plotting ###
$ofile = "tmp.dat";
$rfile = "tmp.r";
my $height = 480;
my $width = 480;
my $numsamples = $#files;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}

### skip single-end read plotting - need to update this to work ###
if (0) { 
### absolute read plot ###
print "Generating alignments.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("smap_count", "mmap_count", "umap_count") {
	print OFILE "$sample-R1\t$data\t$data{$ifile}{L}{$data}\n";
	print OFILE "$sample-R2\t$data\t$data{$ifile}{R}{$data}\n" if ($pe);
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"alignments.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Total aligned reads\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#0000FF\", \"#00008B\", \"#7F7F7F\"), name=\"\", breaks=c(\"smap_count\", \"mmap_count\", \"umap_count\"), labels=c(\"Unique\", \"Multiple\", \"Unmapped\"));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### relative read plot ###
print "Generating alignments-pct.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("smap_pct", "mmap_pct") {
	print OFILE "$sample-R1\t$data\t$data{$ifile}{L}{$data}\n";
	print OFILE "$sample-R2\t$data\t$data{$ifile}{R}{$data}\n" if ($pe);
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"))
  png(filename=\"alignments-pct.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Percent aligned reads\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#0000FF\", \"#00008B\"), name=\"\", breaks=c(\"smap_pct\", \"mmap_pct\"), labels=c(\"Unique\", \"Multiple\")) + coord_cartesian(ylim = c(0, 100));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");
}
#exit unless ($pe); # only paired-end stuff here on out

### relative aliged-pairs plot ###
print "Generating alignments-pairs-pct.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
#    foreach $data ("concord_pct", "discord_pct", "spike_pct") {
    foreach $data ("concord_pct", "discord_pct") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"))
  png(filename=\"alignments-pairs-pct.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Percent aligned pairs\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#00008B\", \"#0000FF\", \"#228B22\"), name=\"\", breaks=c(\"concord_pct\", \"discord_pct\"), labels=c(\"Concordant\", \"Discordant\")) + coord_cartesian(ylim = c(0, 100));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### absolute aliged-pairs plot ###
print "Generating alignments-pairs.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("concord_count", "discord_count", "unmapped_count") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"))
  png(filename=\"alignments-pairs.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Total aligned pairs\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#00008B\", \"#0000FF\", \"#7F7F7F\"), name=\"\", breaks=c(\"concord_count\", \"discord_count\", \"unmapped_count\"), labels=c(\"Concordant\", \"Discordant\", \"Unmapped\"));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

