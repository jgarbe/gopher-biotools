#!/usr/bin/perl -w

#################################
# vcf-merge.pl
# John Garbe
# June 2015
#
# Combine non-overlapping vcf files (each file should contain a different chromosome/scaffold)
#
# 
#
#################################

$usage = "USAGE: vcf-merge.pl [directory] outputfile.vcf\n";
die $usage unless ($#ARGV >= 0);

$dir = "";
if ($#ARGV == 1) {
    $dir = shift @ARGV;
    $dir = "$dir/";
}

@files = <$dir*.vcf>;
$filenum = @files;
die "No vcf file found in $dir\n" if ($filenum == 0);
print "$filenum vcf files found\n";

$ofile = $ARGV[0] // "combined.vcf";

#$first = shift @files;
$first = shift @files;
`cp "$first" $ofile`;

$datastart = `awk '/^#CHROM/ {print FNR}' "$first"`;
chomp $datastart;
$datastart++;

if (0) {
#new method (faster??)
$filecount = 0;
foreach $file (@files) {
    $filecount++;
    if ($filecount % 1000 == 0) {
	$date = `date`;
	chomp $date;
	print "$date $filecount files processed\n"; 
    }
    $command = "sed -n '$datastart,\$p' $file >> $ofile";
#    print "Command: $command\n";
    `$command`;

}

    exit;
}
# old method - turns out this is faster
open OFILE, ">>$ofile" or die "Cannot open file $ofile for writing: $!\n";
$filecount = 0;
foreach $file (@files) {
    next if ($file eq $ofile);
    $filecount++;
#    last if ($filecount > 5000); # why is this here?
    if ($filecount % 1000 == 0) {
	$date = `date`;
	chomp $date;
	print "$date $filecount files processed\n"; 
    }
    open IFILE, $file or die "cannot open vcf file $file: $!\n";
    $datastart = `awk '/^#CHROM/ {print FNR}' "$file"`;
    chomp $datastart;
    $datastart++;
    for my $i (1..$datastart-1) {
	$line = <IFILE>;
    }
#    while ($line = <IFILE>) {
#	last if ($line =~ /^#CHROM/);
#    }
    while ($line = <IFILE>) {
	print OFILE $line;
    }
}
