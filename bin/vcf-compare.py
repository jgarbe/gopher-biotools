#!/usr/bin/env python3

#Created by: Lindsay Guare
#Lasted Edited: 7/31/18 (LAG)
import subprocess as sub
import argparse as ap
import numpy as np
import gzip as gzip
import collections as coll
import os as os
import sys

#checks both files for overlapping sample IDs, returns list of matching ones
def check_samp_num(file1, file2):
    with open(file1, 'r') as f:
        line = next(f, False)
        while line != False and line[:3].lower() != "#ch":
            line = next(f, False)
        f1 = line
    with open(file2, 'r') as f:
        line = next(f, False)
        while line != False and line[:3].lower() != "#ch":
            line = next(f, False)
        f2 = line
    f1s = f1[1:].split()[9:]
    f2s = f2[1:].split()[9:]
    intersection = list(set(f1s) & set(f2s))
    sampNum = len(intersection)
    if sampNum == 0:
        print("\nNo common samples found between VCF Files")
        print("File 1: " + str(f1s))
        print("File 2: " + str(f2s))
        sys.exit()
    elif sampNum == 1:
        print("\n1 common sample found between VCF Files\n")
    else:
        print("\n" + str(sampNum) + " common samples found between VCF Files\n")
    return intersection

#compiles and returns list of marker locations in tuples:
#(chromosome, coordinate)
def make_list_from_file(filename, sampcol):
    arr = np.genfromtxt(filename, dtype = 'str', usecols = (0, 1))
    locs = []
    for row in arr:
        locs.append(tuple(row))
    return locs

#compiles and returns list of markers and the corresponding genotype for that sample in tuples:
#(chromosome, coordinate, genotype call)
def make_type_list_from_file(filename, sampcol):
    arr = np.genfromtxt(filename, dtype = 'str', usecols = (0, 1, sampcol))
    locs = []
    for row in arr:
        locs.append(tuple(row))
    return locs

#compiles and returns list of markers and the corresponding reference and alternate calls
#(chromosome, coordinate, refseq, altseq)
def make_seq_list_from_file(filename):
    arr = np.genfromtxt(filename, dtype = 'str', usecols = (0, 1, 3, 4))
    locs = []
    for row in arr:
        locs.append(tuple(row))
    return locs

#converts genotypes to one of five general types:
#1) ./. - no call
#2) 1/1 - homozygous some variant
#3) 0/0 - homozygous reference
#4) 0/1 - heterozygous reference/some variant
#5) 1/2 - heterozygous some variant/some other variant
def scrub_gtype(m):
    t1 = m[0]
    t2 = m[-1]
    if t1 == "." or t2 == ".":
        return "./."
    elif t1 == t2 and t1 != "0":
        return "1/1"
    elif t1 == t2 and t1 == "0":
        return "0/0"
    elif t1 != t2 and (t1 == "0" or t2 == "0"):
        return "0/1"
    else:
        return "1/2"

#replaces genotypes with the reference and alternative bases
#t is the genotype
#s is a tuple of the reference and alternative sequences
def build_seq(s, t):
    seq = t
    alts = s[1].split(',')
    seq = seq.replace("0", s[0])
    seq = seq.replace("1", alts[0])
    if seq.find("2") >= 0:
        seq = seq.replace("2", alts[1])
    if seq.find("3") >= 0:
        seq = seq.replace("3", alts[2])
    if seq.find("4") >= 0:
        seq = seq.replace("4", alts[3])
    return seq

#parses arguments and returns object with arguments as attributes
def do_parser_stuff():
    parser = ap.ArgumentParser(description = '''
    Compares the contents of two vcf files.

    Output files contain numerical and percentage comparisons in the following categories:
    1) All call type combinations
    2) Marker Location: File 1, File 2, and the Intersection
    3) Genotype Agreement: The intersection is broken down into genotype calls (on a per-sample basis)
        ./.-./., 0/0-0/0, 0/1-0/1, 1/1-1/1, and OTHER
    4) Sequence Agreement: Each matching genotype call from number two (2) is broken down into matching and non-matching categories
    ''')

    parser.add_argument('--file1', action = 'store', dest = 'f1', required = True, type = str)
    parser.add_argument('--name1', action = 'store', dest = 'n1', default = '1')
    parser.add_argument('--file2', action = 'store', dest = 'f2', required = True, type = str)
    parser.add_argument('--name2', action = 'store', dest = 'n2', default = '2')
    parser.add_argument('--runname', help = 'another category which can be added to the plotting output file in order to distinguish between multiple comparisons when files are added together', action = 'store', dest = 'rn', default = "")
    parser.add_argument('--ctminval', help = 'minimum percentage value required for a call type to be displayed as its own category. If the threshold is not met, that category falls into the \'other\' category. Default = 1 percent', action = 'store', type = float, default = '1.0')

    args = parser.parse_args()
    return args

args = do_parser_stuff()

samples = check_samp_num(args.f1, args.f2)

files = [args.f1, args.f2]
samp_type_lists = coll.defaultdict(list) #dictionary of 2D lists, keyed by sample
samp_seq_lists = coll.defaultdict(list) #dictionary of 2D lists, keyed by sample
for filename in files:
    for i in range(len(samples)):
        tempList1 = make_type_list_from_file(filename, i + 9)
        samp_type_lists[samples[i]].append(tempList1)
        tempList2 = make_seq_list_from_file(filename)
        samp_seq_lists[samples[i]].append(tempList2)

samp_tdicts = coll.defaultdict() #sample-keyed dictionary of marker-location-keyed dictionaries of strings representing genotypes
samp_sdicts = coll.defaultdict() #sample-keyed dictionary of marker-location-keyed dictionaries of strings representing sequence calls
for sample in samples:
    type_lists = samp_type_lists[sample] #2D list
    type_dicts = [coll.defaultdict(str) for _ in range(2)]
    seq_lists = samp_seq_lists[sample]
    seq_dicts = [coll.defaultdict(str) for _ in range(2)]
    for i in range(2):
        for j in range(len(type_lists[i])):
            type = type_lists[i][j][2].split(":")[0]
            seq = build_seq(seq_lists[i][j][2:4], type)
            seq_dicts[i][tuple(seq_lists[i][j][:2])] = seq
            type = scrub_gtype(type)
            type_dicts[i][tuple(type_lists[i][j][:2])] = type
    samp_tdicts[sample] = type_dicts
    samp_sdicts[sample] = seq_dicts

union = list(set(seq_dicts[0].keys()) | set(seq_dicts[1].keys())) #all of the marker locations
all_u = len(union) * len(samples) #total used for percentage
intersection = list(set(seq_dicts[0].keys()) & set(seq_dicts[1].keys())) #intersecting marker locations
all_i = len(intersection) * len(samples) #total used for percentage

type_totals = coll.defaultdict(int)
venn_totals = coll.defaultdict(int)
seq_totals = coll.defaultdict(int)

ignore = ["./.-./.", "./.-non", "./.-0/0", "non-./.", "non-0/0", "0/0-./.", "0/0-non"]
gtypeAgree = ["0/0-0/0", "0/1-0/1", "1/1-1/1", "1/2-1/2"]

for marker in union: #number of all markers
    for sample in samples: #number of samples
        type_dicts = samp_tdicts[sample]
        types = []
        seq_dicts = samp_sdicts[sample]
        for i in range(2): #number of files
            if marker not in type_dicts[i]:
                type_dicts[i][tuple(marker)] = "non"
            types.append(type_dicts[i][tuple(marker)])
        typeStr = ""
        for type in types:
            typeStr += type + "-"
        typeStr1 = typeStr[:-1]
        type_totals[typeStr1] += 1
        if typeStr1 in gtypeAgree:
            #print(seq_dicts[0][tuple(marker)] + "-" + seq_dicts[1][tuple(marker)])
            if seq_dicts[0][tuple(marker)] == seq_dicts[1][tuple(marker)]:
                seq_totals[typeStr1 + "-match"] += 1
            else:
                seq_totals[typeStr1 + "-mismatch"] += 1

ct = []
igCt = 0
gtAg = 0

for type in ignore: #ignores results that are mostly meaningless, if those data are wanted, then modify the list 'ignore'
    igCt += type_totals[type]
for type in gtypeAgree:
    gtAg += type_totals[type]

other = 0
for k, v in type_totals.items(): #compiles call type info
    if k not in ignore:
        if (100 * (v / (all_u - igCt))) > args.ctminval:
            ct.append((k, str(v), str(100 * (v / (all_u - igCt)))[:5]))
        else:
            other += v
    if k[:3] == "non":
        venn_totals["file2"] += v
    elif k[-3:] == "non":
        venn_totals["file1"] += v

ct.append(("other", str(other), str(100 * (other / (all_u - igCt)))[:5]))
ct.sort()

call_types = open('call_types.txt', 'w')

for type in ct:
    for item in list(type):
        call_types.write(item + "\t")
    call_types.write("\n")

call_types.close()

file = open("VCF_compare_summary.txt", 'w') #more readable
plot_file = open("VCF_compare_plot.txt", 'w') #more plottable
plot_file.write("comparison\tgroup\tnumber\tpercent\t")
if args.rn != "":
    plot_file.write("runname\n")
else:
    plot_file.write("\n")

file.write("#call type breakdown\n\n")
file.write("#ctype\tnum\tperc\n")
for type in ct:
    plot_file.write("call_types\t")
    for item in list(type):
        file.write(item + "\t")
        plot_file.write(item + "\t")
    plot_file.write(args.rn + "\n")
    file.write("\n")

file.write("\n\n")
file.write("#variant location agreement breakdown\n\n")
file.write("#file\tnum\tperc\n")
vennSum = (venn_totals["file1"] / len(samples)) + (venn_totals["file2"] / len(samples)) + len(intersection)
for k, v in venn_totals.items():
    file.write(k + "\t")
    file.write(str(int(v / len(samples))) + "\t")
    file.write(str(100 * (v / (len(samples) * vennSum)))[:5] + "\n")
    plot_file.write("marker_location\t")
    plot_file.write(k + "\t")
    plot_file.write(str(int(v / len(samples))) + "\t")
    plot_file.write(str(100 * (v / (len(samples) * vennSum)))[:5] + "\t")
    plot_file.write(args.rn + "\n")
file.write("overlap\t")
file.write(str(len(intersection)) + "\t")
file.write(str(100 * (len(intersection) / vennSum))[:5] + "\n")
plot_file.write("marker_location\t")
plot_file.write("overlap\t")
plot_file.write(str(len(intersection)) + "\t")
plot_file.write(str(100 * (len(intersection) / vennSum))[:5] + "\t")
plot_file.write(args.rn + "\n")

file.write("\n\n")
file.write("#genotype agreement breakdown of markers for which the location agrees\n\n")
file.write("#gtype\tnum\tperc\n")
for type in gtypeAgree:
    file.write(type + "\t")
    file.write(str(type_totals[type]) + "\t")
    file.write(str(100 * (type_totals[type] / all_i))[:5] + "\n")
    plot_file.write("genotype_agreement\t")
    plot_file.write(type + "\t")
    plot_file.write(str(type_totals[type]) + "\t")
    plot_file.write(str(100 * (type_totals[type] / all_i))[:5] + "\t")
    plot_file.write(args.rn + "\n")
file.write("other\t")
file.write(str(all_i - gtAg) + "\t")
file.write(str(100 * ((all_i - gtAg) / all_i))[:5] + "\n")
plot_file.write("genotype_agreement\t")
plot_file.write("other\t")
plot_file.write(str(all_i - gtAg) + "\t")
plot_file.write(str(100 * ((all_i - gtAg) / all_i))[:5] + "\t")
plot_file.write(args.rn + "\n")
file.write("total\t")
file.write(str(all_i) + "\t")
file.write(str(100 * ((all_i) / all_i))[:5] + "\n")

file.write("\n\n")
file.write("#base sequence agreement breakdown of markers for which the genotype agrees\n\n")
file.write("#gtype\tnum-match\tperc-match\tnum-mismatch\tperc-mismatch\n")
for type in gtypeAgree:
    file.write(type + "\t")
    file.write(str(seq_totals[type + "-match"]) + "\t\t")
    file.write(str(seq_totals[type + "-match"] / (0.01 * type_totals[type]))[:5] + "\t\t")
    file.write(str(seq_totals[type + "-mismatch"]) + "\t\t")
    file.write(str(seq_totals[type + "-mismatch"] / (0.01 * type_totals[type]))[:5] + "\n")

for i in range(len(gtypeAgree)):
    plot_file.write("seq_-" + gtypeAgree[i] + "\t")
    plot_file.write("match\t")
    plot_file.write(str(seq_totals[gtypeAgree[i] + "-match"]) + "\t")
    plot_file.write(str(seq_totals[gtypeAgree[i] + "-match"] / (0.01 * type_totals[gtypeAgree[i]]))[:5] + "\t")
    plot_file.write(args.rn + "\n")
    plot_file.write("seq_-" + gtypeAgree[i] + "\t")
    plot_file.write("mismatch\t")
    plot_file.write(str(seq_totals[gtypeAgree[i] + "-mismatch"]) + "\t")
    plot_file.write(str(seq_totals[gtypeAgree[i] + "-mismatch"] / (0.01 * type_totals[gtypeAgree[i]]))[:5] + "\t")
    plot_file.write(args.rn + "\n")

file.write("\n\n")

file.close()
plot_file.close()
