#!/usr/bin/perl -w

#####################################
# sequel-demultiplex.pl
# John Garbe
# November 2019 - Happy Thanksgiving
#####################################

=head1 DESCRIPTION

sequel-demultiplex.pl - Demultiplex a sequel run, naming output by sample name

=head1 SYNOPSIS

sequel-demultiplex.pl --subreadbam input.bam --samplesheet barcodes.txt [--prefix sequel-demultiplex]

=head1 OPTIONS

Options:

 --help : Print usage instructions and exit
 --verbose : Print more information while running
 --subreadbam file : subread bam file from sequel run to demultiplex
 --samplesheet file : list of sample names and barcode IDs
 --prefix string : prefix for output files
 --bams : generate bams (this is always on)
 --fasta : generate fasta files
 --fastq : generate fastq files
 --ccs : generate circular-consensus-sequences
 --single : single indexed sequel run (default)
 --dual : dual indexed sequel run
 --barcodefasta file : fasta file of barcode sequences

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{threads} = 10;
$args{prefix} = "sequel-demultiplex";
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "subreadbam=s" => \$args{subreadbam},
	   "barcodefasta=s" => \$args{barcodefasta},
	   "samplesheet=s" => \$args{samplesheet},
	   "windowsize=i" => \$args{windowsize},
	   "prefix=s" => \$args{prefix},
	   "bams" => \$args{bams},
	   "fasta" => \$args{fasta},
	   "fastq" => \$args{fastq},
	   "ccs" => \$args{ccs},
	   "single" => \$args{single},
	   "dual" => \$args{dual},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
$args{subreadbam} = abs_path($args{subreadbam}) if ($args{subreadbam});

### Handle Parameters ###

die "--subreadbam is required\n" unless ($args{subreadbam}); 

if ($args{samplesheet}) {
    $args{barcodefasta} = "/home/umgc/public/bin/smrtlink/barcodes/Sequel_RSII_384_barcodes_v1.fasta" unless ($args{barcodefasta});
} elsif ($args{barcodefasta}) {
    # good
} else {
    die "--samplesheet or --barcodefasta is required\n";
}

# read in barcodefasta
open IFILE, $args{barcodefasta} or die "cannot open barcodefasta $args{barcodefasta}: $!\n";
while (my $id = <IFILE>) {
    $id =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
    chomp $id;
    $id =~ s/^>//;
    $seq = <IFILE>;
    $seq =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
    chomp $seq;
    $sanid = sanitize($id);
    $origid{$sanid} = $id;
    $refbarcodes{$sanid} = $seq;
}

# read in samplesheet, write out barcodefasta with sample barcodes
if ($args{samplesheet}) {
    open IFILE, $args{samplesheet} or die "cannot open samplesheet $args{samplesheet}: $!\n";
    $mybarcodefasta = "$args{prefix}.barcodes.fasta";
    open OFILE, ">$mybarcodefasta" or die "cannot open $mybarcodefasta: $!\n";
    while ($line = <IFILE>) {
	$line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
	chomp $line;
	next if ($line eq "");
	if ($args{dual}) {
	    ($sample, $bc1, $bc2) = split ' ', $line;
	    $bc1 = sanitize($bc1);
	    $bc2 = sanitize($bc2);
	    $barcodes{$bc1}{$bc2} = $sample;
	    push @samples, $sample;
	    die "Unknown barcode id for sample $sample: $bc1\n" if (!defined($refbarcodes{$bc1}));
	    die "Unknown barcode id for sample $sample: $bc2\n" if (!defined($refbarcodes{$bc2}));
	    if (!defined($seenbc{$bc1})) {
		print OFILE ">$bc1\n$refbarcodes{$bc1}\n";
	    }
	    if (!defined($seenbc{$bc2})) {
		print OFILE ">$bc2\n$refbarcodes{$bc2}\n";
	    }
	    $seenbc{$bc1} = 1;
	    $seenbc{$bc2} = 1;
	} else {
	    ($sample, $bc1) = split ' ', $line;
	    $bc1 = sanitize($bc1);
	    $barcodes{$bc1}{$bc1} = $sample;
	    push @samples, $sample;
	    die "Unknown barcode id for sample $sample: '$bc1'\n" if (!defined($refbarcodes{$bc1}));
	    if (!defined($seenbc{$bc1})) {
		print OFILE ">$bc1\n$refbarcodes{$bc1}\n";
	    }
	    $seenbc{$bc1} = 1;
	}
    }
} else {
    $mybarcodefasta = $args{barcodefasta};
    # assume a sample for each barcode
    foreach $id (keys %refbarcodes) {
	$barcodes{$id}{$id} = $origid{$id};
	print "sample: $origid bcid = $id\n"; # DEBUG
    }
}

# Run lima to demultiplex
$windowsize = ($args{windowsize}) ? "-W $args{windowsize}" : "";
$type = "--same"; # default is same #  if ($args{single});
$type = "--different" if ($args{dual}) ;

$command = "module load smrtlink; lima $type $windowsize --split-bam --split-bam-named --num-threads $args{threads} $args{subreadbam} $mybarcodefasta $args{prefix}.bam";
print "$command\n";
$result = `$command`;
print $result;
die "smrtlink lima failure\n" if ($?);

# rename bam files by sample
@bams = `ls $args{prefix}*.bam`;
foreach $bam (@bams) {
    chomp $bam;
#    if ($args{dual}) { # dual indexed
	if ($bam =~ /$args{prefix}\.(.+)--(.+)\.bam/) {
	    $bc1 = sanitize($1);
	    $bc2 = sanitize($2);
	    next if (!defined($barcodes{$bc1}{$bc2}));
	    $seen{$barcodes{$bc1}{$bc2}} = 1;
	    $sample = $barcodes{$bc1}{$bc2};
	    $newbam = "$args{prefix}.$sample.bam";
	    $newbams{$sample} = $newbam;
	    `mv $bam $newbam`;
	    $bampbi = "$bam.pbi";
	    $newbampbi = "$newbam.pbi";
	    `mv $bampbi $newbampbi`;
	} else {
	    print STDERR "Cannot parse bam file $bam while trying to rename bams by sample\n";
	}
	
 #   } else { # single indexed
#	if ($bam =~ /$args{prefix}\.(.+)\.bam/) {
#	    $bc1 = $1;
#	    next if (!defined($barcodes{$bc1}));
#	    $seen{$barcodes{$bc1}} = 1;
#	    $newbam = "$args{prefix}.$barcodes{$bc1}.bam";
#	    $newbams{$sample} = $newbam;
#	    `mv $bam $newbam`;
#	    $bampbi = "$bam.pbi";
#	    $newbampbi = "$newbam.pbi";
#	    `mv $bampbi $newbampbi`;
#	} else {
#	    print STDERR "Cannot parse bam file $bam\n";
#	}
#    }
}
# check that all samples were seen
foreach $sample (@samples) {
    if (!defined($seen{$sample})) {
	print STDERR "WARNING: Sample $sample not seen\n";
    }
}

# generate fasta, fastq, and ccs if requested
%ccsmetrics = ();
# just run 2 jobs at a time, ccs will take all available processors
open GP, "| parallel -j 2 > parallel.out" or die "Cannot open pipe to gnu parallel\n";
foreach $sample (keys %newbams) {
#    print "$sample\n";
    if ($args{fasta}) {
	print GP "module load smrtlink; bam2fasta --output $args{prefix}.$sample $newbams{$sample}\n";
    }
    if ($args{fastq}) {
	print GP "module load smrtlink; bam2fastq --output $args{prefix}.$sample $newbams{$sample}\n";
    }
    if ($args{ccs}) {
	$command = "module load smrtlink; ccs --num-threads $args{threads} --skip-polish --min-passes 0 $newbams{$sample} $args{prefix}.$sample.ccs.bam; ";
	if ($args{fasta}) {
	    $command .= "module load smrtlink; bam2fasta --output $args{prefix}.$sample.ccs sequel-demultiplex.$sample.ccs.bam; ";
	}
	if ($args{fastq}) {
	    $command .= "module load smrtlink; bam2fastq --output $args{prefix}.$sample.ccs sequel-demultiplex.$sample.ccs.bam; ";
	}
	print GP "$command\n";
    }
}
close GP;

# get some ccs metrics
if ($args{ccs} && $args{fasta}) {
    foreach $sample (keys %newbams) {
	@results = `ccs-summary.pl $args{prefix}.$sample.ccs.fasta.gz`;
	foreach $result (@results) {
	    chomp $result;
	    ($value, $metric) = split ' ', $result;
	    $metrics{$sample}{$metric} = $value;
	    $ccsmetrics{$metric} = 1;
	}
    }
}

# re-write lima.counts file with sample names instead of barcode ids
$ifile = "$args{prefix}.lima.counts";
open IFILE, $ifile or die "Cannot open $ifile: $!\n";
$ofile = "$args{prefix}.log";
open OFILE, ">$ofile" or die "Cannot open $ofile: $!\n";
my $header = <IFILE>;
print OFILE "Sample\tCount";
foreach $metric (sort keys %ccsmetrics) {
    print OFILE "\t$metric";
}
print OFILE "\n";
while ($line = <IFILE>) {
    my ($index, $index2, $bc1, $bc2, $count, $score) = split /\t/, $line;
    print "$bc1, $bc2\n"; # DEBUG
    $sample = $barcodes{sanitize($bc1)}{sanitize($bc2)};
    print OFILE "$sample\t$count";
    foreach $metric (sort keys %ccsmetrics) {
	$value = $metrics{$sample}{$metric} // "";
	print OFILE "\t$value";
    }
    print OFILE "\n";
}
print OFILE "\n";
close OFILE;
`cat $args{prefix}.lima.summary >> $ofile`;

# cleanup
@files = ("$args{prefix}.json");
foreach $file (@files) {
    `rm $file` if (-e $file);
}
`rm $args{prefix}.*.subreadset.xml`;
exit;




################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev {
    my($data) = @_;
    if(@$data == 1) {
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}

# sanitize a barcode name
sub sanitize {
    my $id = shift @_;
    $id = lc($id);
    $id =~ s/-/_/g;
    return $id;
}
