#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014-2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

expressiontablepca.pl - Given a table of expression data, generate pca plots and heatmaps using the DESeq2 R packagea and the "Emperor" PCA visualization tool

=head1 SYNOPSIS

expressiontablepca.pl data.txt mappingfile.txt

=head1 DESCRIPTION

Generate a series of plots summarizing a table of expression data. The input data file should be tab delimited with a header. There should be a row for each feature (gene, transcript, exon, etc), and a column for each sample. The first row should contain sample names and the first column feature IDs. Mapping file requirements: the first column is named "#SampleID" and contains the sample names, a column named "Group" must be present, and at least one additional column must be present. 

Options:
    -n : Number of top genes to use for clustering
    -h : Display usage information
    -v : Verbose output

DESeq2 data structures are save in the file DESeq2-data.RDATA
To use this data start R, install DESeq2:
 source("http://bioconductor.org/biocLite.R")
 biocLite("DESeq2")
And load the data:
 load("DESeq2-data.RDATA")

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_v, $opt_n);

$usage = "USAGE: expressiontablepca.pl data.txt mappingfile.txt\n";

die $usage unless getopts('hvn:');
pod2usage(q(-verbose) => 3) if ($opt_h);
die $usage unless ($#ARGV == 1);

# get the input arguments
my $expressionfile = $ARGV[0];
my $mappingfile = $ARGV[1];
my $topgenes = $opt_n // 500;
my $heatmapgenes = $opt_n // 50;
my $mdsplotfile = "mdsplot.png";
my $dendroplotfile = "dendrogramplot.png";
my $heatmapplotfile = "heatmapplot.png";

die "Cannot find file $expressionfile" unless (-e $expressionfile);

### PCA Plots ###
print "Generating PCA plots\n";
my $r_script = "expressiontablepca.r";
my $height = 480;
my $width = 480;
my $numsamples = `wc -l < $mappingfile`;;
$numsamples--;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}
$dendrowidth = $width / 2;

open RFILE, ">$r_script" or die "Cannot open $r_script\n";
print RFILE << "EOF";
library("DESeq2")
library("ggplot2")
library("RColorBrewer")
library("gplots")

# Read in data
data <- read.table("$expressionfile", header=TRUE, row.names=1, check.names=FALSE);
data <- data[,order(colnames(data))]
# Round (for fpkm values)
data <- round(data)
    
if (file.access("$mappingfile", mode=4) == 0) {
 write("Pulling metadata from mapping file", stderr())
 # Create metadata from mapping file, put in same order as data
 meta <- read.table("$mappingfile", header=TRUE, row.names=1, comment.char = "", sep="\\t", colClasses=c("X.SampleID"="factor"))
 meta <- meta[order(rownames(meta)),]
 meta\$X <- NULL
} else {
 write("Creating fake metadata", stderr())
 # Create (fake) metadata and column names
 (Group <- factor(c(rep(c("dummy1","dummy2"),ncol(data)/2))))
 (meta <- data.frame(row.names=colnames(data), Group))
}

# Create DESeq2 data type, transform
dds <- DESeqDataSetFromMatrix(countData = data, colData = meta, design = ~ Group)
rld <- rlogTransformation(dds)

# calculate the variance for each gene
rv <- genefilter::rowVars(assay(rld))

# select the ntop genes by variance
select <- order(rv, decreasing=TRUE)[seq_len(min($topgenes, length(rv)))]

# perform a PCA on the data for the selected genes
pca <- prcomp(t(assay(rld)[select,]))

# Eigenvalues
ev <- t(pca\$sdev^2);
row.names(ev) <- c("eigvals");
# Proportion of variance
pv <- t(pca\$sdev^2/sum(pca\$sdev^2) * 100);
row.names(pv) <- c(\"% variation explained\");
# Write to file
write.table(pca\$x, "emperor.dat", sep="\t");
cat(\"\\n\\n\", file = "emperor.dat", append = TRUE) 
write.table(ev, "emperor.dat", sep="\t", append=TRUE, col.names=FALSE);
write.table(pv, "emperor.dat", sep="\t", append=TRUE, col.names=FALSE);

# Save session to file
save.image("DESeq2-session.RDATA")
save(data, meta, file = "DESeq2-data.RDATA")

# Create MDS plot
cmd <- cmdscale(dist(t(assay(rld)[select,])));
cmd2 <- data.frame(cmd);
cmd3 <- cbind(Sample=rownames(cmd2), cmd2);
colnames(cmd3) <- c("Sample", "Xvar", "Yvar");
png(filename="$mdsplotfile"); 
#  ggplot(data=cmd3, aes(x=Xvar, y=Yvar, color=Sample, label=Sample)) + geom_point(shape=1) + geom_text(size=3);
  ggplot(data=cmd3, aes(x=Xvar, y=Yvar, color=Sample, label=Sample)) + geom_text(size=3) + xlab("Dimension 1") + ylab("Dimension 2") + theme(legend.position="none");
  dev.off();

# Create Dendrogram
  png(filename=\"$dendroplotfile\", height = $height, width  = $dendrowidth); 
  hc = hclust(dist(t(assay(rld)[select,])))
  plot(hc, hang = -1, xlab= "Samples", ylab= "Height", sub=""); 
  dev.off();

# Create 2D clustered heatmap of all samples and top genes
png(filename=\"$heatmapplotfile\", height = $height * 1.5 * ($heatmapgenes / 50), width  = $dendrowidth+100); 
topVarGenes <- head(order(-genefilter::rowVars(assay(rld))),$heatmapgenes)
colors <- colorRampPalette( rev(brewer.pal(9, "PuOr")) )(255)
mat <- assay(rld)[ topVarGenes, ]
mat <- mat - rowMeans(mat)
heatmap.2(mat, trace="none", col=colors, mar=c(10,10), scale="row")

EOF
    ;

close RFILE;
system("R --no-restore --no-save --no-readline < $r_script &> $r_script.out");

# Run emperor
$command = 'sed -i \'s/"//g\' emperor.dat'; 
#print "Running command: $command\n";
$result = `$command`;
print "$result";
`echo -n -e "pc vector number\t" > emperor.txt`;
`cat emperor.dat >> emperor.txt`;
print "Running Emperor\n";
# Check that enough variation is present to run emperor
$line = `grep variation emperor.txt`;
chomp $line;
my ($text, $v1, $v2, $v3) = split /\t/, $line;
if ($v3 < 0.5) {
    print "Emperor cannot be run because the variation explained is less than 5% in one of the first three components: PC1: $v1 PC2: $v2, PC3: $v3\n";
    exit;
}

$command = "module load qiime/1.8.0; make_emperor.py -i emperor.txt -m $mappingfile";
print "Running command: $command\n";
$result = `$command`;
print $result;

exit;


