#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

insertplot.pl - generate a fragment-length plot from Picard output

=head1 SYNOPSIS

insertplot.pl insert_summary1.txt [insert_summary2.txt ...]
insertplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a plot summarizing multiple Picard-tools insert-size-metrics output files. R is required, as well as the R package ggplot2. 

Options:
    -f filelist.txt : provide a file with a list of picard insert-size-metrics output files, one per line. A second tab-delimited column may be included containing sample names
    -h : Print usage instructions and exit
    -v : Print more information whie running (verbose)

=head1 EXAMPLE

Generate a plot from six different picard output files::

    $ cd /home/msistaff/public/garbe/sampledata/RNAseq/Hsap/analysis
    $ insertplot.pl heart.1/insertmetrics.txt heart.2/insertmetrics.txt heart.3/insertmetrics.txt skeletal.1/insertmetrics.txt heart.2/insertmetrics.txt heart.3/insertmetrics.txt

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_v, $opt_t);

$usage = "USAGE: insertplot.pl insert_summary.txt [insert_summary2.txt ...]\n";
die $usage unless ($#ARGV >= 0);
die $usage unless getopts('f:hvt:');
pod2usage(q(-verbose) => 3) if ($opt_h);
$verbose = $opt_v // 0;

# Get list of insert files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	push @files, $line[0];
	push @samples, $line[1] || $line[0];
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

if ($verbose) {
    print "File\tSample\n";
    for $i (0..$#files) { 
	print "$files[$i] $samples[$i]\n";
    }
}
#print "@files\n";
#print "@samples\n";

 # run through the insert metric files, saving stats
print "STATS Start\n";
for $i (0..$#files) {
    if (not -e $files[$i]) { # skip files that don't exist
	print STDERR "File $files[$i] not found, skipping\n";
	next;
    }
    open IFILE, "$files[$i]" or die "Cannot open input file $files[$i] $!\n";

    $sample = $samples[$i];
    
    # skip header
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    if ($line =~ /picard.analysis.InsertSizeMetrics/) {
		$header = <IFILE>;
		chomp $header;
		@header = split /\t/, $header;
		$reads = 0;
		while ($line = <IFILE>) {
		    chomp $line;
		    last if ($line =~ /^## /);
		    next if ($line =~ /^\s*$/); # skip blank lines
		    @line = split /\t/, $line;
		    $thesereads = $line[7];
		    if ($thesereads > $reads) {
			$mean = $line[5];
			$stdev = $line[6];
		    }
		}
		$mean{$sample} = $mean;
		$stdev{$sample} = $stdev;
	    }
	}

	last if ($line =~ /^insert_size/);
    }
#    print "File $files[$i] columns: $line";
    $total = 0;
    while (<IFILE>) {
	chomp;
	next unless length;
	# some datasets have mulitple columns: sum them
	my (@line)  = split;
	$size = shift @line;
	$count = shift @line;
	while (@line) {
	    $count += shift @line;
	}
	$total += $count;
	$data{$sample}{$size} = $count;
    }

    $median{$sample} = &median(\%{$data{$sample}}, $total);
#    $stdev{$sample} = &stdev(\%{$data{$sample}}, $median{$sample});
    $total{$sample} = $total;
    print "$sample\tmeanfragmentsize\t", $mean{$sample}, "\n";
    print "$sample\tstdevfragmentsize\t", $stdev{$sample}, "\n";
}
print "STATS End\n";


# generate insert plot
$name = "insertplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating insert plot\n" if ($args->{verbose});
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# generate order for report sorting
@order = sort {$median{$a} <=> $median{$b}} keys %$median;
$order = "";
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tInsert size\t$order\n";

foreach $sample (keys %data) {
    $scale = $total{$sample} / 20000; # scale data down to 20k reads
    foreach $size (sort keys %{$data{$sample}}) {
	$reps = int($data{$sample}{$size} / $scale);
	for (1..$reps) {
#       for (1..$data{$sample}{$size}) { # this would bypass scaling
	    push @{$fulldata{$sample}}, $size;
	}
    }
}


$maxlength = 0;
foreach $sample (keys %data) {
    $length = $#{$fulldata{$sample}};
    $maxlength = $length if ($length > $maxlength);
}
print "Maxlength: $maxlength\n";

# print out the data
# NOTE: we're being fancy here, prepending an "a" to each sample name to force plotly to treat sample names as categorical values, otherwise samplenames beginning with digits cause failure. This is only necessary for violin plots
@sample = sort keys %data;
print OFILE "size";
foreach $sample (@samples) {
    print OFILE "\ta$sample";
}
print OFILE "\n";
for $i (0..$maxlength) {
    print OFILE "$i";
    foreach $sample (@samples) {
	$value = (defined($fulldata{$sample}[$i])) ? $fulldata{$sample}[$i] : "";
	print OFILE "\t$value";
    }
    print OFILE "\n";
}
close OFILE;

$title = "Insert Size";
$text = "The distribution of library fragment insert sizes is shown for each sample. This metric is calculated by Picard. ";

$traces = "";
for $i (1..$#samples) {
    $traces .= " %>% add_trace(y=~a$samples[$i], name='$samples[$i]', line=list(color = '#1E77B4'), type='violin', hoveron='violins', hoverinfo='text', text='$samples[$i]') ";
}

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name, warning=FALSE}

library(plotly)

datat <- read.table("$ofile", header=T, sep="\t");

config(plot_ly(
  data=datat,
  y = ~a$samples[0],
  name = '$samples[0]',
 type = "violin", points = FALSE, line=list(color = '#1E77B4'), showlegend = FALSE,
hoveron = "violins",
hoverinfo="text",
text='$samples[0]'
) $traces %>% 
 layout(dragmode='pan', xaxis = list(title = "Sample", type = "category", tickmode = "linear", tickfont = list(size = 10)), yaxis = list(title = "Insert size (bp)", range = c(0,1200), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;
$args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);


exit;

################# subs ###################

sub median {
    $data = shift;
    $total = shift;
    $sum = 0;
    foreach $size (sort {$a <=> $b} keys %{$data}) {
	$sum += $data->{$size};
	return $size if ($sum >= $total / 2);
    }	
}

sub stdev{
    my ($data) = shift @_;
    $count = keys (%{$data});
    if ($count == 1) {
	return 0;
    }
    $average = shift @_;
    my $sqtotal = 0;
    foreach $key (keys %{$data}) {
	$sqtotal += ($average-$data->{$key}) ** 2;
    }
    my $std = ($sqtotal / ($count-1)) ** 0.5;
    return $std;
}
