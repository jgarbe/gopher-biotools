#!/usr/bin/perl -w

###############################################################
# ccs-summary.pl
# John Garbe
# November 2019 - Happy thanksgiving
#
##############################################################

=head1 NAME

ccs-summary.pl - Calculate stats about a sequel ccs fasta(.gz) file

=head1 SYNOPSIS

ccs-summary.pl file.fasta > file.txt

=head1 DESCRIPTION

Options:
    -h : Display usage information

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: ccs-summary.pl file.fasta > file.txt\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('h');
pod2usage(q(-verbose) => 3) if ($opt_h);

$ifile = $ARGV[0];
if ($ifile =~ /\.gz$/) {
    open "IFILE", "gunzip -c $ifile |" || die("cannot open $ifile\n");
}
else {
    open "IFILE", "<$ifile" || die("cannot open $ifile\n");
}

@line = ("junkname");
$seq = "junkseq";
$id = "junk";
$first = 1;

$readcount = 0;
@lengths = ();
$bases = 0;
$max = 0;
$min = 100000000;
while ($line = <IFILE>) {
    $line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
    chomp $line;

    if ($line =~ "^>") {
	if (! $first) {
#  print "$id\t$seq\n" unless ($first);
	    $readcount++;
	    $mylength = length($seq);
	    push @lengths, $mylength;
	    $bases += $mylength;
	    $min = $mylength if ($mylength < $min);
	    $max = $mylength if ($mylength > $max);
	}
	$first = 0;
	$id = $line;
	$seq = "";
	
    } else {
	$seq = "$seq$line";
    }
    
}
#print "$id\t$seq\n" unless ($first);
if (! $first) {
    $readcount++;
    $mylength = length($seq);
    push @lengths, $mylength;
    $bases += $mylength;
    $min = $mylength if ($mylength < $min);
    $max = $mylength if ($mylength > $max);
}

print "$bases bases\n";
print "$readcount reads\n";
$mean = &round10(&mean(\@lengths));
print "$mean readlength (mean)\n";
print "$max max readlength\n";
print "$min min readlength\n";


exit 0;

######################### helper subs #######################

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}
