#!/usr/bin/perl -w

#######################################################################
#  Copyright 2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

hisat2plot.pl - Generate plot from Hisat2 alignment summaries

=head1 SYNOPSIS

hisat2plot.pl align_summary1.txt [align_summary2.txt ...]
hisat2plot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing Hisat2 mapping percentage for multiple samples

Options:
    -f filelist.txt : Provide a file with a list of align_summary.txt files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: hisat2plot.pl align_summary.txt [align_summary2.txt ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of alignment files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# For debugging:
#for $i (0..$#files) { 
#    print "$files[$i] $samples[$i]\n";
#}
#print "@files\n";
#print "@samples\n";

# run through the alignment files, saving stats
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";
    $pe = 0; # start out assuming single-end read file

    $line = <IFILE>; # 20000 reads; of these:
    $line =~ /(\d+)/;
    $total_count = $1;
    $line = <IFILE>; 
    # SE:
    # 20000 (100.00%) were unpaired; of these:
    # PE: 
    # 10000 (100.00%) were paired; of these:

    if ($line =~ /were unpaired/) {
	# single-end
	$line = <IFILE>; # 1247 (6.24%) aligned 0 times
	$line = <IFILE>; # 18739 (93.69%) aligned exactly 1 time
	$line =~ /(\d+)/;
	$smap_count = $1;
	$line = <IFILE>; # 14 (0.07%) aligned >1 times
	$line =~ /(\d+)/;
	$mmap_count = $1;
	$line = <IFILE>; # 93.77% overall alignment rate

	# save stats
	$data{$ifile}{pe} = $pe;
	$data{$ifile}{total_count} = $total_count;
	$data{$ifile}{aligned_pct} = ($smap_count + $mmap_count) / $total_count * 100;
	$data{$ifile}{umap_count} = $total_count - $smap_count - $mmap_count;
	$data{$ifile}{smap_count} = $smap_count;
	$data{$ifile}{smap_pct} = ($smap_count) / $total_count * 100;
	$data{$ifile}{mmap_count} = $mmap_count;
	$data{$ifile}{mmap_pct} = $mmap_count / $total_count * 100;    

    } elsif ($line =~ /were paired/) {
	# paired-end
	$pe = 1;
	$line = <IFILE>; # 650 (6.50%) aligned concordantly 0 times
	$line = <IFILE>; # 8823 (88.23%) aligned concordantly exactly 1 time
	$line =~ /(\d+)/;
	$concord_count = $1;
	$line = <IFILE>; # 527 (5.27%) aligned concordantly >1 times
	$line =~ /(\d+)/;
	$mconcord_count = $1;
	$line = <IFILE>; # ----
	$line = <IFILE>; # 650 pairs aligned concordantly 0 times; of these:
	$line = <IFILE>; # 34 (5.23%) aligned discordantly 1 time
	$line =~ /(\d+)/;
	$discord_count = $1;

	$line = <IFILE>; # ----
	$line = <IFILE>; # 616 pairs aligned 0 times concordantly or discordantly; of these:
	$line = <IFILE>; # 1232 mates make up the pairs; of these:
	$line = <IFILE>; # 660 (53.57%) aligned 0 times
	$line = <IFILE>; # 571 (46.35%) aligned exactly 1 time
	$line = <IFILE>; # 1 (0.08%) aligned >1 times
	$line = <IFILE>; # 96.70% overall alignment rate

	# save stats
	$data{$ifile}{pe} = $pe;
	$data{$ifile}{total_count} = $total_count;
	$data{$ifile}{aligned_pct} = ($concord_count + $discord_count) / $total_count * 100;
	$data{$ifile}{concord_count} = $concord_count;
	$data{$ifile}{concord_pct} = $concord_count / $total_count * 100;
	$data{$ifile}{mconcord_count} = $mconcord_count;
	$data{$ifile}{mconcord_pct} = $mconcord_count / $total_count * 100;
	$data{$ifile}{discord_count} = $discord_count;
	$data{$ifile}{discord_pct} = $discord_count / $total_count * 100;
	$data{$ifile}{unmapped_count} = $total_count - $concord_count - $discord_count - $mconcord_count;
    } else {
	$data{$ifile}{total_count} = $total_count;
	$data{$ifile}{aligned_pct} = 0;
    }
}

### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print "$sample\tpct_aligned\t" . &round100($data{$ifile}{aligned_pct}) . "\n";
    if ($pe) {
	print "$sample\tconcordant\t$data{$ifile}{concord_count}\n";
	print "$sample\tpct_concordant\t" . &round100($data{$ifile}{concord_pct}) . "\n";
	print "$sample\ttotalreads\t$data{$ifile}{total_count}\n";
    }

    if ($pe) {
	$data{$sample}{concord_count} = $data{$ifile}{concord_pct};
	$data{$sample}{mconcord_count} = $data{$ifile}{mconcord_pct};
	$data{$sample}{discord_count} = $data{$ifile}{discord_pct};
    } else {
	$data{$sample}{smap_pct} = $data{$ifile}{smap_pct};
	$data{$sample}{mmap_pct} = $data{$ifile}{mmap_pct};
    }
    $total{$sample} = $data{$ifile}{aligned_pct};
}
print "STATS End\n";

if ($pe) {
    @metrics = ("concord_pct", "mconcord_pct", "discord_pct");
    @metricnames = ("Uniquely mapped", "Multi mapped", "discordantly mapped");
} else {
    @metrics = ("smap_pct", "mmap_pct");
    @metricnames = ("Uniquely mapped", "Multi mapped")
}

### Print out samples ordered by alignment
@order = sort {$total{$b} <=> $total{$a}} keys %total;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tAlignment rate\t$order\n";

### Plotting ###
$name = "hisat2alignmentplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating $name\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n";
# print out the data
print OFILE "sample";
foreach $metric (@metrics) {
    print OFILE "\t$metric";
}
print OFILE "\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print OFILE "$sample";
    foreach $metric (@metrics) {
        print OFILE "\t$data{$ifile}{$metric}";
    }
    print OFILE "\n";
}
close OFILE;

# plot the data
$traces = "";
for $metric (1..$#metrics) {
    $traces .= qq(%>% add_trace(y=~$metrics[$metric],  name = "$metricnames[$metric]", text = paste0(datat\$sample,"<br>$metricnames[$metric] ",round(datat\$$metrics[$metric],2),"%")) );
}

$title = "Alignment Rate";
$text = "Percent of reads aligned to reference is shown. These metrics are calculated by Hisat2.";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$metrics[0],
 name = "$metricnames[0]",
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>$metricnames[0] ",round(datat\$$metrics[0],2),"%") ) $traces
%>% layout(xaxis = sampleorder, dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


# Round to nearest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
