#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

insertsize.pl - Calculate the insert size mean and standard deviation of a paired-end dataset

=head1 SYNOPSIS

insertsize.pl [-m 1] bowtieindex R1.fastq R2.fastq

=head1 DESCRIPTION

Calculate the insert size mean and standard deviation by aligning some reads from a pair of fastq files to a bowtie2 index

 -b bowtieindex : A Bowtie2 index
 -m integer : The first N million reads from the fastq files will be aligned (Default 1)
 -p integers : Number of threads to use (Default $PBS_NUM_PPN or 1);
 -h : Print usage instructions and exit
 -v : Print more information while running (verbose)

=head1 EXAMPLE

Run the script::

    $ insertsize.pl bowtieindex R1.fastq R2.fastq

Runtime: 15 seconds using "-m .1 -p 8" on Itasca, 102 seconds using "-m 1 -p 8" on Itasca

=cut

############################# Main  ###############################

use Cwd 'abs_path';
use Getopt::Std;
use Pod::Usage;
use File::Temp qw/ tempdir /;

our ($opt_m, $opt_p, $opt_h, $opt_v);

$usage = "USAGE: insertsize.pl bowtieindex R1.fastq R2.fastq\n";
die $usage unless getopts('m:hvp:');
pod2usage(q(-verbose) => 3) if ($opt_h);
die $usage unless ($#ARGV == 2);

### parameters
my ($bowtie, $r1, $r2) = @ARGV;
die "File $r1 not found\n" unless (-e $r1);
die "File $r2 not found\n" unless (-e $r2);
print "WARNING: Could not locate a Bowtie index corresponding to basename $bowtie\n" if (! -e "$bowtie.1.bt2");
$scratchfolder = tempdir( DIR => "./", CLEANUP => 1) || die "unable to create temporary directory $!\n"; 
my $threads = $opt_p // $ENV{"PBS_NUM_PPN"} // 1;
my $million = $opt_m // 1;
my $verbose = $opt_v // 0; # print out more stuff
my $log = "";
$log = " 2> /dev/null" unless ($verbose);

# Create sample datasets
print "Creating sample datasets with $million million reads\n" if ($verbose);
$r1tmp = "$scratchfolder/r1";
$r2tmp = "$scratchfolder/r2";
$out = "$scratchfolder/out";
#$sam = "$scratchfolder/out.sam";
#$bam = "$scratchfolder/out.bam";
#$bamsort = "$scratchfolder/out.sortbam";

$numlines = $million * 1000000 * 4;
`head -n $numlines $r1 > $r1tmp`;
`head -n $numlines $r2 > $r2tmp`;

# align the sample datasets
print "Aligning sample datasets with bowtie\n" if ($verbose);
#print "module load bowtie; bowtie2 --no-mixed -k 1 --very-fast --no-discordant --no-unal -x $bowtie -1 $r1tmp -2 $r2tmp -S $out.sam\n";
$result = `module load bowtie; bowtie2 -X 700 --no-mixed -k 1 --very-fast --no-discordant --no-unal -p $threads -x $bowtie -1 $r1tmp -2 $r2tmp -S $out.sam $log`;
print $result if ($verbose);

# process bam file
print "Processing sam/bam files with samtools\n" if ($verbose);
$command = "module load samtools/1.3; samtools view -bS $out.sam $log | samtools sort -T $scratchfolder - -o $out.sort.bam $log";
$result = `$command`;
print $result if ($verbose);

# run picardtools
print "Running picard\n" if ($verbose);
#print "module load picard; java -Xmx2g -jar /soft/picard/1.83/CollectInsertSizeMetrics.jar INPUT=$out.sort.bam OUTPUT=$scratchfolder/insertmetrics.txt HISTOGRAM_FILE=$scratchfolder/insert.pdf QUIET=true\n";
$result = `module load java/jdk1.8.0_45; java -Xmx2g -jar /panfs/roc/itascasoft/picard/2.3.0/picard.jar CollectInsertSizeMetrics INPUT=$out.sort.bam OUTPUT=$scratchfolder/insertmetrics.txt HISTOGRAM_FILE=$scratchfolder/insertmetrics.pdf QUIET=true $log`;
print $result if ($verbose);

`cp $scratchfolder/insertmetrics.pdf .` if ($verbose);
`cp $scratchfolder/insertmetrics.txt .` if ($verbose);

# grab mean and stddev from metric file
$ifile = "$scratchfolder/insertmetrics.txt";
open IFILE, $ifile or die "cannot open metrics file $ifile: $!\n";
# skip header
while (<IFILE>) {
    if (/^MEDIAN_INSERT_SIZE/) {
	$line = <IFILE>;
	chomp $line;
	@line = split /\t/, $line;
	$median = $line[4];
	$stddev = $line[5];
    }
}

print "$median\t$stddev\n";
close IFILE;

exit;

# run through the insert metric files, saving stats
$ifile = "$scratchfolder/insertmetrics.txt";
open IFILE, $ifile or die "cannot open metrics file $ifile: $!\n";
# skip header
while (<IFILE>) {
    last if (/^insert_size/);
}
#    print "File $files[$i] columns: $line";
$total = 0;
%data = ();
while (<IFILE>) {
    chomp;
    next unless length;
    # some datasets have mulitple columns: sum them
    my (@line)  = split;
    $size = shift @line;
    $count = shift @line;
    while (@line) {
	$count += shift @line;
    }
    $total += $count;
    $data{$size} = $count;
}
print &median(\%data, $total), "\n";

exit;

###################### subs ########################


sub median {
    $data = shift;
    $total = shift;
    $sum = 0;
    foreach $size (sort keys %{$data}) {
	$sum += $data->{$size};
	return $size if ($sum >= $total / 2);
    }	
}
