#!/usr/bin/perl -w

##############################
# s3put.pl
# John Garbe
# January 2016
#
# Recursively upload a folder to an s3 bucket in parallel (20 files at a time)
#
##############################

use File::Basename;

$usage = "USAGE: s3put.pl sourcefolder s3://bucket/\n";
die $usage unless ($#ARGV == 1);
$source = shift @ARGV;
$bucket = shift @ARGV;
$opt_t = 20;
$threads = $opt_t // 10;
my $starttime = time();
my $checkpointtime = $starttime;

# check for trailing "/" on bucket
#$char = substr($bucket,length($bucket)-1,1);
#die "Error: S3 bucket path must end with \"/\"\n" if ($char ne "/");

# open pipe to gnu parallel
#$gpout = "gp.out";
#open GP, "| parallel -j $threads > $gpout" or die "Cannot open pipe to gnu parallel\n";
open GP, "| parallel -j $threads" or die "Cannot open pipe to gnu parallel\n";
#open GP, ">commands.txt";

# parse list of bucket contents, send download commands to gnu parallel
@result = `find $source -type f`;

print "Uploading these files...\n";
$objectcount = 0;
foreach $file (@result) {
    chomp $file;
    my ($name, $path) = fileparse($file);
    $path =~ s/^\///;
    print GP "s3cmd put $file $bucket/$path\n";
    $objectcount++;
}

close GP;
if ($?) {
    print "Error: not all objects uploaded successfully: $?\n" 
} else {
    print "Finished uploading $objectcount objects\n"; 
}
&runtime;

################## subs ###################

# this truncates instead of rounds, fix it
sub round10 {
    return int($_[0] * 10) / 10;
}
# print how long we've been running
sub runtime {
    $now = time();
    my $runtime = $now - $starttime;
    my $checktime = $now - $checkpointtime;
    print "Total time: " . &formattime($runtime) . "\n";
    $checkpointtime = $now;
}

# print out checkpoint time info
sub formattime {
    my ($time) = @_;
    if ($time < 60) {
        return "$time seconds";
    } elsif ($time < 7200) { # 2 hours
        my $minutes = &round10($time / 60);
        return "$minutes minutes";
    } else {
	$hours = &round10($time / 3600);
        return "$hours hours";
    }
}
