#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

subreadplot.pl - Generate plots from Subread FeatureCount summary output files

=head1 SYNOPSIS

subreadplot.pl subread-counts1.txt.summary [subread-counts2.txt.summary ...]
subreadplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing count results for multiple samples

Options:
    -f filelist.txt : Provide a file with a list of subread-counts.txt.summary files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: subreadplot.pl subread-counts1.txt.summary [subread-counts2.txt.summary ..]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of summary files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# run through the summary files, saving stats
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";
#    print "file: $ifile\n";
    while ($line = <IFILE>) {
#	print "$line";
	chomp $line;
	next if ($line =~ /^Status/);
	@line = split /\t/, $line;
	$rawdata{$ifile}{$line[0]} = $line[1];
	$total{$ifile} += $line[1] unless ($line[0] eq "Status");
    }
    close IFILE;
}
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $total = $total{$ifile} // 1;
    $data{$sample}{pct_assigned} = $rawdata{$ifile}{Assigned} / $total * 100;
    $data{$sample}{pct_unmapped} = $rawdata{$ifile}{Unassigned_Unmapped} / $total * 100;
    $data{$sample}{pct_mappingquality} = $rawdata{$ifile}{Unassigned_MappingQuality} / $total * 100;
    $data{$sample}{pct_nofeature} = $rawdata{$ifile}{Unassigned_NoFeatures} / $total * 100;
    $data{$sample}{pct_ambiguous} = $rawdata{$ifile}{Unassigned_Ambiguity} / $total * 100;
    $data{$sample}{pct_multimapped} = $rawdata{$ifile}{Unassigned_MultiMapping} / $total * 100;
}

@metrics = ("pct_assigned", "pct_ambiguous", "pct_nofeature", "pct_multimapped", "pct_mappingquality", "pct_unmapped");
@metricnames = ("Exonic", "Ambiguous", "Intronic and Intergenic", "Multimapped", "Low mapping quality", "Unmapped");

### Print out summary stats ###
print "STATS Start\n";
foreach $sample (sort keys %data) {
    foreach $metric (@metrics) {
	print "$sample\t$metric\t$data{$sample}{$metric}\n";
    }
}
print "STATS End\n";

### Print out samples ordered by assigned
@order = sort {$data{$b}{pct_assigned} <=> $data{$a}{pct_assigned}} keys %data;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tSubread Counts (Exonic)\t$order\n";

### Plotting ###
$name = "subreadplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating $name\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n";
# print out the data
print OFILE "sample";
foreach $metric (@metrics) {
    print OFILE "\t$metric";
}
print OFILE "\n";
foreach $sample (sort keys %data) {
    print OFILE "$sample";
    foreach $metric (@metrics) {
        print OFILE "\t$data{$sample}{$metric}";
    }
    print OFILE "\n";
}
close OFILE;

# plot the data
$traces = "";
for $metric (1..$#metrics) {
    $traces .= qq(%>% add_trace(y=~$metrics[$metric],  name = "$metricnames[$metric]", text = paste0(datat\$sample,"<br>$metricnames[$metric] ",round(datat\$$metrics[$metric],2),"%")) );
}

$title = "Subread Counts";
$text = "Subread assigns read to genomic features defined in the GTF annotation file. This plot summarizes read assignment categories. Only reads in the \"Exonic\" category are used in calculating the raw read counts for each gene.";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$metrics[0],
 name = "$metricnames[0]",
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>$metricnames[0] ",round(datat\$$metrics[0],2),"%") ) $traces
%>% layout(xaxis = sampleorder, dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

