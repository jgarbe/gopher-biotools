#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

picardhsmetricsplot.pl - Generate plots from Picard HsMetrics output files

=head1 SYNOPSIS

picardhsmetricsplot.pl hsmetrics1.txt [hsmetrics2.txt ...]
picardhsmetricsplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing HsMetrics stats from multiple samples

Options:
    -f filelist.txt : Provide a file with a list of hsmetrics.txt files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: picardhsmetricsplot.pl hsmetrics1.txt [hsmetrics2.txt ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of summary files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# run through the summary files, saving stats
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    $line = <IFILE>;
	    chomp $line;
	    @keys = split /\t/, $line;
	    $line = <IFILE>;
	    chomp $line;
	    @values = split /\t/, $line;
	    for $i (0..$#keys) {
		$values[$i] = "" unless (defined($values[$i])); # fill in for empty values
		$data{$ifile}{$keys[$i]} = $values[$i];
	    }
	    last;
	}
    }
}

### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $sum = 0;
    foreach $key (keys %{$data{$ifile}}) {
	print "$sample\t$key\t$data{$ifile}{$key}\n";
    }
}
print "STATS End\n";

### On-near-bait Plot ###
### Print out data to a file ###
$ofile = "hsmetricsplot.dat";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $sum = 0;

    $offbases = $data{$ifile}{"OFF_BAIT_BASES"};
    $nearbases = $data{$ifile}{"NEAR_BAIT_BASES"};
    $onbases = $data{$ifile}{"ON_BAIT_BASES"};

    $totalbases = $offbases + $nearbases + $onbases;
    $pctnear = round100($nearbases / $totalbases * 100);
    $pcton = round100($onbases / $totalbases * 100);

    print OFILE "$sample\tpcton\t$pcton\n";
    print OFILE "$sample\tpctnear\t$pctnear\n";
}
close OFILE;

### Plotting ###
$ofile = "hsmetricsplot.dat";
$rfile = "hsmetricsplot.r";
my $height = 480;
my $width = 480;
my $numsamples = $#files;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}

### summary plot ###
print "Generating picard-bait.png\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"picard-bait.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data))

  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Percent of aligned bases\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#0000FF\", \"#00008B\"), name=\"\", breaks=c(\"pcton\", \"pctnear\"), labels=c(\"On Bait\", \"Near Bait\")) + coord_cartesian(ylim = c(0, 100));


  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

#### Coverage Plot ####

### Print out data to a file ###
$ofile = "hsmetricsplot.dat";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $sum = 0;
    foreach $key ("PCT_TARGET_BASES_100X", "PCT_TARGET_BASES_50X", "PCT_TARGET_BASES_40X", "PCT_TARGET_BASES_30X", "PCT_TARGET_BASES_20X", "PCT_TARGET_BASES_10X", "PCT_TARGET_BASES_2X") {
	$value = ($data{$ifile}{$key} * 100) - $sum; # scale from fraction to percent
	$sum += $value;
	print OFILE "$sample\t$key\t$value\n";
    }
}
close OFILE;

### Plotting ###
$ofile = "hsmetricsplot.dat";
$rfile = "hsmetricsplot.r";
$height = 480;
$width = 550;
$numsamples = $#files;
if ($numsamples > 6) {
    $width = 550 + (20 * ($numsamples-6));
}

### summary plot ###
print "Generating picard-coverage.png\n";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"picard-coverage.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data))

  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"% of all target bases achieving coverage level\") + theme(legend.title=element_blank()) + scale_fill_manual(drop=FALSE, values=c(\"#4d4d4d\", \"#ef8a62\", \"#fddbc7\", \"#b2182b\", \"#ffffff\", \"#e0e0e0\", \"#999999\"), breaks=c(\"PCT_TARGET_BASES_2X\", \"PCT_TARGET_BASES_10X\", \"PCT_TARGET_BASES_20X\", \"PCT_TARGET_BASES_30X\", \"PCT_TARGET_BASES_40X\", \"PCT_TARGET_BASES_50X\", \"PCT_TARGET_BASES_100X\"), labels=c(\"2X\", \"10X\", \"20X\", \"30X\", \"40X\", \"50X\", \"100X\")) + scale_y_continuous(limits = c(0, 100));

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

########################## subs ######################

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
}
