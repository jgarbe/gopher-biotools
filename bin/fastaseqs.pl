#!/usr/bin/perl -w

###############################################################
# fastaseqs.pl
# John Garbe
# March 2020
#
##############################################################

=head1 NAME

fastaseqs.pl - Report number of sequences in a fasta file

=head1 SYNOPSIS

fastaseqs.pl file.fasta

=head1 DESCRIPTION

Report the number of sequences in a fasta file

Options:
    -h : Display usage information

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: fastaseqs.pl file.fasta\n";

die $usage unless getopts('h');
pod2usage(q(-verbose) => 3) if ($opt_h);

$file = shift @ARGV or die $usage;
if ($file =~ /\.gz/) {
    $result = `gunzip -c $file | grep "^>" | wc -l`;
} else {
    $result = `grep "^>" $file | wc -l`;
}
print $result;
