#!/usr/bin/perl -w

#######################################################################
#  Copyright 2012 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

pe-sync-2-files.pl - Check if the reads in a pair of paired-end fastq files are in the same order (synchronized)

=head1 SYNOPSIS

pe-sync-2-files.pl sample1_R1.fastq sample1_R2.fastq 

=head1 DESCRIPTION

Checks if a pair of paired-end fastq files are synchronized. Fastq read ID 1.7 and 1.8 styles are supported. This progam takes several minutes to run on large (>10Gb) fastq files. Paired-end fastq files often become out-of-sync when run through programs that are not designed to handle paired-end data, such as quality trimming or filtering program that remove a read from one file, but leave the companion read in the other file in place. This script will tell you if your failes are out of sync. If they are, try using the resync.pl script to resynchronize them. The "quick" option will not report the percent of out-of-sync reads for many failed files, but will run much faster.

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_v);

$usage = "Usage: pe-sync-2-files.pl [quick] left_fastqfile right_fastqfile\n";
die $usage unless getopts('hv');
pod2usage(q(-verbose) => 3) if ($opt_h);
die $usage unless ($#ARGV > 0);

# determine if we're running on solaris in order to change system calls
$output = `perl -v`;
$solaris = 0;
$solaris = 1 if ($output =~ /solaris/);
if ($solaris) {
#    print STDERR "Running on Solaris\n";
}

# handle "quick" option
$quick = 0; # 1 for quickcheck, 0 for thorough check
if (defined($ARGV[0])) { 
    if ($ARGV[0] eq "quick") {
	$quick = 1;
	shift @ARGV;
    }
}
print STDERR "QUICK CHECK enabled\n" if ($quick);

$filename_r1 = $ARGV[0];
$filename_r2 = $ARGV[1];

# identify read id format type
if ($solaris) {
    $readid = `head -1 $filename_r1`;
} else {
    $readid = `head -n 1 $filename_r1`;
}
chomp $readid;
if ($readid =~ /^@\S+\/[12]$/) { # @ at the start of the line followed by non-whitespace, a /, a 1 or 2, the end of the line
    $id_type = 1;
    print STDERR "Casava 1.7 read id style\n"; # TESTING
} elsif ($readid =~ /^@\S+\W[12]\S+$/) { # @ at the start of the line followed by non-whitspace, a space, a 1 or 2, non-whitespace
    $id_type = 2;
    print STDERR "Casava 1.8 read id style\n"; # TESTING
} else {
    print STDERR "Cannot determine read id style\n";
    print STDOUT "Unknwon id style: $readid\n";
    exit 1;
}

chomp $filename_r1;
chomp $filename_r2;

if ($quick) {
    $failure = &quickcheck($filename_r1, $filename_r2); # quickie check
    print STDERR "FAILED quickcheck\n" if ($failure);
}
# If we aren't doing the quick check, or the quick check passed, do the slow check
if (!($quick) || !($failure)) {
    $failure = &percent($filename_r1, $filename_r2); # slow thorough check
}

exit;

######################## subs #########################
# do a quick check to see if the files are bad (many bad files will pass this quick check)
sub quickcheck {
    my ($file1, $file2) = @_;
    if ($solaris) {
	$result1 = `tail -10000l $file1 | head -1`; # grab the line 10,000 lines from the EOF
	$result2 = `tail -10000l $file2 | head -1`;
    } else {
	$result1 = `tail -n 10000 $file1 | head -n 1`;
	$result2 = `tail -n 10000 $file2 | head -n 1`;
    }
    # process the ids depending on id style
    if ($id_type == 1) { # 1 or 2 at end of id
	chomp $result1;
	chomp $result2;
	chop $result1;
	chop $result2;
    } else { # first word is a unique read id, second word is left/right-specific
	($result1) = split ' ', $result1;
	($result2) = split ' ', $result2;
    }
    if ($result1 eq $result2) {
#	print "pass\n";
	return 0;
    } else {
#	print "fail\n";
	return 1;
    }
}


# compare every read id from each file and determine what percent of reads are
# out of sync
sub percent {
    my($file1, $file2) = @_;
    
    open R1, '<', $file1 or die "Couldn't open $file1 for reading:$!";
    open R2, '<', $file2 or die "Couldn't open $file2 for reading:$!";
    
    my $readCount = 0;
    my $failcount = 0;
    while (!eof(R1) and !eof(R2)) {
	$id1 = <R1>;
	$id2 = <R2>;
	next unless ($. % 4 == 1); # check every sequence (4th line in the file)
	# process the ids depending on id style
	if ($id_type == 1) { # 1 or 2 at end of id
	    	($id1) = split /\//, $id1;
	    	($id2) = split /\//, $id2;
	} else {
	    ($id1) = split ' ', $id1;
	    ($id2) = split ' ', $id2;
	}
	$failcount++ unless ($id1 eq $id2);
	
	print "$id1 != $id2\n" if (($id1 ne $id2) and ($opt_v)); 
	$readCount++;
    }
    my $percent = $failcount / $readCount;
    $prettypercent = $percent * 100; # make it percent
    $prettypercent = sprintf("%.2f", $prettypercent);
    if ($failcount > 0) {
	print STDERR "FAILED full check\t$prettypercent% of reads are out-of-sync\n";
	$exitStatus=1;
    } else {
	print STDERR "PASSED full check\n";
	$exitStatus=0;
    }

    return $exitStatus;
}
