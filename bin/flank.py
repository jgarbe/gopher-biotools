#!/usr/bin/env python3

import pysam
import argparse

parser = argparse.ArgumentParser(description='Generate a table of flanking sequence for each variant in a VCF file.')
parser.add_argument('--vcf', dest='vcffile', type=str, required=True,
                    help='input VCF file')
parser.add_argument('--genome', dest='genomefile', type=str, required=True,
                    help='reference genome fasta file')
parser.add_argument('--flank', dest='flank', type=int, default=50,
                    help='number of bases of flanking sequence')

args = parser.parse_args()

# open vcf file
vcf = pysam.VariantFile(args.vcffile)
# open fasta file
genome = pysam.FastaFile(args.genomefile)
# define by how many bases the variant should be flanked
flank = args.flank

# iterate over each variant
for record in vcf:
    # extract sequence
    #
    # The start position is calculated by subtract the number of bases
    # given by 'flank' from the variant position. The position in the vcf file
    # is 1-based. pysam's fetch() expected 0-base coordinate. That's why we
    # need to subtract on more base.
    #
    # The end position is calculated by adding the number of bases
    # given by 'flank' to the variant position. We also need to add the length
    # of the REF value and subtract again 1 due to the 0-based/1-based thing.
    #
    # Now we have the complete sequence like this:
    # [number of bases given by flank]+REF+[number of bases given by flank]
    seq = genome.fetch(record.chrom, record.pos-1-flank, record.pos-1+len(record.ref)+flank)

    # print out tab seperated columns:
    # CRHOM, POS, REF, ALT, flanking sequencing with variant given in the format '[REF/ALT]'
    print(
        record.chrom,
        record.pos,
        record.ref,
        record.alts[0],
        '{}[{}/{}]{}'.format(seq[:flank], record.ref, record.alts[0], seq[flank+len(record.ref):]),
        sep="\t"
    )
