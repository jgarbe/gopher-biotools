#!/usr/bin/perl -w

#######################################################################
#  Copyright 2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

gtf-prep.pl - Prepare a GTF file for use by Cufflinks

=head1 SYNOPSIS

gtf-prep.pl -g annotation.gtf [-r reference.fa] > newannotation.gtf

=head1 DESCRIPTION

This script adds tss_ids and p_ids to a GTF file and verifys that it will work with Cufflinks

Options:
    -g file : GTF file to process
    -r file : Reference genome fasta file
    -o file : output GTF file (default: gtf-prep.gtf)
    -h      : Print usage information
    -v      : Verbose

=cut

use Getopt::Std;
use Pod::Usage;
use File::Basename;

our ($opt_h, $opt_v, $opt_g, $opt_r, $opt_o);

# get parameters
$ok = getopts('g:r:o:hv');
pod2usage(q(-verbose) => 3) if ($opt_h);
die "USAGE: gtf-prep.pl -g annotation.gtf -r reference.fa > newannotation.gtf\n" unless ($opt_g && $ok);

$fasta = $opt_r // "";
die "Unable to locate reference fasta file $fasta\n" unless ($fasta && -e $fasta);
$gtf = $opt_g;
die "Unable to locate reference GTF file $gtf\n" unless (-e $gtf);
$output = $opt_o // "gtf-prep.gtf";
$verbose = $opt_v // 0;

### Check that reference fasta chromosomes and gtf chromosomes are the same
if ($fasta) {
    print "Verifying that reference fasta and gtf chromosome IDs are the same\n";
    $error = 0;
    print "Fasta file chromosome IDs:\n" if ($verbose);
    @results = `grep "^>" $fasta`;
    foreach $result (@results) {
	substr $result, 0, 1, ""; # remove >
	chomp $result;
	$fasta{$result} = 1;
	print "$result\n" if ($verbose);
    }
    
    @results = `cut -f1 $gtf | sort -u`;
    print "GTF file chromosome IDs:\n" if ($verbose);
    foreach $result (@results) {
	chomp $result;
#    $fasta{$result} = 1;
	print "$result\n" if ($verbose);
	if (!defined($fasta{$result})) {
	    print "ERROR: Chromosome ID $result in GTF file not found in reference fasta file\n";
	    $error = 1;
	}
    }

die "ERROR: Chromosome IDs in GTF file to not match Reference fasta file\n" if ($error);
}

### Run through gffread
print "Processing GTF file with gffread\n";
$result = `gffread -E $gtf -o- > gtf-prep.gffread.gtf`;
die "ERROR: $result" if ($result);

### Run through cuffcompare
print "Running cuffcompare\n";
#$result = `cuffcompare -o gtf-prep gtf-prep.gffread.gtf`;
$result = `cuffcompare -o gtf-prep -GC -s $fasta -r gtf-prep.gffread.gtf gtf-prep.gffread.gtf`;
print $result;

# pull out tss_id and p_id from new gtf
print "Pulling tss and p ids from new gtf file\n";
open IFILE, "gtf-prep.combined.gtf" or die "Cannot open combined gtf file gtf-prep.combined.gtf: $!\n";
my $junk;
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line; #chrI ce10_refGene exon 11641 11689 . + . gene_id "XLOC_000001"; transcript_id "TCONS_00000001"; exon_number "1"; gene_name "NM_058259"; oId "NM_058259"; nearest_ref "NM_058259"; class_code "="; tss_id "TSS1"; p_id "P2";
    @info = split /;/, $line[8];
    foreach $info (@info) {
	if ($info =~ /gene_name/) {
	    $gene_name = $info;
	    ($junk, $gene) = split /"/, $gene_name;
	} elsif ($info =~ /tss_id/) {
	    $ids{$gene}{tss} = $info;
	} elsif ($info =~ /p_id/) {
	    $ids{$gene}{p} = $info;
	}
    }
}
close IFILE;

# add tss_id and p_id to original gtf file
print "Making copy of original GTF file with tss and p ids added\n";
open IFILE, $gtf or die "Cannot open original gtf file $gtf: $!\n";
open OFILE, ">$output" or die "Cannot open output file $output: $!\n";
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line; #chrI ce10_refGene exon 11641 11689 . + . gene_id "XLOC_000001"; transcript_id "TCONS_00000001"; exon_number "1"; gene_name "NM_058259"; oId "NM_058259"; nearest_ref "NM_058259"; class_code "="; tss_id "TSS1"; p_id "P2";
    $line[8] =~ /gene_id "([^"]*)"/;
    $gene = $1;
    if (!defined($ids{$gene})) {
	print "Unknown gene name $gene\n";
	next;
    }
    if (!defined($ids{$gene}{tss})) {
	print "Unknown tss for gene $gene\n";
	next;
    }
    if (!defined($ids{$gene}{p})) {
	print "Unknown p for gene $gene\n";
	next;
    }
    print OFILE "$line $ids{$gene}{tss}; $ids{$gene}{p};\n"; 
}

print "Finished\n";
exit;


