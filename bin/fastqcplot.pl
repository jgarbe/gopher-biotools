#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastqqualityplot.pl - Generate per-base quality plot for multiple fastq files

=head1 SYNOPSIS

fastqqualityplot.pl -l filelist

=head1 DESCRIPTION

Generate per-base quality plot for multiple fastq files 

 -l file : a file with a list of fastqqc.pl output files, one per line. A second tab-delimited column contains the sample names, and a third columns contains the read type (R1 or R2).
 -f : add "filtered" to the plot names
 -h : Print usage instructions and exit
 -v : Print more information while running (verbose)

=head1 EXAMPLE

Run the script::

    $ fastqcplot.pl -l filelist.txt

=cut

############################# Main  ###############################

use Cwd 'abs_path';
use Getopt::Std;
use Term::ANSIColor;
use Pod::Usage;
use File::Temp qw/ tempdir /;

our ($opt_h, $opt_v, $opt_l, $opt_f);

$usage = "USAGE: fastqqcplot.pl -l filelist\n";
die $usage unless getopts('hvl:f');
die $usage unless ($opt_l);
pod2usage(q(-verbose) => 3) if ($opt_h);

### parameters
$filelist = abs_path($opt_l) unless (!$opt_l);
die "A filelist must be provided using the -l option\n" unless ($filelist);
die "Filelist $filelist not found\n" if ($filelist && (!-e $filelist));
my $verbose = $opt_v // 0; # print out more stuff

$numsamples = 0;

my (%r1quality, %r2quality);

### Run through file list ###
open IFILE, "$filelist" or die "Cannot open filelist $filelist: $!\n";
$pe = 0;
while ($fline = <IFILE>) {
    chomp $fline;
    ($file, $sample, $read) = split /\t/, $fline;
    if (! -e $file) {
	print "File $file not found, skipping\n";
	next;
    }
    print "Processing file $file\n" if ($verbose);
    $numsamples++ unless ($read eq "R2");
    $pe = 1 if ($read eq "R2");
    if ($file =~ /\.gz/) { # file is a fastqc zip file that needs uncompressing
	$scratchfolder = tempdir( DIR => "/scratch", CLEANUP => 1) || die "unable to create temporary directory $!\n";
	`unzip $file -d $scratchfolder */fastqc_data.txt`;
	($folder) = <$scratchfolder/*_fastqc>;
	$rfile = "$folder/fastqc_data.txt";
    } else { # file is a fastqc_data.txt file, process directly
	$rfile = $file;
    }
    open RFILE, "$rfile" or die "Cannot open file $rfile: $!\n";
    
    while ($line = <RFILE>) {
	next unless ($line =~ /^>>/);
	if ($line =~ /^>>Basic Statistics/) {
	    $gc = 0;
	    $totalsequences = 0;
	    $readlength = 0;
	    while ($line = <RFILE>) {
		last if ($line =~ /^>>END_MODULE/);
		chomp $line;
		if ($line =~ /^%GC/) {
		    ($text, $gc) = split /\t/, $line;
		}
		if ($line =~ /^Total Sequences/) {
		    ($text, $totalsequences) = split /\t/, $line;
		}
		if ($line =~ /^Sequence length/) {
		    ($text, $readlength) = split /\t/, $line;
		}
	    }
	    $basic{$sample}{gc} = $gc;
	    $basic{$sample}{totalsequences} = $totalsequences;
	    $basic{$sample}{readlength} = $readlength;
	} elsif ($line =~ /^>>Per sequence quality scores/) {
	    $header = <RFILE>;
	    $countsum = 0;
	    $qualitysum = 0;
	    while ($line = <RFILE>) {
		last if ($line =~ /^>>END_MODULE/);
		chomp $line;
		($quality, $count) = split /\t/, $line;
		$qualitysum += $quality * $count;
		$countsum += $count;
	    }
	    $meanreadquality = 0;
	    $meanreadquality = round10($qualitysum / $countsum) if ($countsum > 0);
	    
	} elsif ($line =~ /^>>Per base sequence quality/) {
	    $header = <RFILE>;
	    $junk = 0; 
	    while ($rline = <RFILE>) {
		last if ($rline =~ /^>>END_MODULE/);
		chomp $rline;
		($base, $mean) = split /\t/, $rline;
		($base, $junk) = split /-/, $base if ($base =~ /-/); # for pacbio support
		if ($read eq "R1") {
		    $qualityr1{$sample}{$base} = $mean;
		} elsif ($read eq "R2") {
		    $qualityr2{$sample}{$base} = $mean;
		} else {
		    next;
		}
	    }
	    
	} elsif ($line =~ /^>>Adapter Content/) {
	    $header = <RFILE>;
	    @header = split /\t/, $header;
	    $cycle = 0;
	    $adaptersum = 0;
	    while ($line = <RFILE>) {
		last if ($line =~ /^>>END_MODULE/);
		chomp $line;
		@line = split /\t/, $line;
		$cycle = $line[0];
		$sum = 0;
		for $i (1..$#line) {
		    $sum += $line[$i];
		}
		$adapter{$sample}{$cycle} = $sum;
		$adaptersum += $sum;
	    }
#		$maxcycle = $cycle;
	    $maxadapter = -1;
	    $maxadapter{$sample} = "";
	    for $i (1..$#line) {
		if ($line[$i] > $maxadapter) {
		    $maxadapter = $line[$i];
		    $maxadapter{$sample} = $header[$i];
		}
	    }
	    $adaptercontent = 0;
	    $adaptercontent = $adaptersum / $cycle if ($cycle > 0);
	    
	} elsif ($line =~ /^>>Sequence Duplication Levels/) {
	    $header = <RFILE>;
	    chomp $header; #Total Deduplicated Percentage  79.43262411347
	    my ($junk, $dup) = split /\t/, $header;
	    $duplication{$sample} = $dup;
	}
    }
    close RFILE;
    
    # calculate stats
    $stats{$sample}{"pct-gc"} = $gc;
    $stats{$sample}{"totalsequences"} = $totalsequences;
    $stats{$sample}{"sequencelength"} = $readlength;
    $stats{$sample}{"pct-dimer"} = round100($adapter{$sample}{20} // 0);
    $stats{$sample}{"pct-adapter"} = round100($adaptercontent);	
    $stats{$sample}{"pct-deduplicated"} = round100($duplication{$sample});
    $stats{$sample}{"meanreadquality$read"} = $meanreadquality;
}
close IFILE;

# print out stats
print "STATS Start\n";
foreach $sample (keys %stats) {
    foreach $key (keys %{$stats{$sample}}) {
	print "$sample\t$key\t$stats{$sample}{$key}\n";
    }
}
print "STATS End\n";

# generate cycleplot plots
$bins = int(100/($numsamples/10));
$fileprefix = ($opt_f) ? "filtered" : "";
$titleprefix = ($opt_f) ? "Filtered " : "";
$title = "${titleprefix}Quality Summary";
$title = "${titleprefix}Quality Summary (R1)" if ($pe);
&cycleplot("${fileprefix}fastqqualityplotr1", \%qualityr1, $title, $bins);
&cycleplot("${fileprefix}fastqqualityplotr2", \%qualityr2, "${titleprefix}Quality Summary (R2)", $bins) if ($pe);
&cycleplot("${fileprefix}fastqadapterplot", \%adapter, "${titleprefix}Adapter Content", $bins);


### duplication plot ###

# generate reads per sample plot
$name = "${fileprefix}fastqduplicationplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

### Print out samples ordered by duplication
@order = sort {$duplication{$b} <=> $duplication{$a}} keys %duplication;
$order = "";
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tLibrary diversity\t$order\n";

# print out the data
print OFILE "sample\tvalue\n";
foreach $sample (sort keys %duplication) {
    print OFILE "$sample\t$duplication{$sample}\n";
}
close OFILE;

$rtype = ($pe) ? " R1" : "";
$updatemenues = "updatemenus = updatemenus,";
$updatemenues = "";

# plot the data
open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">${titleprefix}Library Diversity
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
This plot shows the percentage of$rtype reads that remain after deduplication (removing duplicated sequences), which is a measure of library diversity. Sequence data from genomic DNA libraries typically have high library diversity, and data from targeted sequencing libraries typically have low diversity. This metric is calculated by FastQC.
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$value)),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~value,
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>",round(datat\$value,2),"%") )
%>% layout(xaxis = sampleorder, $updatemenues dragmode='pan', showlegend = FALSE, barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads remaining<br>after deduplication", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;

exit;

####################### helper subs #######################

sub cycleplot {
    my ($name, $data, $title, $bins) = @_;
    
    my (%smalldata, $cycles, @samples, @cycles, %binsize, %color);
    
    ### parameters
    
    $ofile = "$name.dat";
        
    @samples = sort keys %{$data};
    @cycles = sort {$a<=>$b} keys %{$data->{$samples[0]}};
    $cycles = $cycles[$#cycles];
    $maxbins = int($cycles / 4); # put a limit on the number of bins
    $bins = $maxbins if ($bins > $maxbins);
    $bins = 5 if ($bins < 5);
    
    # downsample data into $bin bins
    if (0) { # old version: uniform bins
	$bins = $cycles if ($bins > $cycles);
	$binsize = $cycles / $bins;
	print "cycles: $cycles\n";
	print "bins: $bins\n";
	print "binsize: $binsize\n";
	
	foreach $sample (@samples) {
	    $start = 1;
	    for $bin (1..$bins) {
		$sum = 0;
		$end = int($binsize * $bin);
#	    print "bin: $bin start: $start end: $end\n" if ($sample eq "JL.A1.DM1.R4");
		for $i ($start..$end) {
#		print(int($data->{$sample}{$i}) . " ") if ($sample eq "JL.A1.DM1.R4");
		    $sum += $data->{$sample}{$i} // 0;
		}
		$smalldata{$sample}{$bin} = int($sum / ($end - $start + 1))+1;
		$binsize{$bin} = $end - $start + 1;
#	    print "sum: $sum value: $smalldata{$sample}{$bin} size: $binsize{$bin}\n" if ($sample eq "JL.A1.DM1.R4");
		$bin++;
		$start = $end+1;
	    }
	}
    }
    
# new version, cluster into bins with ckmeans
# print data out to tmp file, run ckmeans on it
    $ofile = "cycleplot.dat";
    open $OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
    foreach $sample (@samples) {
	foreach $cycle (@cycles) {
	    $data->{$sample}{$cycle} = $data->{$sample}{$cycle-1} unless (defined($data->{$sample}{$cycle})); # fill in missing data
	    print $OFILE "$data->{$sample}{$cycle} ";
	    $total{$sample} += $data->{$sample}{$cycle};
	}
	print $OFILE "\n";
    }
    close OFILE;
    
    print "ckmeans.r $ofile $bins\n";
    @results = `ckmeans.r $ofile $bins`;
#    print @results;

    chomp @results;
# process results
    foreach $sample (@samples) {
	$clusters = shift @results;
	$centers = shift @results;
#	if ($sample eq "JL.A1.D6.R4") {
#	    print "$clusters\n";
#	    print "$centers\n";
#	}
	@clusters = split /\t/, $clusters;
	@centers = split /\t/, $centers;
	# calculate size of each bin
	foreach $cluster (@clusters) {
	    $binsize{$sample}{$cluster}++;
	}
	# store value (center) for each bin
#	for $i (0..$#centers) {
	for $i (0..$bins-1) {
	    $bin = $i+1;
	    $smalldata{$sample}{$bin} = $centers[$i] // 0;
	    $binsize{$sample}{$bin} = 0 unless ($binsize{$sample}{$bin});
	}
    }

    # print out data
    $ofile = "$name.dat";
    open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

    # print out the data
    print OFILE "sample";
    for $bin (1..$bins) {
	print OFILE "\tcluster$bin";
    }
    print OFILE "\ttotal\n";
    foreach $sample (@samples) {
	print OFILE "$sample";
	for $bin (1..$bins) {
	    print OFILE "\t$binsize{$sample}{$bin}";
	}
	print OFILE "\t$total{$sample}\n";
    }
    close OFILE;

    # customize based on plot type
    if ($name =~ /fastqadapterplot/) {
	$colors = qq(
colfunc<-colorRampPalette(c('darkred','indianred', 'gold', 'green4'))
mycolors <- rev(colfunc(n=100+1))
);
	$sort = "data2 <- datat[order(datat\$total),]";
	$preunit = "";
	$postunit = "% Adapter";
	$text = "This plot shows the cumulative proportion of each sample in which sequencing adapter sequences have been seen at each position. Once an adapter sequence has been seen in a read it is counted as being present right through to the end of the read so the percentages only increase as the read length goes on. It is common to see significant adapter sequence content at the ends of reads in short insert libraries (16s/18s, small RNA, amplicon). This metric is calculated by FastQC.";
	$ordername = "Adapter content";
    }
    if ($name =~ /fastqqualityplot/) {
	$colors = qq(
colfunc<-colorRampPalette(c('darkred','indianred', 'yellow', 'gold', 'darkgreen', 'green'))
mycolors <- colfunc(n=41+1)
);
	$sort = "data2 <- datat[rev(order(datat\$total)),]";	
	$preunit = "Q";
	$postunit = "";
	$text = "This plot shows the mean base quality score for each position in a fastq file. The higher the score the better the base call. Green indicates very good quality, yellow indicates reasonable quality, and red indicates poor quality. The quality of calls degrade as the sequencing run progresses, so it is common to see base calls turning yellow towards the end of a read. It is common to see base calls turning red in short insert libraries (16s/18s, small RNA, amplicon).";
	$text .= " R2 read quality is typically slightly lower overall than R1 read quality." if ($pe);
	$text .= " This metric is calculated by FastQC.";
	$ordername = "Quality";
	$ordername = "Quality (R1)" if (($name =~ /r1/) && ($pe));
	$ordername = "Quality (R2)" if ($name =~ /r2/);
    }

    # generate colors and text
    for $bin (1..$bins) {
	$color{$bin} = "marker = list(color = c(";
	$text{$bin} = "c(";
	foreach $sample (@samples) {
	    print "undefined: $sample $bin\n" if (!defined($smalldata{$sample}{$bin}));
	    $color{$bin} .= "mycolors[$smalldata{$sample}{$bin}+1],"; # colors are 1-based
	    $value = round10($smalldata{$sample}{$bin});
	    $maxadapter = $maxadapter{$sample};
	    $maxadapter =~ s/'/\\'/g;
	    $aname = ($name =~ /fastqadapterplot/) ? "<br>$maxadapter" : "";
	    $text{$bin} .= "'$sample<br>$preunit$value$postunit$aname',";

	}
	chop $color{$bin}; # take off last ,
	$color{$bin} .= "))";
	chop $text{$bin}; # take off last ,
	$text{$bin} .= ")";
    }	

    $updatemenues = "updatemenus = updatemenus,";
    $updatemenues = ""; #disable sort buttons

    # plot the data
    $traces = "";
    for $bin (2..$bins) {
	$traces .= " %>% add_trace(y=~cluster$bin, $color{$bin}, text = $text{$bin}) ";
    }

    $rfile = "$name.rmd";
    open RFILE, ">$rfile" or die "Cannot open $rfile\n";
    print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

$colors

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

$sort
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~cluster1,
 type = 'bar', showlegend=FALSE, $color{1},
hoverinfo="text",
text = $text{1} )
$traces
%>% layout(xaxis = sampleorder, $updatemenues xaxis = list(title='Sample'), dragmode='pan', showlegend = FALSE, barmode='stack', yaxis = list(title = "Position in read (bp)", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);

    # Print out samples ordered by duplication
    if ($name =~ /fastqadapterplot/) {
	@order = sort {$total{$a} <=> $total{$b}} keys %total;
    } else {
	@order = sort {$total{$b} <=> $total{$a}} keys %total;
    }
    $order = "";
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    print "ORDER\t$ordername\t$order\n";

}

########## subs ###############

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
}
