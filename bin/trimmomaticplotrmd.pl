#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

trimmomaticplot.pl - Generate plots from trimmomatic log files (trimmomatic stdout)

=head1 SYNOPSIS

trimmomaticplot.pl trimmomatic1.log [trimmomatic2.log ...]
trimmomaticplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing Trimmomatic stats for multiple samples

Options:
    -f filelist.txt : Provide a file with a list of trimmomatic.log files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: trimmomaticplot.pl trimmomatic.log [trimmomatic2.log ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of log files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# For debugging:
#for $i (0..$#files) { 
#    print "$files[$i] $samples[$i]\n";
#}
#print "@files\n";
#print "@samples\n";

# run through the log files, saving stats
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";
    $pe = 0; # start out assuming single-end read file

    # Parse stats from stdout
    # Paired-end output looks like this:
    # Input Read Pairs: 2070441 Both Surviving: 1605329 (77.54%) Forward Only Surviving: 115264 (5.57%) Reverse Only Surviving: 24110 (1.16%) Dropped: 325738 (15.73%)
    # Single-end looks like this:
    # Input Reads: 26118 Surviving: 25950 (99.36%) Dropped: 168 (0.64%)
    while ($line = <IFILE>) {
	chomp $line;
	if ($line =~ /^Input Read Pairs/) {
	    $pe = 1;
	    $line =~ tr/\(%\)//d;
	    @parsed = split ' ', $line;
	    
	    $data{$ifile}{inputreadpairs} = $parsed[3];
	    $data{$ifile}{bothsurviving} = $parsed[6];
	    $data{$ifile}{forwardonlysurviving} = $parsed[11];
	    $data{$ifile}{reverseonlysurviving} = $parsed[16];
	    $data{$ifile}{dropped} = $parsed[19];
	    $data{$ifile}{pct-bothsurviving} = $parsed[7];
	    $data{$ifile}{pct-forwardonlysurviving} = $parsed[12];
	    $data{$ifile}{pct-reverseonlysurviving} = $parsed[17];
	    $data{$ifile}{pct-dropped} = $parsed[20];
	    last;
	} elsif ($line =~ /^Input Reads/) { # single-end
	    $line =~ tr/\(%\)//d;
	    @parsed = split ' ', $line;
	    $data{$ifile}{inputreads} = $parsed[2];
	    $data{$ifile}{surviving} = $parsed[4];
	    $data{$ifile}{dropped} = $parsed[7];
	    $data{$ifile}{pct-surviving} = $parsed[5];
	    $data{$ifile}{pct-dropped} = $parsed[8];
	    last;
	}
    }	    
}

### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $key (keys %{$data{$ifile}}) {
	print "$sample\t$key\t$data{$ifile}{$key}\n";
    }
}
print "STATS End\n";

### Plotting ###
$ofile = "tmp.dat";
$rfile = "tmp.r";
my $height = 480;
my $width = 480;
my $numsamples = $#files;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}

########### single-end plots ################
if (!$pe) {
### relative trim plot ###
print "Generating trimmomatic-pct.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("pct-surviving", "pct-dropped") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq (
  library(ggplot2);
  datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));
  png(filename="trimmomatic-pct.png", height = $height, width = $width);

cols <- c("pct-bothsurviving" = "#b50000", "pct-forwardonlysurviving" = "#8500b5", "pct-reverseonlysurviving" = "#0003b5", "pct-dropped" = "#00b2b5")

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab("Sample") + ylab("Percent of reads") + theme(legend.position="top") + theme(legend.title=element_blank()) + scale_fill_manual(values=cols, name="", labels=c("Surviving", "R1only", "R2only", "Dropped")) + coord_cartesian(ylim = c(0, 100));

  dev.off();
  #eof) . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### absolute trim plot ###
print "Generating trimmomatic.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("surviving", "dropped") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE "
  library(ggplot2);
  library(scales);
  datat <- read.table(\"$ofile\", header=T, colClasses=c(\"sample\"=\"factor\"));
  png(filename=\"trimmomatic.png\", height = $height, width = $width);

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Total reads\") + theme(legend.position=\"top\") + theme(legend.title=element_blank()) + scale_fill_manual(values=c(\"#0000FF\", \"#7F7F7F\"), name=\"\", breaks=c(\"surviving\", \"dropped\"), labels=c(\"Surviving\", \"Dropped\")) + scale_y_continuous(labels = comma);

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");


} else {


############ paired-end plots ##############

### relative trim plot ###
print "Generating trimmomatic-pct.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("pct-bothsurviving", "pct-forwardonlysurviving", "pct-reverseonlysurviving", "pct-dropped") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq (
  library(ggplot2);
  datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));
  png(filename="trimmomatic-pct.png", height = $height, width = $width);

cols <- c("pct-bothsurviving" = "#00008B", "pct-forwardonlysurviving" = "#0000FF", "pct-reverseonlysurviving" = "#8888FF", "pct-dropped" = "#7F7F7F")

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab("Sample") + ylab("Percent of reads") + theme(legend.position="top") + theme(legend.title=element_blank()) + scale_fill_manual(values=cols, name="", breaks=c("pct-bothsurviving", "pct-forwardonlysurviving", "pct-reverseonlysurviving", "pct-dropped"), labels=c("Both kept", "R1 kept", "R2 kept", "Both dropped")) + coord_cartesian(ylim = c(0, 100));

  dev.off();
  #eof) . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### absolute trim plot ###
print "Generating trimmomatic.png\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample\tdata\tvalue\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    foreach $data ("bothsurviving", "forwardonlysurviving", "reverseonlysurviving", "dropped") {
	print OFILE "$sample\t$data\t$data{$ifile}{$data}\n";
    }
}
close OFILE;

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq (
  library(ggplot2);
  library(scales);
  datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));
  png(filename="trimmomatic.png", height = $height, width = $width);

#c("#00008B", "#0000FF", "#8888FF", "#7F7F7F"), 
cols <- c("bothsurviving" = "#00008B", "forwardonlysurviving" = "#0000FF", "reverseonlysurviving" = "#8888FF", "dropped" = "#7F7F7F")

  p <- ggplot(datat, aes(x=sample, y=value, fill=data)) 
  p + geom_bar(stat='identity') + theme(axis.text.x = element_text(angle = 90)) + xlab("Sample") + ylab("Total reads") + theme(legend.position="top") + theme(legend.title=element_blank()) + scale_fill_manual(values=cols, name="", breaks=c("bothsurviving", "forwardonlysurviving", "reverseonlysurviving", "dropped"), labels=c("Both kept", "R1 dropped", "R2 dropped", "Both dropped")) + scale_y_continuous(labels = comma);

  dev.off();
  #eof) . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

}
