#!/usr/bin/perl -w

#####################################
# vcf--scatterplot.pl
# John Garbe
# June 2018
#####################################

=head1 DESCRIPTION

Take a multi-sample vcf file and plot genotype calls. Apply some usefull filteres. Designed for "hoffseq" amplicon genotype data.

=head1 SYNOPSIS

vcf-scatterplot.pl --vcf file

=head1 OPTIONS

Options:

 --vcf file : vcf input file
 --help : Print usage instructions and exit
 --verbose : Print more information while running
 --minreads 10 : Minimum reads to call genotype
 --minquality 99 : Minimum GATK genotype quality score
 --samplecallrate .7 : Minimum fraction of genotype calls for a sample
 --variantcallrate .7 : Minimum fraction of genotype calls for an assay
 --badsamples file : list of sample names to exclude from pass/fail rate
 --badassays file : list of assay names to exclude from pass/fail rate

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );
use lib "$ENV{GOPHERBIOTOOLS_HOME}/bin";
require "snp_hwe.pl";

# set defaults
$args{threads} = 10;
$args{minreads} = 10;
$args{minquality} = 99;
$args{samplecallrate} = .7;
$args{variantcallrate} = .7;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "vcf=s" => \$args{vcffile},
	   "badsamples=s" => \$args{badsamplesfile},
	   "badassays=s" => \$args{badvariantsfile},
	   "minreads=i" => \$args{minreads},
	   "minquality=i" => \$args{minquality},
	   "samplecallrate=f" => \$args{samplecallrate},
	   "variantcallrate=f" => \$args{variantcallrate},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
$args{vcffile} = abs_path($args{vcffile}) if ($args{vcffile});

### Handle Parameters ###

if (! $args{vcffile}) {
    print "--vcf is required\n";
    pod2usage();
}

die "Cannot open file $args{vcffile}\n" unless (-r $args{vcffile});

# use GATK to generate table of genotype calls in A/A, A/C, CC format
$loadgatk = "";
`which gatk &> /dev/null`;
if ($?) {
    $loadgatk = "module load gatk/4.0.11 2>&1; ";
}
$tablefile = "vcf-scatterplot.tmpAT";
$result = `$loadgatk gatk VariantsToTable --variant $args{vcffile} --output $tablefile -GF GT -F CHROM -F POS 2>&1`;
if ($?) {
    print $result;
    die "Error using gatk VariantsToTable\n";
}

# read in genotypes
open IFILE, "$tablefile" or die "cannot open $tablefile: $!\n";
$header = <IFILE>;
chomp $header;
@header = split /\t/, $header;
$chrom = shift @header; # CHROM
$pos = shift @header; # POS
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    $chrom = shift @line;
    $pos = shift @line;
    $variant = "$chrom-$pos";
    $variants{$variant} = 1;

    for $i (0..$#header) {
	if (! defined($line[$i])) {
	    print "not defined: $i\n";
	}
	$sample = $header[$i];
	$sample =~ s/\.GT$//;
	$genotypesAT{$sample}{$variant} = $line[$i];
    }
}
close IFILE;

# use vcftools to generate table of genotype calls in 0/0, 0/1, 1/1 format
$loadvcftools = "";
`which vcftools &> /dev/null`;
if ($?) {
    $loadvcftools = "module load vcftools 2>&1; ";
}
$tablefile = "vcf-scatterplot.tmp";
$result = `$loadvcftools vcftools --vcf $args{vcffile} --extract-FORMAT-info GT --out vcf-scatterplot 2>&1`;
if ($?) {
    print $result;
    die "Error using vcftools\n";
}

# read in genotypes
$genotypefile = "vcf-scatterplot.GT.FORMAT";
open IFILE, "$genotypefile" or die "cannot open $genotypefile: $!\n";
$header = <IFILE>;
chomp $header;
@header = split /\t/, $header;
shift @header; # CHROM
shift @header; # POS
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    $chrom = shift @line;
    $pos = shift @line;
    $variant = "$chrom-$pos";
    $variants{$variant} = 1;

    for $i (0..$#header) {
	if (! defined($line[$i])) {
	    print "not defined: $i\n";
	}
	$line[$i] =~ s/\|/\//; # convert 0|0 to 0/0
	my ($a1, $a2) = split /\//, $line[$i];
	$sample = $header[$i];
	$sample =~ s/\.AD$//;
	$samples{$sample} = 1;
	$genotypes{$sample}{$variant} = $line[$i];
	$allelefreqcounts{$variant}[$a1]++ unless ($a1 eq ".");
	$allelefreqcounts{$variant}[$a2]++ unless ($a2 eq ".");
    }
}
close IFILE;

### read in lists of bad samples and bad variants ###
$goodsamples = keys %samples;
if ($args{badsamplesfile}) {
    open IFILE, $args{badsamplesfile} or die "Cannot open badsamples file $args{badsamplesfile}: $!\n";
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$sample = $line[0];
	$value = "Fail";
	if ($#line > 0) {
	    $value = $line[1];
	}
	if (!(defined($samples{$sample}))) {
	    $sample =~ s/[^a-z0-9]/\./ig; # sanitize sample ids the same way gopher-pipelines sanitizes things
	}
	if (!(defined($samples{$sample}))) {
	    print "Warning: sample $sample in badsamples file not found in vcf file\n";
	    next;
	}
	$badsamples{$sample} = $value;
	$goodsamples--;
    }
}
$goodvariants = keys %variants;
if ($args{badvariantsfile}) {
    open IFILE, $args{badvariantsfile} or die "Cannot open badassays file $args{badvariantsfile}: $!\n";
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$variant = $line[0];
	$value = "Fail";
	if ($#line >0) {
	    $value = $line[1];
	}
	if (!(defined($variants{$variant}))) {
	    print "Warning: assay $variant in badassays file not found in vcf file\n";
	    next;
	}
	$badvariants{$variant} = 1;
	$goodvariants--;
    }
}
foreach $sample (keys %samples) {
    $badsamples{$sample} = "Pass" unless ($badsamples{$sample});
}
foreach $variant (keys %variants) {
    $badvariants{$variant} = "Pass" unless ($badvariants{$variant});
}

# use GATK to generate table of genotype quality
$tablefile = "vcf-scatterplot.tmp1";
$result = `$loadgatk gatk VariantsToTable --variant $args{vcffile} --output $tablefile -GF GQ -F CHROM -F POS 2>&1`;
if ($?) {
    print $result;
    die "Error using gatk VariantsToTable\n";
}

# read in genotype quality
open IFILE, "$tablefile" or die "cannot open $tablefile: $!\n";
$header = <IFILE>;
chomp $header;
@header = split /\t/, $header;
shift @header; # CHROM
shift @header; # POS
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    $chrom = shift @line;
    $pos = shift @line;
    $variant = "$chrom-$pos";

    for $i (0..$#header) {
	if (! defined($line[$i])) {
	    print "not defined: $i\n";
	}
	$sample = $header[$i];
	$sample =~ s/\.GQ$//;
	$quality = $line[$i];
	$quality = 0 if ($quality eq "NA");	
	$quality{$sample}{$variant} = $quality;
    }
}
close IFILE;

# use GATK to generate table of allele depths
$tablefile = "vcf-scatterplot.tmp2";
$result = `$loadgatk gatk VariantsToTable --variant $args{vcffile} --output $tablefile -GF AD -F CHROM -F POS 2>&1`;
if ($?) {
    print $result;
    die "Error using gatk VariantsToTable\n";
}

# read in allele depths
open IFILE, "$tablefile" or die "cannot open $tablefile: $!\n";
$header = <IFILE>;
chomp $header;
@header = split /\t/, $header;
shift @header; # CHROM
shift @header; # POS
while ($line = <IFILE>) {
    chomp $line;
    @line = split /\t/, $line;
    $chrom = shift @line;
    $pos = shift @line;
    $variant = "$chrom-$pos";    

    for $i (0..$#header) {
	if (! defined($line[$i])) {
	    print "not defined: $i\n";
	}
	
	my @counts = split /,/, $line[$i];
	$sample = $header[$i];
	$sample =~ s/\.AD$//;
	$allelecounts{$sample}{$variant}[0] = $counts[0] // 0;
	$allelecounts{$sample}{$variant}[1] = $counts[1] // 0;
	$allelecounts{$sample}{$variant}[2] = $counts[2] // 0;
	$allelecounts{$sample}{$variant}[3] = $counts[3] // 0;
	$allelecountsbyvariant{$variant}[0] += $counts[0] // 0;
	$allelecountsbyvariant{$variant}[1] += $counts[1] // 0;
	$allelecountsbyvariant{$variant}[2] += $counts[2] // 0;
	$allelecountsbyvariant{$variant}[3] += $counts[3] // 0;

    }
}
close IFILE;

### eh, do this later down below ###
# use GATK to generate table of genotype calls for customer
#$tablefile = "vcf-scatterplot.genotypes.txt";
#$result = `module load gatk/4.0.11; gatk VariantsToTable --variant $args{vcffile} --output $tablefile -GF AD -F CHROM -F POS 2>&1`;

# use GATK to generate table of genotype calls for customer
#$tablefile = "vcf-scatterplot.tmp";
#$result = `module load vcftools; vcftools --vcf $args{vcffile} --extract-FORMAT-info GT --out vcf-scatterplot 2>&1`;
### ###

# initialize to zero anything not already defined
# variantstotable doesn't always return data for all variants or samples
foreach $variant (sort keys %variants) {
    $allelecountsbyvariant{$variant}[0] = 0 unless ($allelecountsbyvariant{$variant}[0]);
    $allelecountsbyvariant{$variant}[1] = 0 unless ($allelecountsbyvariant{$variant}[1]);
    $allelecountsbyvariant{$variant}[2] = 0 unless ($allelecountsbyvariant{$variant}[2]);
    $allelecountsbyvariant{$variant}[3] = 0 unless ($allelecountsbyvariant{$variant}[3]);
    $allelefreqcounts{$variant}[0] = 0 unless ($allelefreqcounts{$variant}[0]);
    $allelefreqcounts{$variant}[1] = 0 unless ($allelefreqcounts{$variant}[1]);
    $allelefreqcounts{$variant}[2] = 0 unless ($allelefreqcounts{$variant}[2]);
    $allelefreqcounts{$variant}[3] = 0 unless ($allelefreqcounts{$variant}[3]);
    
    foreach $sample (sort keys %samples) { 
	$allelecounts{$sample}{$variant}[0] = 0 unless ($allelecounts{$sample}{$variant}[0]);
	$allelecounts{$sample}{$variant}[1] = 0 unless ($allelecounts{$sample}{$variant}[1]);
	$allelecounts{$sample}{$variant}[2] = 0 unless ($allelecounts{$sample}{$variant}[2]);
	$allelecounts{$sample}{$variant}[3] = 0 unless ($allelecounts{$sample}{$variant}[3]);
	$genotypes{$sample}{$variant} = "./." unless ($genotypes{$sample}{$variant});
	$quality{$sample}{$variant} = "0" unless ($quality{$sample}{$variant});
    }
}

### We now have this data: ###
# %samples and %variants
# $allelecounts{$sample}{$variant}[0-3]
# $genotypes{$sample}{$variant}
# $quality{$sample}{$variant}
# $hwe{$variant}; actually don't have this yet

# apply filters
foreach $variant (sort keys %variants) {
    $nocalls = 0;
    foreach $sample (sort keys %samples) { 

	$filter{$sample}{$variant} = "OK";

	$quality = $quality{$sample}{$variant};
	$filter{$sample}{$variant} = "lowquality" if ($quality < $args{minquality});

	# min reads
	$depth = 0;
	foreach $count (@{$allelecounts{$sample}{$variant}}) {
	    $depth += $count;
	}
	$filter{$sample}{$variant} = "lowdepth" if ($depth < $args{minreads});
	$filter{$sample}{$variant} = "nocall" if ($genotypes{$sample}{$variant} eq "./.");
	$nocalls++ if (($filter{$sample}{$variant} ne "OK") && ($badsamples{$sample} eq "Pass"));
    }
    $variants{$variant} = 1 - ($nocalls / $goodsamples);
}

# calculate sample call rate
foreach $sample (sort keys %samples) {
    $nocalls = 0;
    foreach $variant (sort keys %variants) {
	$nocalls++ if (($filter{$sample}{$variant} ne "OK") && ($badvariants{$variant} eq "Pass"));
    }

    $samples{$sample} = 1 - ($nocalls / $goodvariants);
}

foreach $variant (sort keys %variants) {
    foreach $sample (sort keys %samples) { 
	if ($samples{$sample} < $args{samplecallrate}) {
	    $filter{$sample}{$variant} = "samplecallrate";
	}
	if ($variants{$variant} < $args{variantcallrate}) {
	    $filter{$sample}{$variant} = "variantcallrate";
	}
    }
}

# print out genotype calls for customer
$datafile3 = "genotypes.txt";
open OFILE3, ">$datafile3" or die "cannot open $datafile3: $!\n";
print OFILE3 "Variant"; #"Sample\tVariant\tGenotype\tFilter\tSkew\tSignal\n";
foreach $sample (sort keys %samples) {
    print OFILE3 "\t$sample";
}
print OFILE3 "\n";
foreach $variant (sort keys %variants) {
    print OFILE3 "$variant";
    foreach $sample (sort keys %samples) {
	if ($filter{$sample}{$variant} eq "OK") {
	    $genotype = $genotypesAT{$sample}{$variant};
	} else {
	    $genotype = "-";
	}
	print OFILE3 "\t$genotype";
    }
    print OFILE3 "\n";
}
close OFILE3;

# reads per sample allele per assay for Cody
$datafile3 = "reads-sample-allele-assay.txt";
open OFILE3, ">$datafile3" or die "cannot open $datafile3: $!\n";
print OFILE3 "Assay";
foreach $variant (sort keys %variants) {
    print OFILE3 "\t$variant-ref";
    print OFILE3 "\t$variant-alt1";
    print OFILE3 "\t$variant-alt2" if (defined($allelcountsbyvariant{$variant}[2]));
    print OFILE3 "\t$variant-alt3" if (defined($allelcountsbyvariant{$variant}[3]));
}
print OFILE3 "\n";

foreach $sample (sort keys %samples) {
    print OFILE3 "$sample";
    foreach $variant (sort keys %variants) {
	print OFILE3 "\t$allelecounts{$sample}{$variant}[0]";
	print OFILE3 "\t$allelecounts{$sample}{$variant}[1]";
	print OFILE3 "\t$allelecounts{$sample}{$variant}[2]" if (defined($allelcountsbyvariant{$variant}[2]));
	print OFILE3 "\t$allelecounts{$sample}{$variant}[3]" if (defined($allelcountsbyvariant{$variant}[3]));
    }
    print OFILE3 "\n";
}
close OFILE3;

# print out genotype calls and skew-signal data for plotting
$datafile = "ref-alt.dat";
open OFILE, ">$datafile" or die "cannot open $datafile: $!\n";
print OFILE "Sample\tVariant\tGenotype\tFilter\tRefreads\tAltreads\tAlleles\tType\n";

# skew-signal plot
$datafile2 = "skew-signal.dat";
open OFILE2, ">$datafile2" or die "cannot open $datafile2: $!\n";
print OFILE2 "Sample\tVariant\tGenotype\tFilter\tSkew\tSignal\tAlleles\tType\n";

# Plot skew by total reads, color by genotype, facet_wrap by variant
@multiallelic = ();
foreach $variant (sort keys %variants) {
    $ref = $allelecountsbyvariant{$variant}[0];
    $alt1 = $allelecountsbyvariant{$variant}[1];
    $alt2 = $allelecountsbyvariant{$variant}[2]; 
    $alt3 = $allelecountsbyvariant{$variant}[3];
#    $totalcounts = $ref + $alt1 + $alt2 + $alt3;

    # determine ref and alt allele
    $sort{0} = $allelefreqcounts{$variant}[0] // 0;
    $sort{1} = $allelefreqcounts{$variant}[1] // 0;
    $sort{2} = $allelefreqcounts{$variant}[2] // 0;
    $sort{3} = $allelefreqcounts{$variant}[3] // 0;
    $totalalleles = $sort{0} + $sort{1} + $sort{2} + $sort{3};

#    $sort{0} = $ref;
#    $sort{1} = $alt1;
#    $sort{2} = $alt2;
#    $sort{3} = $alt3;

    @sortarray = sort { if ($sort{$b} == $sort{$a}) { return ($a <=> $b) } else { $sort{$b} <=> $sort{$a} } } keys %sort;
    $refindex = $sortarray[0];
    $altindex = $sortarray[1];
    if ($refindex > $altindex) {
	$tmp = $refindex;
	$refindex = $altindex;
	$altindex = $tmp;
    }

#    print "$variant: $ref, $alt1, $alt2, $alt3 : $refindex, $altindex\n";
#    print "$variant: $sort{0}, $sort{1}, $sort{2}, $sort{3} : $refindex, $altindex, $totalalleles\n";

    # biallelic if two alleles have > 95% of all reads 
#    if (($totalcounts > 0) and (($allelecountsbyvariant{$variant}[$refindex] + $allelecountsbyvariant{$variant}[$altindex]) / $tota;counts < .95)) {
    if ( ($totalalleles > 0) and (($allelefreqcounts{$variant}[$refindex] + $allelefreqcounts{$variant}[$altindex]) / $totalalleles < .99) and ($variants{$variant} >= $args{variantcallrate}) ) {
	print "Multialleleic: $variant\n";
	$multiallelic{$variant} = 1;
    }

    foreach $sample (sort keys %samples) {

	if ($multiallelic{$variant}) {
	    if ($ref > 0) {
		&printline($sample, $variant, 0, 1, "multiallelic") if ($alt1 > 0);
		&printline($sample, $variant, 0, 2, "multiallelic") if ($alt2 > 0);
		&printline($sample, $variant, 0, 3, "multiallelic") if ($alt3 > 0);
	    }
	    if ($alt1 > 0) {
		&printline($sample, $variant, 1, 2, "multiallelic") if ($alt2 > 0);
		&printline($sample, $variant, 1, 3, "multiallelic") if ($alt3 > 0);
	    }
	    if (($alt2 > 0) and ($alt3 > 0)) {
		&printline($sample, $variant, 2, 3, "multiallelic");
	    }
	} else {
	    &printline($sample, $variant, $refindex, $altindex, "biallelic");
	}
    }
}

# calculate hardy-wienberg p-value
if (1) {
$datafilehwe = "hwe.txt";
open OFILEHWE, ">$datafilehwe" or die "cannot open $datafilehwe: $!\n";
print OFILEHWE "Variant\tH-W P-Value\t0/0\t0/1\t1/1\t./.\n";
foreach $variant (sort keys %variants) {
    $hets = $hwe{$variant}{"0/1"} // 0;
    $homs1 = $hwe{$variant}{"0/0"} // 0;
    $homs2 = $hwe{$variant}{"1/1"} // 0;
    $nc = $hwe{$variant}{"./."} // 0;
    $hwe = &snphwe($hets, $homs1, $homs2);
    $name = $variant;
    $name =~ s/-.*//;
    print OFILEHWE "$name\t$hwe\t$homs1\t$hets\t$homs2\t$nc\n";
}

close OFILEHWE;
}

### Plotting genotype calls  ###
$name = "ref-alt";
print "Generating $name.png...\n";

$rfile = "$name.r";
$numvariants = keys %variants;
$numsamples = keys %samples;
$mywidth = 1000;
if ($numvariants > 100) {
    $mywidth = 1000 + int(sqrt($numvariants - 100)+1) * 100;
}
$multiwidth = "500";
$alpha = "";
$alpha = $numsamples / 500;
$alpha = 1 if ($alpha < 1);
$alpha = 10 if ($alpha > 10);
$alpha = "alpha = 1/$alpha";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
datat <- read.table('$datafile', header=TRUE, colClasses=c("Refreads"="numeric", "Altreads"="numeric"))
library(ggplot2);

datat\$data <- factor(datat\$Genotype, levels = c('0/0', '0/1', '1/1', './.') )
bidatat <- subset(datat,Type=="biallelic")
passdatat <- subset(bidatat,Filter!="variantcallrate")
faildatat <- subset(bidatat,Filter=="variantcallrate")

# pass filter = filled dot, failed filter = open dot
png(filename="$name.png",height=$mywidth,width=$mywidth)
 ggplot(data=subset(passdatat,Filter=="OK"), aes(x=Refreads, y=Altreads, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Ref Reads') + xlab('Alt Reads') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) +  geom_point(data=subset(passdatat,Filter!="OK"), aes(x=Refreads, y=Altreads, color=Genotype, shape=1), size=.5, $alpha) + scale_shape_identity() + facet_wrap(~Variant)


# pass filter = filled dot, failed filter = open dot
png(filename="$name-fail.png",height=$mywidth,width=$mywidth)
 ggplot(data=faildatat, aes(x=Refreads, y=Altreads, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Ref Reads') + xlab('Alt Reads') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) + scale_shape_identity() + facet_wrap(~Variant)

multidatat <- subset(datat,Type=="multiallelic")
passdatat <- subset(multidatat,Filter!="variantcallrate")
faildatat <- subset(multidatat,Filter=="variantcallrate")
multiheight <- 500
print(multiheight)
if (nrow(passdatat) > 0) {
# pass filter = filled dot, failed filter = open dot
png(filename="$name-multi.png",height=multiheight,width=$multiwidth)
 ggplot(data=subset(passdatat,Filter=="OK"), aes(x=Refreads, y=Altreads, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Ref Reads') + xlab('Alt Reads') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) +  geom_point(data=subset(passdatat,Filter!="OK"), aes(x=Refreads, y=Altreads, color=Genotype, shape=1), size=.5, $alpha) + scale_shape_identity() + facet_grid(vars(Variant), vars(Alleles))
}

multiheight <- 500
print(multiheight)
if (nrow(faildatat) > 0) {
# pass filter = filled dot, failed filter = open dot
png(filename="$name-multi-fail.png",height=multiheight,width=$multiwidth)
 ggplot(data=faildatat, aes(x=Refreads, y=Altreads, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Ref Reads') + xlab('Alt Reads') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) + scale_shape_identity() + facet_grid(vars(Variant), vars(Alleles))
}
# with dot sizing
#ggplot(datat, aes(x=Refreads, y=Altreads, color=Genotype)) + geom_point(aes(size=Quality)) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Ref Reads') + xlab('Alt Reads') + facet_wrap(~Variant) + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00"))

 dev.off()
  #eof)
    ;

    close RFILE;
    system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### Plotting skew-signal  ###
$name = "skew-signal";
print "Generating $name.png...\n";

$rfile = "$name.r";
$numvariants = keys %variants;
$mywidth = 1000;
if ($numvariants > 100) {
    $mywidth = 1000 + int(sqrt($numvariants - 100)+1) * 100;
}

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
datat <- read.table('$datafile2', header=TRUE, colClasses=c("Skew"="numeric", "Signal"="numeric"))
library(ggplot2);

datat\$data <- factor(datat\$Genotype, levels = c('0/0', '0/1', '1/1', './.') )
bidatat <- subset(datat,Type=="biallelic")
passdatat <- subset(bidatat,Filter!="variantcallrate")
faildatat <- subset(bidatat,Filter=="variantcallrate")

# pass filter = filled dot, failed filter = open dot
png(filename="$name.png",height=$mywidth,width=$mywidth)
 ggplot(data=subset(passdatat,Filter=="OK"), aes(x=Skew, y=Signal, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Reads') + xlab('Alt <-- --> Ref') + facet_wrap(~Variant) + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) +  geom_point(data=subset(passdatat,Filter!="OK"), aes(x=Skew, y=Signal, color=Genotype, shape=1), size=.5, $alpha) + scale_shape_identity() + xlim(-1,1) + annotate("rect", xmin = -.8, xmax = -.4, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = 0.4, xmax = .8, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = -Inf, xmax = Inf, ymin = 0, ymax = $args{minreads}, alpha = .2)

png(filename="$name-fail.png",height=$mywidth,width=$mywidth)

datat\$data <- factor(datat\$Genotype, levels = c('0/0', '0/1', '1/1', './.') )
# pass filter = filled dot, failed filter = open dot
 ggplot(faildatat, aes(x=Skew, y=Signal, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Reads') + xlab('Alt <-- --> Ref') + facet_wrap(~Variant) + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) + scale_shape_identity() + xlim(-1,1) + annotate("rect", xmin = -.8, xmax = -.4, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = 0.4, xmax = .8, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = -Inf, xmax = Inf, ymin = 0, ymax = $args{minreads}, alpha = .2)


multidatat <- subset(datat,Type=="multiallelic")
passdatat <- subset(multidatat,Filter!="variantcallrate")
faildatat <- subset(multidatat,Filter=="variantcallrate")

multiheight <- 500
print(multiheight)
if (nrow(passdatat) > 0) {
# pass filter = filled dot, failed filter = open dot
png(filename="$name-multi.png",height=multiheight,width=$multiwidth)
 ggplot(data=subset(passdatat,Filter=="OK"), aes(x=Skew, y=Signal, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Reads') + xlab('Alt <-- --> Ref') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) +  geom_point(data=subset(passdatat,Filter!="OK"), aes(x=Skew, y=Signal, color=Genotype, shape=1), size=.5, $alpha) + scale_shape_identity() + xlim(-1,1) + annotate("rect", xmin = -.8, xmax = -.4, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = 0.4, xmax = .8, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = -Inf, xmax = Inf, ymin = 0, ymax = $args{minreads}, alpha = .2) + facet_grid(vars(Variant), vars(Alleles))
}
multiheight <- 500
print(multiheight)
if (nrow(faildatat) > 0) {
png(filename="$name-multi-fail.png",height=multiheight,width=$multiwidth)
datat\$data <- factor(datat\$Genotype, levels = c('0/0', '0/1', '1/1', './.') )
# pass filter = filled dot, failed filter = open dot
 ggplot(faildatat, aes(x=Skew, y=Signal, color=Genotype)) + geom_point(size=.5, $alpha) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Reads') + xlab('Alt <-- --> Ref') + scale_color_manual(values=c("#D80000", "#00A1FF", "#00DD00", "#FFBF00")) + scale_shape_identity() + xlim(-1,1) + annotate("rect", xmin = -.8, xmax = -.4, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = 0.4, xmax = .8, ymin = 0, ymax = Inf, alpha = .2) + annotate("rect", xmin = -Inf, xmax = Inf, ymin = 0, ymax = $args{minreads}, alpha = .2) + facet_grid(vars(Variant), vars(Alleles))
}
 dev.off()

  #eof
);

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");


### variant call rate plot ###
$datafile = "variantcallrate.dat";
open OFILE, ">$datafile" or die "cannot open $datafile: $!\n";
print OFILE "Name\tValue\tQC\n";

foreach $variant (sort keys %variants) {
    print OFILE "$variant\t$variants{$variant}\t$badvariants{$variant}\n";
}

$name = "variantcallrate";
print "Generating $name.png...\n";

$rfile = "$name.r";
$numvariants = keys %variants;
$mywidth = 1000;
if ($numvariants > 100) { # 100-200 samples
    $mywidth = $numvariants * 10;
} if ($numvariants > 200) { # more than 200 samples
    $mywidth = 2000 + int(sqrt($numvariants - 200)+1) * 100;
}

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
datat <- read.table('$datafile', header=TRUE, colClasses=c("Name"="factor", "Value"="numeric", "QC"="factor"))
png(filename="$name.png",height=300,width=$mywidth)
library(ggplot2);

 ggplot(datat, aes(reorder(Name, -Value), Value)) + geom_bar(stat='identity', aes(fill=QC)) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Fraction passing filter') + xlab('Variant') + geom_hline(yintercept=$args{variantcallrate}, linetype="dashed", color = "red")

 dev.off()
  #eof)
    ;

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");

### sample call rate plot ###
$datafile = "samplecallrate.dat";
open OFILE, ">$datafile" or die "cannot open $datafile: $!\n";
print OFILE "Name\tValue\tQC\n";

foreach $sample (sort keys %samples) {
    print OFILE "$sample\t$samples{$sample}\t$badsamples{$sample}\n";
}

$name = "samplecallrate";
print "Generating $name.png...\n";

$rfile = "$name.r";
$numvariants = keys %variants;
$mywidth = 1000;
if ($numvariants > 100) {
    $mywidth = 1000 + int(sqrt($numvariants - 100)+1) * 100;
}

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
datat <- read.table('$datafile', header=TRUE, colClasses=c("Name"="factor", "Value"="numeric", "QC"="factor"))
png(filename="$name.png",height=300,width=$mywidth)
library(ggplot2);

 ggplot(datat, aes(reorder(Name, -Value), Value)) + geom_bar(stat='identity', aes(fill=QC)) + theme(axis.text.x=element_text(angle=45, hjust=1)) + ylab('Fraction passing filter') + xlab('Sample') + geom_hline(yintercept=$args{samplecallrate}, linetype="dashed", color = "red")

 dev.off()
  #eof)
    ;

close RFILE;
system("R --no-restore --no-save --no-readline < $rfile > $rfile.out");



sub printline {
    ($sample, $variant, $refindex, $altindex, $allelic) = @_;
	
    $refcount = $allelecounts{$sample}{$variant}[$refindex];
    $altcount = $allelecounts{$sample}{$variant}[$altindex];

    $myrefcount = $refcount;
    $myaltcount = $altcount;
    $refaltcap = 200;
    $myrefcount = $refaltcap if ($refcount > $refaltcap); # cap so plots are reasonable
    $myaltcount = $refaltcap if ($altcount > $refaltcap); 

    if ($filter{$sample}{$variant} eq "OK") {
	$genotype = $genotypes{$sample}{$variant};
    } else {
	$genotype = "./.";
    }
#    $quality = $quality{$sample}{$variant}; # not using this
    $genotype = "$refindex/$altindex" if ($genotype eq "$altindex/$refindex");

    return unless (($genotype eq "$refindex/$refindex") or ($genotype eq "$refindex/$altindex") or ($genotype eq "$altindex/$altindex") or ($genotype eq "./."));
    $genotype =~ s/$refindex/0/g;
    $genotype =~ s/$altindex/1/g;
    $genotype =~ s/\|/\//; # convert 0|0 to 0/0
    $filter = $filter{$sample}{$variant};
    print OFILE "$sample\t$variant\t$genotype\t$filter\t$myrefcount\t$myaltcount\t$refindex-$altindex\t$allelic\n";

    # skew-signal
    $signal = $refcount + $altcount;
    $skew = ($signal == 0) ? 0 : 1 - ($altcount / $signal) * 2;
    $signal = 200 if ($signal > 200);
    print "Error: $variant $sample $skew $refcount $altcount\n" if (($skew > 1) or ($skew < -1));
    print OFILE2 "$sample\t$variant\t$genotype\t$filter\t$skew\t$signal\t$refindex-$altindex\t$allelic\n";
    
    $hwe{$variant}{$genotype}++;
}
