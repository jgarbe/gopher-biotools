#!/usr/bin/perl -w

#######################################################################
#  Copyright 2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastqreadestimate.pl - Generate an accurate estimate of the number of reads in a fastq file, useful for very large fast files that take a long time to run wc on. This script grabs the first 10 and last 10 reads (40 lines each) from the input file and calculates the average read size based on those 20 reads. The number of reads in the file is estimated as the size (in bytes) of the file diveded by the average read size (in bytes). Typically only the read ID line length varies slightly from read to read, so the estimated number of reads is quite accurate (>99.9%). However, if your input file has more variability in bytes-per-read, or the first and last reads in the file are not representative, then the estimated number of reads won't be accurate. Files compressed with gzip with a .gz extension are supported, but average read size is based on the first 20 reads in the file only resulting in lower accuracy.

=head1 SYNOPSIS

fastqreadestimate.pl [-v] [-n reads] file.fastq
fastqreadestimate.pl [-v] [-n reads] file.fastq.gz
fastqreadestimate.pl -h

=head1 DESCRIPTION

    -n integer : estimate bytes per read from n reads (Default 20)
    -v : verbose output

=head1 EXAMPLE

Run the script::

    $ fastqreadestimate.pl file.fastq

file.fastq: 51246 reads, 50 bp read length

=cut

############################# Main  ###############################

use Cwd 'abs_path';
use Getopt::Std;
use Pod::Usage;
use File::Temp qw/ tempdir /;
use File::Basename;

our ($opt_h, $opt_v, $opt_n);

$usage = "USAGE: fastqreadlength.pl file.fastq\n";
die $usage unless getopts('hvn:');
pod2usage(q(-verbose) => 3) if ($opt_h);
die $usage unless ($#ARGV == 0);

### parameters
my ($ifile) = @ARGV;
$linestosample = ($opt_n // 20) * 4;

open IFILE, $ifile or die "Cannot open file $ifile: $!\n";
close IFILE;

# get true path so this works with symlinks
$ifile = abs_path($ifile);
my ($filename,$path,$suffix) = fileparse($ifile);

$gz = 0;
$gz = 1 if ($filename =~ /\.gz$/);

if ($gz) {
    # get read length
    $length = `gunzip -c $ifile | head -n 2 | sed '2q;d' | wc -c`;
    $length--; # subtract newline

    # get (uncompressed) file size
    @output = `gzip -l $ifile`;
    @result = split ' ', $output[1];
    $size = $result[1];

    # calculate bytes per read (average over first 20 and last 0 reads)
    $firstten = `gunzip -c $ifile | head -n $linestosample | wc -c`;
    chomp $firstten;
    $lastten = 0;
    chomp $lastten;

} else {

    # get read length
    $length = `sed '2q;d' $ifile | wc -c`;
    $length--; # subtract newline

    # get file size
    $result = `ls -lL $ifile`;
    @result = split ' ', $result;
    $size = $result[4];

    # calculate bytes per read (average over first 10 and last 10 reads)
    $linestosample = int($linestosample / 2);
    $firstten = `head -n $linestosample $ifile | wc -c`;
    chomp $firstten;
    $lastten = `tail -n $linestosample $ifile | wc -c`;
    chomp $lastten;
    $linestosample = $linestosample * 2;
}

$bytesperread = ($firstten + $lastten) / $linestosample * 4;

# print out
$reads = int($size / $bytesperread);
if ($opt_v) {
    print "$filename: $reads reads, $length bp read length, $bytesperread bytes per read\n";
} else {
    print "$filename: $reads reads, $length bp read length\n";
}

exit;


############################################################
# Alternative method, reads in reads until standard error of 
# mean bytes-per-read is suitably small. Doesn't work very well
# because of the slight file position dependence on bytes per read
############################################################

if ($gz) {

    # get read length
    $length = `gunzip -c $ifile | head -n 2 | sed '2q;d' | wc -c`;
    $length--; # subtract newline

    # get (uncompressed) file size
    @output = `gzip -l $ifile`;
    @result = split ' ', $output[1];
    $size = $result[1];

    # open fastq file
    open IFILE, "gunzip -c $ifile |" or die "Cannopt open gzip pipe to $ifile: $!\n";
    
} else {

    # get read length
    $length = `sed '2q;d' $ifile | wc -c`;
    $length--; # subtract newline

    # get file size
    $result = `ls -l $ifile`;
    @result = split ' ', $result;
    $size = $result[4];

    # open fastq file
    open IFILE, $ifile or die "Cannot open file $ifile: $!\n";
}

$readcount = 0;
$sum = 0;
while ($line = <IFILE>) {
    $line .= <IFILE>;
    $line .= <IFILE>;
    $line .= <IFILE>;
    
    $length = length($line);
    push @lengths, $length;
    $sum += $length;
    $readcount++;
    if ($readcount % 40 == 0) {
	$mean = $sum / $readcount;
	my $sumsquares = 0; 
	$sumsquares += ( $_ - $mean ) ** 2 for (@lengths);
	$stddev = sqrt( (1 / $readcount) * $sumsquares);
	$stderr = $stddev / sqrt($readcount);
	print "Stderr: $stderr, $mean, " . int($size / $mean) . "\n";
	if (($stderr < .25) or ($readcount > 50)) {
	    $bytesperread = $mean;
	    last;
	}
    }
}

# print out
$reads = int($size / $bytesperread);
if ($opt_v) {
    print "$filename: $reads reads, $length bp read length, $bytesperread bytes per read\n";
} else {
    print "$filename: $reads reads, $length bp read length\n";
}
