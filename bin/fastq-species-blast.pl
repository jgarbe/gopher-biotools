#!/usr/bin/perl -w

#######################################################################
#  Copyright 2012 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastq-species-blast.pl - Given a fastq file, blast a sample of the sequences and count how many hits there are to each species.

=head1 SYNOPSIS

fastq-species-blast.pl [-n number_of_sequences_to_blast] [-t num_threads] [-d blast_database(s)] input.fastq

=head1 DESCRIPTION

fastq-species-blast.pl can be used to blast a small number of fastq reads against a BLAST database in order to determine what species the fastq file contains, and if there are significant amounts of contaminating sequence from other species. The -n option is used to specify how many reads from the input.fastq file shoule be BLASTed (default is 10). The -t option specifies how many processor cores to use (default is 1, this script cannot run across multiple nodes). The -d option specifies which BLAST database to use (default is htgs). Any database installed with the local NCBI Blast installation can be used (the taxdb must be installed). Multiple databases can be blasted against: fastq-species-blast.pl input.fastq -d "human_genomic vector"

=head1 EXAMPLE

fastq-species-blast.pl /home/msistaff/public/garbe/sampledata/RNAseq/Hsap/fastq/heart-1_R1.fastq
6 out of 10 sequences (60%) have a hit in the htgs blast database
    Common name               Scientific name      # of sequences
    grivet                    Chlorocebus aethiops 1
    cattle                    Bos taurus           1
    white-tufted-ear marmoset Callithrix jacchus   1
    human                     Homo sapiens         3

=cut

use Getopt::Std;
use Pod::Usage;

$usage = "Usage: fastq-species-blast.pl [-n number_of_sequences_to_blast] [-t num_threads] [-d blast_database(s)] input.fastq\n";

die $usage if ($#ARGV < 0);

# parameters 
getopts('n:t:d:h', \%opts) or die "$usage\n";;
pod2usage(q(-verbose) => 3) if ($opts{'h'});
die "ERROR: Fastq file must be listed after command-line options\n$usage" if ($#ARGV > 0);
$fastq = $ARGV[0]; # input fastq file
$seqs = $opts{'n'} || 10; # number of sequences to blast
$threads = $opts{'t'} || 1; # number of cores to run on
$database = $opts{'d'} || "htgs"; # database to blast against
# end parameters

$lines = $seqs * 4; # number of lines to pull out of fastq file

$head = "head -n $lines $fastq";
if ($fastq =~ /\.gz/) {
    $head = "gunzip -c $fastq | head -n $lines";
}

# run blast and read in the results as they are generated
open PIPE, "module load fastx_toolkit; module load ncbi_blast+; $head | fastq_to_fasta -Q33 -n | blastn -db $database -outfmt \"6 qseqid sseqid sgi sacc\" -num_threads $threads -max_target_seqs 1 -task megablast -evalue .0001 |" or die "Cannot run blast command: $!\n";

# for each blast hit look up the taxon and keep track of # of hits to each taxon
$count = 0;
while ($line = <PIPE>) {
    $count++;
    chomp $line;
    my ($qseqid, $sseqid, $sgi, $sacc) = split /\t/, $line;
    next if ($seen{$qseqid}{$sseqid});
    $seen{$qseqid}{$sseqid} = 1;
    $taxid = `module load ncbi_blast+; blastdbcmd -db $database -entry $sgi -outfmt \"\%L\t\%S\"`;
    die "Cannot lookup taxid: $!" if ($? == -1);
    chomp $taxid;
    if (defined($taxon{$taxid})) {
	$taxon{$taxid}++;
    } else {
	$taxon{$taxid} = 1;
    }
}

# print out the number of sequences with hits
$actualseqs = `head -n $lines $fastq | wc -l`;
chomp $actualseqs;
$actualseqs = $actualseqs / 4;
$percent = int($count / $actualseqs * 1000) / 10;
print "$count out of $seqs sequences ($percent\%) have a hit in the $database blast database\n";

# print out the number of hits to each taxon
print "Common name\tScientific name\t# of sequences\n";
foreach $value (sort {$taxon{$a} <=> $taxon{$b} } keys %taxon) {
    print "$value\t$taxon{$value}\n";
}

exit 0;
