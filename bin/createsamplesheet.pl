#!/usr/bin/perl -w

#######################################
# createsamplesheet.pl
# John Garbe
# 2014, updated March 2019
#######################################

=head1 NAME

createsamplesheet.pl - Automatically create a samplesheet for a folder of fastq files

=head1 SYNOPSIS

createsamplesheet.pl -f folder

=head1 DESCRIPTION

Automatically generate a samplesheet suitable for use with various gopher-pipeline scripts. 

 -f folder : A folder containing fastq files to process
 -o file : Name of the output samplesheet file
 -z : fastq files are gzip compressed (filenames end with fastq.gz)
 -h : Print usage instructions and exit
 -v : Print more information while running (verbose)

=cut

############################# Main  ###############################

my $starttime = time();
my $checkpointtime = $starttime;

use Getopt::Std;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
#use feature "switch";
use Scalar::Util qw(looks_like_number);
#use FindBin qw($RealBin);

my %samples;
my %args;
my %stats;

our ($opt_f, $opt_o, $opt_h, $opt_v, $opt_z);

die pod2usage unless getopts('f:o:hvzi');
die pod2usage unless ($opt_f);
die pod2usage if ($#ARGV >= 0);
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($opt_h);

### parameters
die "Fastq folder $opt_f not found\n" if (!-e $opt_f);
$fastqfolder = abs_path($opt_f);
$opt_o = abs_path($opt_o) if ($opt_o);
$outputfile = $opt_o // "samplesheet.txt";
print "Creating samplesheet file: $outputfile\n" if ($opt_v);
$verbose = $opt_v // 0; # print out more stuff
$gz = $opt_z // 0;

### process fastq directory
# do a lot of work to get short sample names from the fastq filenames
if ($gz) {
    @files = <$fastqfolder/*.fastq.gz>;
} else {
    @files = <$fastqfolder/*.fastq>;
    if ($#files < 0) {
	print "No fastq files found, looking for compressed files...\n";
	@files = <$fastqfolder/*.fastq.gz>;		
    }
}
die "No fastq files found in $fastqfolder\n" if ($#files < 0);

foreach $file (@files) {
    my ($fname, $path) = fileparse($file);
    if ($fname =~ /_(R[12])[._]/) {
	push @fastqfiles, $file;
    } else {
	print "Ignoring fastq file $file, name doesn't contain _R1 or _R2\n";
    }
}

# (Attempt to) Split the filenames based on illumina format
%samples = ();
$failedfiles = 0;
foreach $file (@fastqfiles) {
    my ($fname, $path, $mysuffix) = fileparse($file, (".fastq", ".fastq.gz"));

    if ($fname =~ /(.*)_([AGCT]{3,8})(-[AGCT]{3,8})?_L00([1-8])_(R[12])_[0-9]{3}$/) {
	# standard old-style filenames
	print STDERR "Filename format: old-style\n" if ($verbose);
	$sample = $1;
	$read = $5;
	$lane = $4;
	$sample = "$sample.L00$lane";
    } elsif ($fname =~ /(.*)_S[\d]{1,4}_L00([1-8])_(R[12])_[0-9]{3}$/) {
	# standard new-style filenames
	print STDERR "Filename format: new-style\n" if ($verbose);
	$sample = $1;
	$read = $3;
	$lane = $2;
	$sample = "$sample.L00$lane";
    } elsif ($fname =~ /(.*)_S[\d]{1,4}_(R[12])_[0-9]{3}$/) {
	# standard new-style concatenated (no lane number) filenames
	print STDERR "Filename format: new-style concatenated\n" if ($verbose);
	$sample = $1;
	$read = $2;
    } elsif ($fname =~ /lane[1-8]_Undetermined_L00([1-8)_(R[12])_[0-9]{3}$/) {
	# old-style undetermined filenames
	print STDERR "Filename format: old-style undetermined\n" if ($verbose);
	$sample = "lane${1}-Undetermined";
	$read = $2;
    } elsif ($fname =~ /Undetermined_S0_(R[12])_[0-9]{3}$/) {
	# new-style undetermined concatenated filenames
	print STDERR "Filename format: new-style undetermined concatenated\n" if ($verbose);
	$sample = "Undetermined";
	$read = $1;
    } else {
	print "Unable to parse $fname, skipping\n";
	$failedfiles++;
	next;
    }

    if (defined($samples{$sample}{$read}{fastq})) {
	die "Error: multiple fastq files with sample sample name: $file\t    $samples{$sample}{$read}{fastq}\n";
    }
    $samples{$sample}{$read}{fastq} = $file;
    $samples{$sample}{$read}{fastqname} = $fname . $mysuffix;
}

if ($failedfiles > 0) {
    print "Filenames don't match Illumina format, attempting alternative file parsing methods...\n";
    # Split the filenames at the first, second, third, etc underscore character, until the split filenames are unique for each sample
    for $i (0..7) {
	%samples = ();
	$pe = 0;
	foreach $file (@fastqfiles) {
	    my ($fname, $path) = fileparse($file);
	    @parts = split /_/, $fname;
	    if ($#parts <= $i) {
		$sample = $fname;
	    } else {
		$sample = join "_", @parts[0..$i];
	    }
#	    $sample =~ s/_/-/g; # convert _ to -
	    if ($fname =~ /_(R[12])[._]/) {
		$read = $1;
		$pe = 1 if ($read eq "R2");
		$samples{$sample}{$read}{fastq} = $file;
		$samples{$sample}{$read}{fastqname} = $fname;
		
	    }
	}
	$samplenumber = keys %samples;
#       print "i: $i, $samplenumber samples; $#fastqfiles fastqfiles\n";
	if ($pe) {
	    last if (keys %samples eq (($#fastqfiles + 1) / 2));
	} else {
	    last if (keys %samples eq ($#fastqfiles + 1));
	}
    }
}

# determine number of R1 and R2 files
$r1count = 0;
$r2count = 0;
foreach $sample (keys %samples) {
    $r1count++ if ($samples{$sample}{R1});
    $r2count++ if ($samples{$sample}{R2});
}
    
if ($r2count == 0) {
    print "Single-end read dataset\n";
    $pe = 0;
} elsif ($r2count == $r1count) {
    print "Paired-end read dataset\n";
    $pe = 1;
} else {
    print "Unequal number of R1 and R2 fastq files found: $#r1count R1 fastq files, $#r2count R2 files. Mixing paired-end and single-end datasets is not supported\n";
    exit;
}

$samplenumber = keys %samples;
print "$samplenumber samples found in folder $fastqfolder\n";
die "No fastq files with parseable format filenames could be found, you'll have to create a samplesheet by hand\n" if ($samplenumber == 0);

# Add group attribute, based on first two letters of sample name
foreach $sample (sort keys %samples) {
    $group = substr $sample, 0,2;
    $stats{$sample}{Group} = $group;
    $groups{$group} = 1; # keep track of groups
    $stats{$sample}{fastqR1} = $samples{$sample}{R1}{fastqname};
    $stats{$sample}{fastqR2} = $samples{$sample}{R2}{fastqname} if ($pe);
    $stats{$sample}{Description} = "Sample-$sample";
    $sanitized = $sample;
    $sanitized =~ s/[^a-z0-9]/\./ig;
    $stats{$sample}{"#SampleID"} = $sanitized;
}

### Print samplesheet ###
# Determine columns to print
@columns = ("#SampleID", "fastqR1");
push @columns, "fastqR2" if ($pe);
push @columns, "Group" if (keys %groups > 1);
push @columns, "Description";

open OFILE, ">$outputfile" or die "Cannot open file $outputfile for writing: $!\n";

foreach $column (@columns) {
    print OFILE "$column\t";
}
print OFILE "\n";

foreach $sample (sort keys %stats) {
    foreach $column (@columns) {
	if (defined ($stats{$sample}{$column})) {
	    print OFILE $stats{$sample}{$column} . "\t";
	} else {
	    print OFILE "undef\t";
	}
    }
    print OFILE "\n";
}
close OFILE;

exit;

################################ Helper subs ###############################


