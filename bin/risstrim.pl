#!/usr/bin/perl -w

##############################################################
# risstrim.pl
# John Garbe
# May 2014
#
# Run trimmomatic
#
# TODO: add gz support, add single-end read support
#
###############################################################

=head1 NAME

risstrim.pl - Run trimmomatic

=head1 SYNOPSIS

risstrim.pl [-p 1 -q 15 -g 2 -n -t] R1.fastq [R2.fastq]

=head1 DESCRIPTION

 Run the trimmomatic trimming program, hiding complexity from users
Trimming parameters: 
-automatic quality encoding detection
-automatic read length detection
-minimum read length: 50%
-4bp sliding window trimming
-Minimum quality score of 3 at beginning and end of read (removes Ns)

 R1.fastq : a fastq file to be trimmed (file extension must be .fastq or .fq)
R2.fastq : The second fastq file from a set of paired-end fastq files
-q 15 : Minimum 4bp sliding window quality score
-t : Trim Trueseq V3 adapter sequences
-n : Trim Nextera adapter sequences (overrides -t option)
-p 1 : Number of processors to use
-g 2 : number of gigabytes of memory to use

=cut

#########################################################

# process parameters
use Getopt::Std;
use File::Basename;
use Pod::Usage;

our ($opt_n, $opt_t, $opt_p, $opt_q, $opt_g, $opt_h);
my ($r1directories, $r2directories);

$usage = "USAGE: risstrim.pl [-p 1 -q 15 -g 2 -n -t -h] R1.fastq R2.fastq\n";
die $usage unless getopts('g:q:tnhp:');
pod2usage(q(-verbose) => 3) if ($opt_h);
if ($#ARGV == 0) {
    $PE = 0;
} elsif ($#ARGV == 1) {
    $PE = 1;
} else {
    die $usage;
}

$threads = $opt_p // 1;
$minscore = $opt_q // 16;
$gigs = $opt_g // 2;
$xmx = "-Xmx" . $gigs * 1000 . "M";

# process file names
@suffixes = (".fastq", ".fq");
$r1file = $ARGV[0];
($r1name, $r1directories, $r1suffix) = fileparse($r1file, @suffixes);
if ($PE) {
    $r2file = $ARGV[1];
    ($r2name, $r2directories, $r2suffix) = fileparse($r2file, @suffixes);
}

# determine adapter sequences to trip
$illuminaclip = "";
if ($opt_n) {
    if ($PE) {
	$illuminaclip = "ILLUMINACLIP:/soft/trimmomatic/0.32/adapters/NexteraPE-PE.fa:2:30:10:2:true";
    } else {
	$illuminaclip = "ILLUMINACLIP:/soft/trimmomatic/0.32/adapters/NexteraPE-PE.fa:2:30:10:2:true";
    }
} elsif ($opt_t) {
    if ($PE) {
	$illuminaclip = "ILLUMINACLIP:/soft/trimmomatic/0.32/adapters/TruSeq3-PE-2.fa:2:30:10:2:true";
    } else {
	$illuminaclip = "ILLUMINACLIP:/soft/trimmomatic/0.32/adapters/TruSeq3-SE.fa:2:30:10:2:true";	
    }
}

# get read length
$length = `sed '2q;d' $r1file | wc -c`;
$length--; # subtract newline
$minlength = int($length / 2);

# run trimmomatic
if ($PE) {
    $command = "module load trimmomatic; java $xmx -jar /soft/trimmomatic/0.32/trimmomatic.jar PE -threads $threads $r1file $r2file $r1name.PE$r1suffix $r1name.SE$r1suffix $r2name.PE$r2suffix $r2name.SE$r2suffix $illuminaclip LEADING:3 TRAILING:3 SLIDINGWINDOW:4:$minscore MINLEN:$minlength";
} else {
    $command = "module load trimmomatic; java $xmx -jar /soft/trimmomatic/0.32/trimmomatic.jar SE -threads $threads $r1file $r1name.SE$r1suffix $illuminaclip LEADING:3 TRAILING:3 SLIDINGWINDOW:4:$minscore MINLEN:$minlength";
}
print "\nRunning this trimmomatic command:\n $command\n\n";
$result = `$command`;
print $result;
