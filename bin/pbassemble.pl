#!/usr/bin/perl -w

###############################################################
#
# John Garbe
# September 2017 - Henry's first day of kindergarden
#
###############################################################

=head1 DESCRIPTION

Assemble a pacbio dataset with 3 different assemblers: Canu, SMARTdenovo, and Abruijn. You must first convert the dataset from raw bam format to ccs fasta and fastq files.

=head2 OUTPUT

A directory for each assembly is generated.

=head1 SYNOPSIS

pbassemble.pl --fastafile file [--fastqfile file] [--threads 40] [--coverage 50] [--genomesize 5m] 

pbassemble.pl --help

=head1 OPTIONS

 --fastafile file : a fasta file of ccs reads
 --fastqfile file : a fastq file of ccs reads
 --threads integer : number of threads to run (cpus to use)
 --coverage integer : estimated X coverage of genome (very rough estimate is fine)
 --genomesize number[g|m|k] : estimated genome size
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

###############################################################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

$args{threads} = 40;
$args{coverage} = 50;
$args{genomesize} = "5m";
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "fastafile:s" => \$args{fastafile},
	   "fastqfile:s" => \$args{fastqfile},
	   "threads:i" => \$args{threads},
	   "covearge:i" => \$args{coverage},
	   "genomesize:s" => \$args{genomesize},
	   
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($args{fastafile});

die "Cannot find fasta file $args{fastafile}\n" unless (-e $args{fastafile});
if (! $args{fastqfile}) {
    $args{fastqfile} = $args{fastafile};
    $args{fastqfile} =~ s/fasta$/fastq/;
}
die "Cannot find fastq file $args{fastqfile}\n" unless (-e $args{fastqfile});
$args{fastafile} = abs_path($args{fastafile});
$args{fastqfile} = abs_path($args{fastqfile});

### Canu ###
$ofile = "canu.sh";
open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
print OFILE qq(#!/bin/bash
module load umgc
module load java
module load canu

# pass fastq file, assembly prefix (-p), assembly directory (-d),  aprox. genome size, and technology used (PacBio)
time canu -p canu -d canu genomeSize=$args{genomesize} -pacbio-raw $args{fastqfile} 

);
close OFILE;

### Abruijn ###
$ofile = "abruijn.sh";
open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
print OFILE qq(#!/bin/bash
module load umgc
module load abruijn

# supply abruijn with fasta file, output directory name, and coverage
time abruijn -t $args{threads} -p pacbio $args{fastafile} abruijn $args{coverage}
);
close OFILE;

### SMARTdenovo ###
$ofile = "smartdenovo.sh";
open OFILE, ">$ofile" or die "cannot open $ofile: $!\n";
print OFILE qq(#!/bin/bash
module load umgc
module load smartdenovo

# supply prefix, threads, consensus sequence is desired, and fasta file
mkdir smartdenovo
cd smartdenovo
smartdenovo.pl -p smartdenovo -t $args{threads} -c 1 $args{fastafile} > smartdenovo.mak
time make -j $args{threads} -f smartdenovo.mak
);
close OFILE;

## Run assemblers ###
$command = "bash canu.sh &> canu.log";
print "$command\n";
`$command`;

$command = "bash abruijn.sh &> abruijn.log";
print "$command\n";
`$command`;

$command = "bash smartdenovo.sh &> smartdenovo.log";
print "$command\n";
`$command`;

# todo: pull stats from results: number of contigs, length of assembly, N50
