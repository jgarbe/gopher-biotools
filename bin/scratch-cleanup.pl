#!/usr/bin/perl -w

#####################################
# scratch-cleanup.pl
# John Garbe
# July 2015
#
# Delete top-level folders in scratch that contain no files (but may contain folders and symlinks)
#
#####################################

use Getopt::Std;
our ($opt_d, $opt_f, $opt_h);

$usage = "USAGE: scratch-cleanup.pl [-d] [-f /path/to/folder]\n";

die $usage unless getopts('dhf:');
die $usage unless ($#ARGV == -1);
die $usage if ($opt_h);

$delete = 0;
$delete = 1 if ($opt_d);
$scratchfolder = $opt_f // "/panfs/roc/scratch/";
die "Cannot find folder $scratchfolder\n" unless (-d $scratchfolder);

$user = `whoami`;
chomp $user;

# get list of top-level folders owned by the user in scratch
@folders = `find $scratchfolder -maxdepth 1 -mindepth 1 -type d -user $user`;
print "find $scratchfolder -maxdepth 1 -mindepth 1 -type d -user $user\n";

#print @folders;

# identify folders containing no regular files, delete if enabled
foreach $folder (sort @folders) {
    chomp $folder;
    print "Checking folder $folder\n";
    @files = `find $folder -type f -print -quit`;
    if ($#files < 0) {
	if ($delete) {
	    print "No files - deleting\n";
	    $result = `rm -r $folder`;
	    print $result;
	} else {
	print "No files\n";
	}
    } else {
#	print $#files + 1 . " files\n";
	print "some files\n";
    }
}

print "Rerun with -d option to delete empty folders\n" unless ($delete);
