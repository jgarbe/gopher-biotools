#!/usr/bin/perl -w

##############################
# s3get.pl
# John Garbe
# September 2015
#
# Download files from an s3 "folder"
#
##############################

use Cwd;
use Getopt::Std;

our ($opt_t);

$usage = "USAGE: s3get.pl [ -t 20 ] s3://bucket/\n";
die $usage unless getopts('t:');
die $usage unless ($#ARGV == 0);
$bucket = shift @ARGV;
$threads = $opt_t // 10;
$objects = 0;
my $starttime = time();
my $checkpointtime = $starttime;

# check for trailing "/" on bucket
$char = substr($bucket,length($bucket)-1,1);
die "Error: S3 bucket path must end with \"/\"\n" if ($char ne "/");

# open pipe to gnu parallel
#$gpout = "gp.out";
#open GP, "| parallel -j $threads > $gpout" or die "Cannot open pipe to gnu parallel\n";
open GP, "| parallel -j $threads" or die "Cannot open pipe to gnu parallel\n";
#open GP, ">commands.txt";

&download($bucket);

close GP;
if ($?) {
    print "Error: not all objects downloaded successfully: $?\n" 
} else {
    print "Finished downloading $objects objects\n"; 
}
&runtime;

exit;

################## subs ###################

sub download { 
    my $directory = $_[0];
    my @dirs;

    print STDERR "Downloading Directory $directory\n";

    $path = $directory;
    $path =~ s/^s3:\///i;
#    print "path: $path\n";
    $path =~ /\/([^\/]*)\/$/;
    $folder = $1;
#    print "folder: $folder\n";
    mkdir $folder;
    chdir $folder;

    $result = `s3cmd ls $directory`;
    if ($result eq "") {
	print STDERR "No objects found in directory $directory\n";
	return;
    }

    # parse list of bucket contents, send download commands to gnu parallel
    @result = split /\n/, $result;
    $objects = 0;

    foreach $line (@result) {
	chomp $line;
	@line = split ' ', $line;
	# treat directories differently
	if (($#line eq 1) and ($line[0] eq "DIR")) {
	    print STDERR "Dir: $line[1]\n";
	    push @dirs, $line[1]; # remember these for later
	} else {
#    print "$line[0], $line[1], $line[2], $line[3]\n";
	    print STDERR "File: $line\n";
	    $objects++;
	    $mydir = getcwd;
	    print GP "s3cmd --continue get $line[3] $mydir\n";
	}
    }

    foreach $dir (@dirs) {
	&download($dir);
    }
    chdir "..";
}

# this truncates instead of rounds, fix it
sub round10 {
    return int($_[0] * 10) / 10;
}
# print how long we've been running
sub runtime {
    $now = time();
    my $runtime = $now - $starttime;
    my $checktime = $now - $checkpointtime;
    print "Total time: " . &formattime($runtime) . "\n";
    $checkpointtime = $now;
}

# print out checkpoint time info
sub formattime {
    my ($time) = @_;
    if ($time < 60) {
        return "$time seconds";
    } elsif ($time < 7200) { # 2 hours
        my $minutes = &round10($time / 60);
        return "$minutes minutes";
    } else {
	$hours = &round10($time / 3600);
        return "$hours hours";
    }
}
