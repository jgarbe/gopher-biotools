#!/usr/bin/perl -w

#######################################################################
#  Copyright 2018 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastq-starts.pl - List most frequent sequences at start of reads in a fastq file

=head1 SYNOPSIS

fastq-starts.pl [-r 100000] [-s 4] [-c 15] file.fastq[.gz]

=head1 DESCRIPTION

This script grabs the first -c bases of each read from the first -r reads of the fastq file. Reads are grouped by the first four bases of the read (the "seed"). For each high frequency seed (> 1% frequency in the file) consensus basecalls are made: uppercase and lowercase base calls indicate the base has a frequency > .9 or .666. A split call [TC] indicates the two bases each have a frequency > .333. All other cases a "." is reported.

Output looks like this:
CGCCtCGATC[TC].... 1086 1.09%

Options:

    -c integer : Number of bases to grab from the beginning of each read
    -r integer : Number of reads to grab from the fastq file
    -s integer : Seed length, set to length of cut site scar (default 4) 
    -i integer : Number of bases at the beginning of the read to ignore (Default 0)

=head1 DETAILS

No details

=cut

use Getopt::Std;
use Pod::Usage;

my ($ok);
our ($opt_r, $opt_c, $opt_h, $opt_s, $opt_i);

# get parameters
$ok = getopts('hr:c:s:i:');
pod2usage(q(-verbose) => 3) if ($opt_h);

die "USAGE: fastq-starts.pl [-r 100000] [-s 4] [-c 10] file.fastq[.gz]\n" unless ($#ARGV == 0);

$reads = $opt_r // 100000;
$lines = $reads * 4;
$opt_c = $opt_c // 15;
$seedlength = $opt_s // 4;
$ignore = $opt_i // 0;
$minseedpct = 1;


$ifile = shift @ARGV;
$start = $ignore + 1;
$end = $opt_c + $ignore;
if ($ifile =~ /\.gz$/) {
    $command = "gunzip -c $ifile | head -n $lines | awk 'NR%4==2' | cut -b $start-$end | sort | uniq -c | sort -h";
} else {
    $command = "head -n $lines $ifile | awk 'NR%4==2' | cut -b $start-$end | sort | uniq -c | sort -h";
}

@results = `$command`;
die "command $command failed" if ($?);

foreach $line (@results) {
    chomp $line;
    $line =~ s/^\s+//;
    ($count, $seq) = split ' ', $line;
    $seed = substr($seq, 0, $seedlength);
    $seedcount{$seed} += $count;
    $totalcount += $count;

    @chars = split '', substr($seq, $seedlength, $opt_c - $seedlength);
    for $pos (0..$#chars) {
	$char = lc($chars[$pos]);
	$seedprofile{$seed}[$pos]{$char} += $count;
    }
}

$nonseedcount = 0;
foreach $seed ( sort { $seedcount{$a} <=> $seedcount{$b} } keys %seedcount ) {
    if ($seedcount{$seed} / $totalcount * 100 > $minseedpct) {
#	print "$seed\t$seedcount{$seed}\n";

	print "$seed";
	for $pos (0..$opt_c - $seedlength-1) {
	    $a = $seedprofile{$seed}[$pos]{a} // 0;
	    $g = $seedprofile{$seed}[$pos]{g} // 0;
	    $c = $seedprofile{$seed}[$pos]{c} // 0;
	    $t = $seedprofile{$seed}[$pos]{t} // 0;
	    $n = $seedprofile{$seed}[$pos]{n} // 0;
	    $total = $a + $g + $c + $t + $n;
	    if ($total > 0) {
		$pct{A} = $a / $total;
		$pct{G} = $g / $total;
		$pct{C} = $c / $total;
		$pct{T} = $t / $total;
		$call = ".";
	    } else {
#		$pct{A} = 0;
#		$pct{G} = 0;
#		$pct{C} = 0;
#		$pct{T} = 0;
#		$call = ".";
	    }		
	    foreach $base ("A", "G", "C", "T") {
		if ($pct{$base} > .6666) {
		    $call = lc($base);
		}
		if ($pct{$base} > .9) {
		    $call = $base;
		}
	    }
	    if ($call eq ".") {
		foreach $base1 ("A", "G", "C", "T") {
		    foreach $base2 ("A", "G", "C", "T") {
			next if ($base1 eq $base2);
			if ($pct{$base1} > .333 and $pct{$base2} > .333) {
			    $call = "[$base1$base2]";
			}
		    }
		}
	    }

	    print $call;
	}
	$pct = &round100( $seedcount{$seed} / $totalcount * 100);
	print " $seedcount{$seed} $pct%\n";

    } else {
	$nonseedcount += $seedcount{$seed};
    }
}
$nonseedpct = &round100($nonseedcount / $totalcount * 100);
print "Total reads: $totalcount\n";
print "Other reads: $nonseedcount $nonseedpct%\n";

################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}




