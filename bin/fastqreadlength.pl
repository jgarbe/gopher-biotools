#!/usr/bin/perl -w

########################################
# fastqreadlength.pl
# John Garbe
# December 2014
#
#######################################

=head1 NAME

fastqreadlength.pl - return the length of the first read in the fastq file

=head1 SYNOPSIS

fastqreadlength.pl file.fastq

=head1 DESCRIPTION

This script prints out the length of the first read in the fastq file.

Options:
    -h : Display usage information

=cut

use Pod::Usage;
use Getopt::Std;

$usage = "Usage: fastqreadlength.pl file.fastq\n";

# handle parameters
getopts('h', \%opts) or die "$usage\n";
pod2usage(q(-verbose) => 3) if ($opts{'h'});
die $usage unless ($#ARGV == 0);

$file = shift @ARGV;
die "Cannot find file $file\n" unless (-e $file);

if ($file =~ /\.gz$/) {
    $result = `gunzip -c $file | sed '2q;d' | wc -m`;
} else {
    $result = `sed '2q;d' $file | wc -m`;
}
chomp $result;
$result--;
print "$result\n";
