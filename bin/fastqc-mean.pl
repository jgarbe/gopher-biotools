#!/usr/bin/perl -w

# print out the mean qscore from a fastqc data file

$usage = "USAGE: fastqc-mean.pl fastqc_data.txt\n";
die $usage unless ($#ARGV == 0);

# open the file
$ifile = shift @ARGV;
open IFILE, "$ifile" or die "Cannot open file $ifile: $!\n";

# get folder name
@parts = split /\//, $ifile;
$folder = $parts[$#parts-1];

# scan to the correct module section
while (<IFILE>) {
    last if (/>>Per base sequence quality/);
}
# read in the module output
my $junk = <IFILE>;
while (<IFILE>) {
    last if (/>>END_MODULE/);
    chomp;
    $bases++;
    my ($base, $mean) = split;
    $sum += $mean;
}

# Make sure that reads don't have variable length (resulting in incorrect mean calculation)
while (<IFILE>) {
    last if (/>>Sequence Length Distribution/);
}
$linecount = 0;
while (<IFILE>) {
    last if (/>>END_MODULE/);
    $linecount++;
}
die "Error: Variable read lengths\n" if ($linecount > 2);

# Print out the mean
if ($bases <= 0) {
    print "Error\n";
} else {
    $mean = $sum / $bases;
    print "$folder\tMean: $mean\n";
}
