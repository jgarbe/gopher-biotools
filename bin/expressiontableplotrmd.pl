#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014-2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

expressiontableplot.pl - Given a table of expression data, generate some summary plots:
 -Expressed genes plot
 -Expression distribution violin plots
 -Dendrogram
 -PCA plot
 -Heatmap???

=head1 SYNOPSIS

expressiontableplot.pl --expressionfile data.txt

=head1 DESCRIPTION

Generate a series of plots summarizing a table of expression data. The input file should be tab delimited with a header. There should be a row for each feature (gene, transcript, exon, etc), and a column for each sample. The first row should contain sample names and the first column feature IDs.

Options:
    --expressionfile file : expression table file
    --normalize : Normalize expression values: 75% quartile normalization
    --minexpression integer : Minimum expression value, in reads-per-million (a value of zero sets minimum to 1 read) (default: 1 per million)
    --featuretype string : Feature type (gene, transcript, exon, etc)
    --topgenes integer : Number of most variable genes to use for clustering (500)
    --samplesheet file : samplesheet with metadata
    --help : Display usage information
    --verbose : Verbose output

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;

$args{featuretype} = "feature";
$args{minexpression} = 1;
$args{topgenes} = 500;
GetOptions("help" => \$args{help},
           "verbose" => \$args{verbose},
           "featuretype=s" => \$args{featuretype},
           "minexpression=i" => \$args{minexpression},
           "topgenes=i" => \$args{topgenes},
           "normalize" => \$args{normalize},
           "expressionfile=s" => \$args{expressionfile},
           "samplesheet=s" => \$args{samplesheet},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
pod2usage unless ($args{expressionfile});

# output files
my $dendroplotfile = "dendrogramplot.png";
my $expressedplotfile = "expressedplot.png";
my $distributionplotfile = "expressiondistributionplot.png";

die "Cannot find file $args{expressionfile}" unless (-e $args{expressionfile});

### Normalize data ###
$ofile = "expressiontableplot.tmp";
if ($args{normalize}) {
    print "Quaartile normalizing data\n"; 
    $result = `quartile_norm.pl -c -1 -s 1 $args{expressionfile} -o $ofile`;
    print $result;
    $args{expressionfile} = $ofile;
}

### read in the expression table ###
open IFILE, "<$args{expressionfile}" or die "Could not open $args{expressionfile}: $!\n";
# header looks like this: feature_id sample1_id sample2_id ...
my $header = <IFILE>;
chomp $header;
@header = split /\t/, $header;

while (<IFILE>) {
    chomp;
    @line = split /\t/;
    $features{$line[0]} = 1;
    for $i (1..$#line) {
	$sample = $header[$i];
#	print "$i: :$sample:\n";
	$line[$i] = 0 unless ($line[$i]);
	$counts{$sample}{$line[0]} = $line[$i];
	$totalcounts{$sample} += $line[$i];
	$samplecounts{$sample} += $line[$i];
    }
}
close IFILE;

# count number of expressed genes
@sorted_features = sort( keys(%features));
foreach $sample (keys %counts) {
    $expressed{$sample} = 0;
    if ($args{minexpression} == 0) {
	$minexpression = 1;
    } else {
	$minexpression = ($totalcounts{$sample} / 1000000) * $args{minexpression};
    }
    foreach $feature (@sorted_features) {
	$expressed{$sample}++ if ($counts{$sample}{$feature} >= $minexpression);	
    }
}

### print stats to screen ###
print "STATS Start\n";
foreach $sample (sort keys(%counts)) {
    print "$sample\texpressedgenes\t$expressed{$sample}\n";
}
foreach $sample (sort keys(%counts)) {
    $samplecounts{$sample} = sprintf("%.1f", $samplecounts{$sample});
    print "$sample\tabundancereads\t$samplecounts{$sample}\n";
}
print "STATS End\n";

$features = $args{featuretype} . "s";

### Expressed Genes Plot ###
$name = "expressedplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# generate order for report sorting
@order = sort {$expressed{$b} <=> $expressed{$a}} keys %expressed;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tExpressed $features\t$order\n";

# print out the data
print OFILE "sample\texpressed\n";
foreach $sample (sort keys %expressed) {
    print OFILE "$sample\t$expressed{$sample}\n";
}
close OFILE;

$title = "Expressed $features";
if ($minexpression > 0) {
    $method = "has expression equal to or higher than $args{minexpression} reads per million";
} else {
    $method = "has expression of at least 1";
}
$text = "The number of $features expressed per sample is shown. For this plot a $args{featuretype} is considered expressed if it $method.";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~expressed,
 type = "bar",
hoverinfo="text", text = paste("Sample:", datat\$sample,"<br>expressed $features",format(datat\$expressed,big.mark=",",scientific=FALSE))
) %>% 
 layout(xaxis = sampleorder, dragmode='pan', barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Expressed $features", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;
$args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);


### generate expression distribution plot
$name = "distributionplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating expression distribution plot\n" if ($args->{verbose});
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# generate order for report sorting
if (0) { # this isn't operation yet
    @order = sort {$median{$a} <=> $median{$b}} keys %$median;
    $order = "";
    foreach $sample (@order) {
	$order .= "\"$sample\",";
    }
    chop $order; # remove last ,
    print "ORDER\tExpression Distribution\t$order\n";
}

# print out the data
# NOTE: we're being fancy here, prepending an "a" to each sample name to force plotly to treat sample names as categorical values, otherwise samplenames beginning with digits cause failure. This is only necessary for violin plots
@samples = sort keys %counts;
print OFILE "size";
foreach $sample (@samples) {
    print OFILE "\ta$sample";
}
print OFILE "\n";
for $i (0..$#sorted_features) {
    print OFILE "$i";
    foreach $sample (@samples) {
	print OFILE "\t" . log10($counts{$sample}{$sorted_features[$i]});
    }
    print OFILE "\n";
}
close OFILE;

$title = "Expression Distribution";
$text = "The distribution of $args{featuretype} expression in each sample is shown. ";

$traces = "";
for $i (1..$#samples) {
    $traces .= " %>% add_trace(y=~a$samples[$i], name='$samples[$i]', line=list(color = '#1E77B4'), type='violin', hoveron='violins', hoverinfo='text', text='$samples[$i]') ";
}

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name, warning=FALSE}

library(plotly)

datat <- read.table("$ofile", header=T, sep="\t");

config(plot_ly(
  data=datat,
  y = ~a$samples[0],
  name = '$samples[0]',
 type = "violin", points = FALSE, line=list(color = '#1E77B4'), showlegend = FALSE,
hoveron = "violins",
hoverinfo="text",
text='$samples[0]'
) $traces %>% 
 layout(dragmode='pan', xaxis = list(title = "Sample", type = "category", tickmode = "linear", tickfont = list(size = 10)), yaxis = list(title = "log10(expression)", fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;
$args->{reporttext} .= qq(```{r test-main, child = '$rfile'}\n```\n);


### PCA plot ###
# print out data in table format
$tmpfile = "expressiontableplot.dat";
open OFILE, ">$tmpfile" or die "Could not open file $tmpfile \n";
# print out header
foreach $feature (@sorted_features) {
    print OFILE "\t$feature";
}
print OFILE "\n";

# print out data
foreach $sample (keys(%counts)) {
    print OFILE "$sample";
    
    foreach $feature (@sorted_features) {
	print OFILE "\t$counts{$sample}{$feature}";
    }
    print OFILE "\n";
}
close OFILE;

### PCA and Dendrogram Plots ###
$r_script = "r.tmp";
open RFILE, ">$r_script" or die "Cannot open $r_script\n";
print RFILE << "EOF";
library("DESeq2")
#library("ggplot2")
#library("RColorBrewer")
#library("gplots")

# Read in data
data <- read.table("$args{expressionfile}", header=TRUE, row.names=1, check.names=FALSE);
data <- data[,order(colnames(data))]
# Round (for fpkm values)
data <- round(data)
data <- data + 1 # add one to all values so vst transformation doesn't complain about zeros

# Create (fake) metadata and column names for DESeq2
if (ncol(data) %% 2 == 0) {
Group <- factor(c(rep(c("dummy1","dummy2"),ncol(data)/2)))
} else {
Group <- factor(c(rep(c("dummy1","dummy2"),ncol(data)/2), "dummy1"))
}

(meta <- data.frame(row.names=colnames(data), Group))

# Create DESeq2 data type, transform
dds <- DESeqDataSetFromMatrix(countData = data, colData = meta, design = ~ Group)
#rld <- rlogTransformation(dds)
# try the normal way, otherwise use a workaround
res <- try(rld <- vst(dds))
if(inherits(res, "try-error"))
{
rld <- varianceStabilizingTransformation(dds)
}

# calculate the variance for each gene
rv <- genefilter::rowVars(assay(rld))

# select the ntop genes by variance
select <- order(rv, decreasing=TRUE)[seq_len(min($args{topgenes}, length(rv)))]

# perform a PCA on the data for the selected genes
pca <- prcomp(t(assay(rld)[select,]))

# Eigenvalues
ev <- t(pca\$sdev^2);
row.names(ev) <- c("eigvals");
# Proportion of variance
pv <- t(pca\$sdev^2/sum(pca\$sdev^2) * 100);
row.names(pv) <- c(\"% variation explained\");
# Write to file
write.table(pca\$x, "pcadata.dat", sep="\t");
cat(\"\\n\\n\", file = "pcadata.dat", append = TRUE) 
write.table(ev, "pcadata.dat", sep="\t", append=TRUE, col.names=FALSE);
write.table(pv, "pcadata.dat", sep="\t", append=TRUE, col.names=FALSE);

EOF
    ;

close RFILE;
system("R --no-restore --no-save --no-readline < $r_script &> $r_script.out");
if ($?) {
    `cat $r_script.out`;
}
$samplesheet = ($args{samplesheet}) ? "--samplesheet $args{samplesheet}" : "";
`pcaplot.pl --datafile pcadata.dat $samplesheet`;


exit;

################################## helper subs ###################
sub log10 {
    my $n = shift;
    return "" if ($n == 0);
    return log($n)/log(10);
}
