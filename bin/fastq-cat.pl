#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastq-cat.pl - Concatenate FastQC files

=head1 SYNOPSIS

    fastq-cat.pl [-c] [-z] ~/fastqfolder [ fastqfolder2 ... ]  > fastq-commands.txt

=head1 DESCRIPTION

This script identifies samples spread across multiple fastq files and generates cat commands to concatenate them together. Symlink commands are generated for single-file samples (unless -c is used). This script only generates the commands to concatenate and link files. Execute the commands that are generated to generate the concatenated and linked files. Single and paired-end files supported. Gzipped files supported (with -z option, mix of zipped and unzipped files not supported).

Options:

    -c : Samples with just one file should be copied instead of symlinked
    -z : Fastq files are gz compressed
    -1 : Concatenate R2 filess into R1 files

=head1 DETAILS

The MiSeq and HiSeq systems generate fastq file names using this pattern:
``<sample name>_<barcode sequence>_L<lane (0-padded to 3 digits)>_R<read number>_<set number (0-padded to 3 digits>.fastq``
Sometimes the barcode and set number are ommited, resulting in this pattern:
``<sample name>_L<lane (0-padded to 3 digits)>_R<read number>.fastq``
This script parses filenames by these to patterns. All files with the same sample name and read number are concattenated together into one file.

=head1 EXAMPLE

Create a directory to contain the concatenated files::

    $ mkdir fastq-cat
    $ cd fastq-cat

Generate the concatenation commands::

    $ fastq-cat.pl [-c] [-z] ~/fastqfolder [ fastqfolder2 ... ]  > fastq-commands.txt

Verify the correctness of the commands, and then execute them::

    $ bash fastq-commands.txt

=cut

use Getopt::Std;
use File::Basename;
use Cwd 'abs_path';
use Pod::Usage;

my ($suffix, $path, $ok);
our ($opt_h, $opt_c, $opt_z, $opt_v, $opt_1);

# get parameters
pod2usage unless getopts('hczv1');
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($opt_h);
pod2usage unless ($#ARGV >= 0);
foreach $folder (@ARGV) {
    die "Cannot find folder $folder\n" unless (-d $folder);
    push @folders, abs_path($folder);
}

$suffix = ($opt_z) ? ".fastq.gz" : ".fastq";

# get list of fastq files
foreach $folder (@folders) {
    @thesefastqs = <$folder/*$suffix>;
    die "No *$suffix files found in $folder\nTry using the -z option to find fastq.gz files\n"if ($#thesefastqs < 0);
    push @fastqs, @thesefastqs;
}

# Illumina FASTQ files use the following naming scheme: 
# <sample name>_<barcode sequence>_L<lane (0-padded to 3 digits)>_R<read number>_<set number (0-padded to 3 digits>.fastq.gz
# sample_ACAGTG_L001_R1_001.fastq
# sample_ACAGTG_L002_R1_001.fastq

# process each file
$samplecount = 0;
foreach $fastq (@fastqs) {
    ($name, $path, $suffix) = fileparse($fastq, ($suffix));

    if ($name =~ /(.*)_([AGCT]{3,8})(?:-[AGCT]{3,8})?_L00([1-8])_([RI][123])_[0-9]{3}$/) {
#    if ($name =~ /(.*)_[AGCT]{3,12}_L00[1-8]_(R[12])_[0-9]{3}$/) {
	print STDERR "Match 1\n" if ($verbose);
	$sample = $1;
	$seen{$sample} = ++$samplecount if (!defined($seen{$sample}));
	$R = ($opt_1) ? "R1" : $5;
	$name = "${sample}_S$seen{$sample}_${R}_001";
	$cat{$name} .= " $fastq";
	$count{$name}++;
    } elsif ($name =~ /(.*)_L00[1-8]_([RI][123])$/) {
	print STDERR "Match 2 sample $1 read $2\n" if ($verbose);
	$sample = $1;
	$seen{$sample} = ++$samplecount if (!defined($seen{$sample}));
	$R = ($opt_1) ? "R1" : $2;
	$name = "${sample}_S$seen{$sample}_${R}_001";
	$cat{$name} .= " $fastq";
	$count{$name}++;
    } elsif ($name =~ /(.*)_(S[0-9]{1,4})(?:_L00[1-8])?_([RI][123])_[0-9]{3}$/) {
	print STDERR "Match 3\n" if ($verbose);
	$sample = $1;
	$seen{$sample} = ++$samplecount if (!defined($seen{$sample}));
	$R = ($opt_1) ? "R1" : $3;
	$name = "${sample}_S$seen{$sample}_${R}_001";
	$cat{$name} .= " $fastq";
	$count{$name}++;
    } else {
	@parts = split /_/, $name;
	print STDERR "Unmatched file: $name";
	foreach $part (@parts) {
	    print STDERR "\t$part";
	}
	print STDERR "\n";
    }
}

# print out the cat commands
$count = 1;
foreach $sample (keys %cat) {
    if ($count{$sample} == 1) {
	if ($opt_c) {
	    print "cp $cat{$sample} $sample$suffix\n";
	} else {
	    print "ln -s $cat{$sample} $sample$suffix\n";
	}
    } else {
	print "cat $cat{$sample} > $sample$suffix\n";
    }
}

exit;
