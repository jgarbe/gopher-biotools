#! /usr/bin/env python3

#Created by Lindsay Guare 7/10/18
#Last Edited 7/24/18 (LG)

import subprocess as sub
import collections as coll
import numpy as np
import math
import argparse as ap
import sys
from os.path import expanduser as eu

def clean_table(mf):
    new_data = []
    for row in mf:
        new_row = []
        for item in row:
            new_str = clean_string(item)
            new_row.append(new_str)
        new_data.append(new_row)
    return new_data

def clean_string(arg):
    new_str = str(arg).lower()
    new_str = new_str.replace('%', 'pct_')
    new_str = new_str.replace('#', 'num_')
    new_str = new_str.replace('-', '_')
    new_str = new_str.replace(' ', '_')
    return new_str

#applies filtering rules in the order of the subset file
#subset file should be tab-delimited and have five columns and no header
#those five columns should be in order:
#1) the column whose values will be filtered
#2) the type of data in that column: "cat" for categorical or "num" for numerical
#3) the comparison to be made: "equals," "less_than," "greater_than," or "contains"*
#4) the value to which the data will be compared
#5) the action done to data that meet the requirement: "keep"** or "remove"
#*CONTAINS can only be used with categorical data as it will use string comparison
#**KEEP will remove those data that don't meet the filter**
def subset_metrics(lowercase_metrics, subset_file, cols):
    metrics = lowercase_metrics
    if subset_file == "NONE":
        return metrics
    else:
        rules = np.genfromtxt(subset_file, dtype = 'str')
        if rules.ndim == 1:
            rules = [rules]
        for row in rules:
            if row[4] != "highlight":
                metrics = apply_rule(row, metrics, cols)
            else:
                metrics = apply_highlight(row, metrics, cols)
        return metrics

def scrub_missing(mv, table, needed_cols):
    needed_col_nums = []
    new_table = [table[0]]
    for col in needed_cols:
        needed_col_nums.append(table[0].index(col))
    for row in table[1:]:
        good_row = True
        for i in needed_col_nums:
            if row[i].lower() == mv.lower():
                good_row = False
        if good_row:
            new_table.append(row)
    return new_table

#applies individual filtering rule from subset file
def apply_rule(rule, metrics, cols):
    col = rule[0]
    colnum = cols.index(col)
    type = rule[1]
    op = rule[2]
    val = rule[3]
    act = rule[4]
    new_metrics = []
    for row in metrics:
        if op == "equals":
            if type == "cat":
                if row[colnum] == val.lower() and act == "keep":
                    new_metrics.append(row)
                elif row[colnum] != val.lower() and act == "remove":
                    new_metrics.append(row)
            elif type == "num":
                if float(row[colnum]) == float(val) and act == "keep":
                    new_metrics.append(row)
                elif float(row[colnum]) != float(val) and act == "remove":
                    new_metrics.append(row)
        elif op == "greater_than":
            if type == "cat":
                if row[colnum] > val.lower() and act == "keep":
                    new_metrics.append(row)
                elif row[colnum] <= val.lower() and act == "remove":
                    new_metrics.append(row)
            elif type == "num":
                if float(row[colnum]) > float(val) and act == "keep":
                    new_metrics.append(row)
                if float(row[colnum]) <= float(val) and act == "remove":
                    new_metrics.append(row)
        elif op == "less_than":
            if type == "cat":
                if row[colnum] < val.lower() and act == "keep":
                    new_metrics.append(row)
                elif row[colnum] >= val.lower() and act == "remove":
                    new_metrics.append(row)
            elif type == "num":
                if float(row[colnum]) < float(val) and act == "keep":
                    new_metrics.append(row)
                elif float(row[colnum]) >= float(val) and act == "remove":
                    new_metrics.append(row)
        elif op == "contains":
            if row[colnum].find(val.lower()) >= 0 and act == "keep":
                new_metrics.append(row)
            elif row[colnum].find(val.lower()) < 0 and act == "remove":
                new_metrics.append(row)
    return new_metrics

#applies individual highlighting rule from subset file
def apply_highlight(rule, metrics, cols):
    col = rule[0]
    colnum = cols.index(col)
    type = rule[1]
    op = rule[2]
    val = rule[3]
    new_metrics = []
    for row in metrics:
        if len(row) == len(cols):
            row.append("F")
        if op == "equals":
            if type == "cat":
                if row[colnum] == val:
                    row[-1] = "T"
            elif type == "num":
                if float(row[colnum]) == float(val):
                    row[-1] = "T"
        elif op == "greater_than":
            if type == "cat":
                if row[colnum] > val:
                    row[-1] = "T"
            elif type == "num":
                if float(row[colnum]) > float(val):
                    row[-1] = "T"
        elif op == "less_than":
            if type == "cat":
                if row[colnum] < val:
                    row[-1] = "T"
            elif type == "num":
                if float(row[colnum]) < float(val):
                    row[-1] = "T"
        elif op == "contains":
            if row[colnum].find(val.lower()) >= 0:
                row[-1] = "T"
        new_metrics.append(row)
    return new_metrics

#condenses data by averaging together rows that have the same values in columns passed to either --averagey or --averagex
def apply_averages(met, dcols, vcols, cols):
    #first collects indices of columns to be kept
    dindx = []
    vindx = []
    for col in dcols:
        dindx.append(cols.index(col))
    for col in vcols:
        vindx.append(cols.index(col))

    #next collects a set of unique 'coordinates' which represent the averaged dataset
    group_coords = []
    for row in met:
        coord = []
        for ix in dindx:
            coord.append(str(row[ix]))
        if coord not in group_coords:
            group_coords.append(coord)
    groups = coll.defaultdict(list)
    for group in group_coords:
        groups[tuple(group)] = []

    #then collects x, y, and/or y2 values for each set of coordinates
    for row in met:
        coord = []
        tempv = []
        for ix in dindx:
            coord.append(str(row[ix]))
        for ix in vindx:
            tempv.append(float(row[ix]))
        groups[tuple(coord)].extend(tempv)
    final_groups = []

    #averages each set of values
    for dtup, vals in groups.items():
        separated_vs = [[] for _ in range(len(vcols))]
        averages = []
        n = len(vcols)
        for i in range(len(vals)): #distributes 1-D list of values to the proper 2-D list (by column)
            separated_vs[(i % n)].append(vals[i])
        for vcol in separated_vs:
            averages.append((str(get_mean(vcol))[:8]))
        coords = list(dtup)
        coords.extend(averages)
        final_groups.append(coords)
    return final_groups

#returns the arithmetic mean of a list of numbers
def get_mean(list):
    sum = 0
    for num in list:
        sum += float(num)
    return sum / len(list)

#checks the value of an argument which should have a column name as its value
#prints an error message and exits if the value is inappropriate
def check_col_arg(arg, cols):
    if not arg[1] and arg[0].lower() == "none":
        print("Missing one or more required columns. Try one of these:")
        print(cols)
        sys.exit()
    elif arg[0].lower() not in cols and not(arg[1] and arg[0].lower() == "none"):
        print(arg[0] + " is not a valid column name. Try one of these:")
        print(cols)
        sys.exit()

#returns a metrics table that does not have columns which won't be used in plotting
def streamline_cols(metrics, cols):
    colnums = []
    print("Picking out these cols:")

    print(cols)

    for col in cols:
        colnums.append(metrics[0].index(col))
    smaller_metrics = []
    for row in metrics:
        smaller_row = []
        for i in range(len(row)):
            if i in colnums:
                smaller_row.append(row[i])
        smaller_metrics.append(smaller_row)
    return smaller_metrics

#modifies the metrics table by transforming the values in col by log with base base
def apply_log(col, base, metrics):
    new_table = [metrics[0]]
    colnum = metrics[0].index(col)
    new_table[0][colnum] = "log_" + str(base) + "_" + new_table[0][colnum]
    for row in metrics[1:]:
        new_row = []
        for i in range(len(row)):
            if i == colnum:
                new_row.append(str(math.log(float(row[i]), base))[:7])
            else:
                new_row.append(row[i])
        new_table.append(new_row)
    return new_table

#writes trendline file to be used in R plotting
def add_trendline_metrics(metrics, xcol, ycol):
    xindx = metrics[0].index(xcol)
    yindx = metrics[0].index(ycol)
    other_indx = sorted(list(set(range(len(metrics[0]))) - set([xindx, yindx])))
    data = coll.defaultdict(list)
    for row in metrics[1:]:
        coords = []
        for i in other_indx:
            coords.append(row[i])
        if tuple(coords) not in data:
            data[tuple(coords)] = [[], []] #two lists: x values and y values
        data[tuple(coords)][0].append(row[xindx])
        data[tuple(coords)][1].append(row[yindx])
    trendData = coll.defaultdict()
    for coords in data.keys():
        trendData[coords] = generate_trendline_points(data[coords][0], data[coords][1])
    new_data = metrics
    new_data[0].append("trendline_value")
    for i in range(1, len(new_data)):
        coords = []
        for j in other_indx:
            coords.append(new_data[i][j])
        coords = tuple(coords)
        new_data[i].append(trendData[coords][new_data[i][xindx]])
    return new_data

#generates a set of points producing a trendline for data y vs x
def generate_trendline_points(xList, yList):
    m, b = np.polyfit(np.array(xList, dtype = 'float'), np.array(yList, dtype = 'float'), deg = 1)
    trendVals = coll.defaultdict(float)
    for x in xList:
        trendVals[x] = linear_transform(float(m), float(x), float(b))
    return trendVals

def linear_transform(m, x, b):
    return (m * x) + b

#puts all of the arguments in a method so that I can collapse them :)
def do_parser_stuff():
    parser = ap.ArgumentParser(description = "Takes in tab-delimited metrics file and generates a plot using R according to the user's specifications")

    req = parser.add_argument_group("required Arguments")

    req.add_argument('--metrics', '-m', help = 'tab-delimited metrics file with one row of header', type = str, dest = 'm', required = True)
    req.add_argument('--xcol', '-x', help = 'column to be plotted on the x axis', type = str, dest = 'x', required = False, default = "NONE")
    req.add_argument('--ycol', '-y', help = 'column to be plotted on the y axis', type = str, dest = 'y', required = False, default = "NONE")

    parser.add_argument('--colhelp', help = 'print column names and exit', action = 'store_true', default = False)
    parser.add_argument('--color', '-c', help = 'column to be used for color grouping', type = str, dest = 'c', default = "NONE")
    parser.add_argument('--facet-wrap', '-w', help = 'column to be used for generating multiple plots', dest = 'w', type = str, default = "NONE")
    parser.add_argument('--linetype', '-l', help = 'column to be used for linetype grouping', type = str, dest = 'l', default = "NONE")
    parser.add_argument('--out', '-o', help = 'output PREFIX for .png file', dest = 'o', type = str, default = "out")
    parser.add_argument('--subsetfile', help = 'tab-delimited text file with restrictions on certain values in the chart', dest = 's', type = str, default = "NONE")
    parser.add_argument('--adddim', help = 'column(s) to be included as opposed to averaged out', default = [], dest = 'adim', action = 'append')
    parser.add_argument('--noavg', help = 'original dimensionality of the data will be preserved as opposed to auto-grouping', action = 'store_true', default = False)
    parser.add_argument('--missingval', help = 'string which denotes a missing value in the metrics table', default = "NONE", dest = 'mv')
    parser.add_argument('--delim', help = 'row-delimiting string, assumed to be white space', default = "NONE")
    parser.add_argument('--xlog', help = 'coverts x axis values to be on a log scale with specified base', action = 'store', default = -1, type = float)
    parser.add_argument('--ylog', help = 'coverts y axis values to be on a log scale with specified base', action = 'store', default = -1, type = float)
    parser.add_argument('--trendline', help = 'when --scatter is also used, adds trendlines to plot', default = False, action = 'store_true')

    plot_type = parser.add_mutually_exclusive_group()
    plot_type.add_argument('--line', action = 'store_const', dest = 'plot_type', default = "line", help = 'generates plots as line plots', const = "line")
    plot_type.add_argument('--scatter', action = 'store_const', dest = 'plot_type', default = "line", help = 'generates plots as scatter plots', const = "point")
    plot_type.add_argument('--bar', action = 'store_const', dest = 'plot_type', default = "line", help = 'generates plots as bar plots', const = "col")

    args = parser.parse_args()
    return args

args = do_parser_stuff()

if args.delim != "NONE":
    mf = np.genfromtxt(args.m, dtype = 'str', comments = "3.14159265358979", delimiter = args.delim) #generates initial metrics table with user-provided delimiter
else:
    mf = np.genfromtxt(args.m, dtype = 'str', comments = "3.14159265358979", delimiter = "\t") #generates initial metrics table
lowercase_metrics = []
for row in mf:
    row_list = []
    for item in row:
        row_list.append(item.lower())
    lowercase_metrics.append(row_list) #converts all letters in the table to lowercase
cols = []
for item in lowercase_metrics[0]:
    cols.append(clean_string(item))
#cols = lowercase_metrics[0]

if args.colhelp: #prints list of possible column names to use and exits
    for col in cols:
        print(col + " ")
    sys.exit()

col_args = [(args.x, False), (args.y, False), (args.c, True), (args.w, True), (args.l, True)]
dim_cols = args.adim
for col in dim_cols:
    col_args.append((col, True))

needed_cols = []
for arg in col_args:
    check_col_arg(arg, cols)
    if arg[0].lower() != "none":
        needed_cols.append(arg[0].lower()) #checks value of each argument, keeps columns that will be used in plotting

if args.mv != "NONE":
    lowercase_metrics = scrub_missing(args.mv, lowercase_metrics, needed_cols)

if args.noavg:
    needed_cols = lowercase_metrics[0]

sm = subset_metrics(lowercase_metrics[1:], args.s, lowercase_metrics[0])
sm0 = [lowercase_metrics[0]]
hl = False
if len(sm[0]) > len(sm0[0]):
    hl = True
    needed_cols.append("highlight")
    cols.append("highlight")
    sm0[0].append("highlight")
sm0.extend(sm)
print("sm0:")
print(sm0[0])
smaller_metrics = streamline_cols(sm0, needed_cols)
needed_cols = smaller_metrics[0]
i = 0
while i < len(cols):
    if cols[i] not in needed_cols:
        cols.remove(cols[i])
    else:
        i += 1

if args.noavg:
    adjusted_metrics = smaller_metrics
else:
    dcols0 = [args.w, args.l]
    if hl:
        dcols0.append(args.c)
        dcols0.append("highlight")
    else:
        dcols0.append(args.c)
    dcols0.extend(args.adim)
    vcols = [args.y]
    dcols0.append(args.x)
    dcols = [x for x in dcols0 if x.lower() != "none"]
    am = apply_averages(smaller_metrics[1:], dcols, vcols, cols)
    adjusted_metrics = [list(dcols + vcols)]
    adjusted_metrics.extend(am)

adjusted_metrics = clean_table(adjusted_metrics)

x = clean_string(args.x) if args.xlog == -1 else "log_" + str(args.xlog) + "_" + args.x
y = clean_string(args.y) if args.ylog == -1 else "log_" + str(args.ylog) + "_" + args.y
c = clean_string(args.c)
lt = clean_string(args.l) if args.l != "NONE" else "highlight"
w = clean_string(args.w)

if args.xlog != -1:
    adjusted_metrics = apply_log(args.x, args.xlog, adjusted_metrics)
if args.ylog != -1:
    adjusted_metrics = apply_log(args.y, args.ylog, adjusted_metrics)

if args.plot_type == "point" and args.trendline:
    adjusted_metrics = add_trendline_metrics(adjusted_metrics, x, y)

home = eu("~")

subset = open('adjusted_metrics.txt', 'w')
for row in adjusted_metrics:
    for item in row:
        subset.write(str(item) + "\t")
    subset.write("\n")
subset.close()

pt = args.plot_type

#create Rscript(s) to generate plots
rfile = open('temp.r', 'w')
#rfile.write("assign(\".lib.loc\", \"C:/Users/linds/Documents/R/win-library/3.4/\", envir = environment(.libPaths))\n")
rfile.write("library(ggplot2);\nlibrary(scales)\n")
rfile.write("datat <- read.table(\"adjusted_metrics.txt\", header=T);\n")
rfile.write("png(filename=\"" + args.o + ".png\", height = 800, width = 800);\n\n")
rfile.write("p <- ggplot(datat)\n")
if pt == "col":
    rfile.write("p + geom_" + pt + "(aes(x=factor(" + x + "), y=" + y)
else:
    rfile.write("p + geom_" + pt + "(aes(x=" + x + ", y=" + y)
if hl and pt == "line" and args.c != "NONE":
    rfile.write(", color=factor(" + c + "), linetype=(" + lt + ")")
elif pt == "line" and args.c != "NONE" and args.l != "NONE":
    rfile.write(", color=factor(" + c + "), linetype=(" + lt + ")")
elif hl:
    rfile.write(", color=factor(" + lt + ")")
elif args.c != "NONE" and pt != "col":
    rfile.write(", color=factor(" + c + ")")
if args.c != "NONE" and pt == "col":
    rfile.write(", fill=factor(" + c + ")")
    rfile.write("), color=\"black\"")
    rfile.write(") + xlab(\"" + x + "\") + ylab(\"" + y + "\")")
else:
    rfile.write(")) + xlab(\"" + x + "\") + ylab(\"" + y + "\")")
if args.w != "NONE":
    rfile.write(" + facet_wrap(\"" + w + "\")")

if pt == "point" and args.trendline:
    rfile.write(" + geom_line(aes(x=" + x + ", y=trendline_value")
    if args.c != "NONE":
        rfile.write(", color=factor(" + c + ")))")
    elif args.c != "NONE" and args.l != "NONE":
        rfile.write(", color=factor(" + c + "), linetype=(" + lt + ")))")
    if args.w != "NONE":
        rfile.write(" + facet_wrap(\"" + w + "\")")
rfile.write("\n")

rfile.close()

sub.run("Rscript temp.r", shell = True)
#sub.run("rm temp*.r", shell = True)
