#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastq-species-bowtie2.pl - Given a fastq file, align some or all of the sequences to all risdb/ensembl species and count how many hits there are to each species.

=head1 SYNOPSIS

fastq-species-bowtie2.pl R1.fastq [ R2.fastq ]

Options:
 -n integer : number of reads from the fastq file to align (default is 10000)
 -t integer : number of bowtie jobs to run at a time (default = 1) (Each bowtie job runs 8 threads)
 -s string : Comma-delimited list of species to align reads against (common names or Genus_species names)
 -v         : verbose
 -h         : help

=head1 DESCRIPTION

fastq-species-bowtie2.pl can be used to align fastq reads against all ensembl Bowtie2 indexes in /home/umii/public/ensembl in order to determine what species the fastq file contains, and if there are significant amounts of contaminating sequence from other species. The -n option is used to specify how many reads from the input fastq file should be aligned (default is 10,000). The -t option specifies how many processor cores to use (default is 1, this script uses GNU parallel so it can run across multiple nodes). 

=head1 EXAMPLE

fastq-species-bowtie2.pl /home/msistaff/public/garbe/sampledata/RNAseq/Hsap/fastq/heart-1_R1.fastq

    Hits Percent Species
    2058 79.98 Homo_sapiens
    1683 65.41 Pan_troglodytes
    1468 57.05 Pongo_abelii
    1229 47.77 Papio_anubis
    1222 47.49 Macaca_mulatta
    .
    .
    .

=cut

use Getopt::Std;
use Pod::Usage;

$usage = "Usage: fastq-species-bowtie2.pl [-n number_of_sequences_to_align] [-t num_threads] [-s human,mouse,maize] input_R1.fastq [ input_R2.fastq ]\n";

die $usage if ($#ARGV < 0);

# die gracefully: catch interrupt signals
$SIG{'INT'} = sub { &countprint; exit; };
$SIG{'TERM'} = sub { &countprint; exit; };
$SIG{'KILL'} = sub { &countprint; exit; };

# parameters 
getopts('n:t:s:hv', \%opts) or die "$usage\n";;
pod2usage(q(-verbose) => 3) if ($opts{'h'});
die "ERROR: Fastq file must be listed after command-line options\n$usage" if ($#ARGV > 1);
$R1 = $ARGV[0]; # input fastq file
$R2 = $ARGV[1] // "";
$seqs = $opts{'n'} // 10000; # number of sequences to align
$threads = $opts{'t'} || 1; # number of cores to run on
$verbose = $opts{'v'} // 0;
$commandfile = "tmp.command.txt";
@species = split /,/, $opts{'s'} if ($opts{'s'});
foreach $species (@species) {
    $species{$species} = 1;
}
die "Cannot find or open $R1\n" if (! -r $R1);
die "Cannot find or open $R2\n" if ($R2 && (! -r $R1));

$lines = $seqs * 4; # number of lines to pull out of fastq file

print "Creating temp file with $seqs reads\n" if ($verbose);
if ($R1 =~ /\.gz/) {
    `gunzip -c $R1 | head -n $lines > fsb.R1.tmp`;
    die "Error: $!\n" if ($?);
} else {
    `head -n $lines $R1 > fsb.R1.tmp`;
    die "Error: $!\n" if ($?);
}
$R1 = "fsb.R1.tmp";
if ($R2) {
    if ($R2 =~ /\.gz/) {
	`gunzip -c $R2 | head -n $lines > fsb.R2.tmp`;
    } else {
	`head -n $lines $R2 > fsb.R2.tmp`;
    }
    $R2 = "fsb.R2.tmp";
}
$totalreads = `wc -l < $R1`;
chomp $totalreads;
$totalreads = $totalreads / 4;

if ($threads > 1) { 
    open COMMAND, ">$commandfile" or die "Cannot open temp command file $commandfile $!\n";
}

# Get list of all genomes, if genomes not specified by user
if (! %species) {
    @specieslist = `find /home/umgc/public/bin/ensembl/1.0/modules /home/umgc/public/bin/genomes/1.0 /home/umgc/public/bin/genomes-other/1.0 -mindepth 1 -maxdepth 1 -type d`;
    foreach $species (@specieslist) {
	chomp $species;
	$species =~ /.*\/(.*)/;
	$name = $1;
	$species{$name} = 1;
    }
}

# get bowtie2 paths for all species
foreach $species (sort keys %species) {
    $index = `BOWTIE2_INDEX=""; module load $species; echo \$BOWTIE2_INDEX`;
    chomp $index;
    if ($index eq "") {
	print "No index for species $species found, skipping...\n";
	delete $species{$species};
    }
    $index{$species} = $index;
    $name = `module load $species; echo \$COMMON_NAME`;
    chomp $name;
    $name{$species} = $name;
}


foreach $species (sort keys %species) {
    $index = $index{$species};
    $name = $name{$species} // "";
    print STDERR "Running Bowtie2 on $species ($name)\n";

    ### Align with Bowtie2 ###
    $grepcommand = "grep -v ^\@ | cut -f1 | sort | uniq > fsb.$species.ids; wc -l < fsb.$species.ids"; 

    if ($R2) {
	$command = "echo -ne '$species\t'; bowtie2 -p 8 -k 1 --very-fast-local --no-unal -x $index -1 $R1 -2 $R2 2>/dev/null | $grepcommand";
    } else {
	$command = "echo -ne '$species\t'; bowtie2 -p 8 -k 1 --very-fast-local --no-unal -x $index -U $R1 2>/dev/null | $grepcommand";
    } 

    if ($threads == 1) {
	$line = `module load bowtie2; $command`;
	chomp $line;
	($myname, $count) = split /\t/, $line;
	$percent = &round100($count / $totalreads * 100);
	print "ERROR: $myname != $name\n" if ((lc($myname) ne lc($name)) and ($name ne "unknown"));;
	$rawdata{$myname}{count} = $count;
	$rawdata{$myname}{percent} = $percent;
    } else {
	print COMMAND "$command\n";
    }
    print STDERR "$command\n" if ($verbose);
}

if ($threads > 1) {
    close COMMAND;
    open GP, "module load parallel; module load bowtie2; cat $commandfile | parallel -j $threads |" or die "Cannot start GNU parallel\n";
    while ($line = <GP>) {
	chomp $line;
#	print "GP: $line\n";
	chomp $line;
	($myname, $count) = split /\t/, $line;
	$percent = &round100($count / $totalreads * 100);
	$rawdata{$myname}{count} = $count;
	$rawdata{$myname}{percent} = $percent;
    }
    close GP;
}

######### New shit ########
# count reads aligning to each species, if a read aligns to more than one, assign it to the species with the most reads
foreach $species (sort {$rawdata{$b}{count} <=> $rawdata{$a}{count}} keys %rawdata) {
    open IFILE, "fsb.$species.ids" or die "cannot open fsb.$species.ids: $!\n";
    while ($readid = <IFILE>) {
	chomp $readid;
	next if ($found{$readid});
	$found{$readid} = 1;
	$data{$species}{count}++;
    }
}

$totalaligned = keys %found;
foreach $species (sort keys %species) {
    $data{$species}{count} = $data{$species}{count} // 0;
    $data{$species}{percent} = &round100($data{$species}{count} / $totalreads * 100);
}

&countprint;

exit 0;

############################### subs ############################

# print out the counts per species
sub countprint {
    print "Hits\tPercent\tSpecies\n";
    foreach $name (sort { $data{$b}{count} <=> $data{$a}{count} } keys %data) {
	print "$data{$name}{count}\t$data{$name}{percent}\t$name";
	print "\t$name{$name}" if ($name{$name} and ($name{$name} ne "unknown"));
	print "\n";
    }
#    $multi = $count{Multi};
#    $multipct = &round100($multi / $totalreads * 100);
#    print "$multi\t$multipct\tMulti\n";
    $nohit = $totalreads - $totalaligned; # - $multi;
    $nohitpct = &round100($nohit / $totalreads * 100);
    print "$nohit\t$nohitpct\tNoHit\n";
}

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

### die gracefully ###
sub diegracefully {
# catch interrupt signals
    $SIG{'INT'} = &countprint;
    $SIG{'TERM'} = &countprint;
    $SIG{'KILL'} = &countprint;
}
