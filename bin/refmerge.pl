#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

refmerge.pl - Merge the fasta and gtf files of two genomes

=head1 SYNOPSIS

refmerge.pl name1 genome1.fa genome1.gtf name2 genome2.fa genome2.gtf

=head1 DESCRIPTION

This script takes two gtf files, two whole-genome fasta files, and two names, and prepends the names to the chromosome names in the gtf and fasta files. The script generates a "merged.fa" and "merged.gtf" file. This doesn't work if there are comment lines (lines starting with #) in the gtf files. It's best if the gene/transcript names and IDs in your GTF files are unique. If not, try the "-n" option to have prefixes added to "gene_id" and "gene_name" attributes in the GTF files. This is not a particularily robust script and is not designed to handle all types of GTF and GFF files, check the output of the script to verify it was generated as you intended.

Options:
    -h : Display usage information
    -n : Also modify gene names and ids in gtf file

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_n);

$usage = "refmerge.pl name1 genome1.fa genome1.gtf name2 genome2.fa genome2.gtf\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hn');
pod2usage(q(-verbose) => 3) if ($opt_h);

die $usage unless ($#ARGV == 5);
my ($name1, $genome1fa, $genome1gtf, $name2, $genome2fa, $genome2gtf) = @ARGV;

$outfa = "merged.fa";
$outgtf = "merged.gtf";

### convert fasta files ###
print STDERR "Merging reference fasta files...\n";
# copy first genome file, prefix chr names with name
$result = `sed 's/^>/>$name1/g' $genome1fa > $outfa`;
print $result;
# add newline to end of file if needed
$result = `sed -i -e '\$a\\' $outfa`;
print $result;
# prefix second chr names with name, append to file
$result = `sed 's/^>/>$name2/g' $genome2fa >> $outfa`;
print $result;

### convert gtf files ###
print STDERR "Merging GTF files...\n";

# modify gene_name and gene_id fields in gtf file, if requested by -n option
if ($opt_n) {
    $result = `sed 's/_id "/_id "$name1/g' $genome1gtf | sed 's/_name "/_name "$name1/g' | sed 's/^/$name1/g' > $outgtf`;

    # add newline to end of file if needed
    $result = `sed -i -e '\$a\\' $outgtf`;

    $result = `sed 's/_id "/_id "$name1/g' $genome2gtf | sed 's/_name "/_name "$name1/g' | sed 's/^/$name2/g' $genome2gtf >> $outgtf`;

} else {

    # copy first gtf file, prefix chr names with name
    $result = `sed '/^#/!s/^/$name1/g' $genome1gtf > $outgtf`;
#    $result = `sed 's/^/$name1/g' $genome1gtf > $outgtf`;
    print $result;
    print "6";
    # add newline to end of file if needed
    $result = `sed -i -e '\$a\\' $outgtf`;
    print $result;
    # prefix second chr names with name, append to file
    $result = `sed '/^#/!s/^/$name2/g' $genome2gtf >> $outgtf`;
#    $result = `sed 's/^/$name2/g' $genome2gtf >> $outgtf`;
    print $result;
    print "7";

}

print STDERR "Finished, please check output files ($outfa and $outgtf) for correctness\n";
