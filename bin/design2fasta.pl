#!/usr/bin/perl -w

###########################################
# design2fasta.pl
# John Garbe
# October 2018
#
# Convert ampliseq/hoffseq design file to fasta file
#
#aecSequenceCentralTargetaec SequenceOutputForDesign rs #Assay Name
#GTAAAAAGAAGAACCGTCTTCTCTTGTAGACAGGGCCCTCTATCT[A/G]TGTGGTGCATCCTTTATATCTCCATATACTGATCTTTAAAACATG GTGGAGACACAGCCAAACCATATCACTATGTGTTCATGAGCTGTGTGAAGAGTCTGTAAAAAGAAGAACCGTCTTCTCTTGTAGACAGGGCCCTCTATCT[A/G]TGTGGTGCATCCTTTATATCTCCATATACTGATCTTTAAAACATGGCATGTGCCACGCCTGCATCAGGATCCATCTGAGAGGAAGAACACTCCAGAAGGC rs10007810 rs10007810
#
###########################################

=head1 DESCRIPTION

design2fasta.pl - convert ampliseq/hoffseq design file to fasta file

=head1 SYNOPSIS

design2fasta.pl --designfile file [--idfile file] [--outputfile design.fa]

=head1 OPTIONS

Options:

 --designfile file : Design file
 --idfile file : list of assay IDs to use (subset of assays present in design file)
 --outputfile file : Name for output fasta file (default: design.fa)
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{outputfile} = "design.fa";
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "idfile=s" => \$args{idfile},
	   "designfile=s" => \$args{designfile},
	   "outputfile=s" => \$args{outputfile},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}

### Handle Parameters ###
$args{designfile} = abs_path($args{designfile}) if ($args{designfile});
die "--designfile is required\n" unless ($args{designfile}); 

# process id file
if ($args{idfile}) {
    open IFILE, $args{idfile} or die "cannot open $args{idfile}: $!\n";
    while ($line = <IFILE>) {
	chomp $line;
	$good{$line} = 1;
    }
}

open OFILE, ">$args{outputfile}" or die "cannot open $args{outputfile}: $\n";
open IFILE, $args{designfile} or die "cannot open $args{designfile}: $!\n";
$header = <IFILE>;
chomp $header;
@header = split /\t/, $header;
$columns = $#header+1;
die "Error: design file format error: the design file should have 4 tab-delimited columns, $columns columns found\n" if ($#header != 3);
while ($line = <IFILE>) {
    chomp $line;
    my ($target, $design, $rs, $name) = split /\t/, $line;
#    print "target: $target\n";
    next if ($args{idfile} && ! $good{$rs});
    $target =~ /(\w+)\[(.*)\](\w+)/;
    $pre = $1;
    $variant = $2;
    $post = $3;
    
    @variants = split /\//, $variant;
    $a = shift @variants;
    $ref = $pre . $a . $post;
    print OFILE ">$rs-ref\n$ref\n";
#    push @rs, $rs;
#    push @alleles, "$rs-ref";
    $variants{"$rs-ref"} = $a;
    
    $altcount = 1;
    foreach $b (@variants) {
	$alt = $pre . $b . $post;
	$variants{"$rs-alt-$altcount"} = $b;
	print OFILE ">$rs-alt-$altcount\n$alt\n";    
#	push @alleles, "$rs-alt-$altcount"; 
	$altcount++;
    }
}
close IFILE;
close OFILE;

