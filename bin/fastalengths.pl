#!/usr/bin/perl -w

###########################################################
# fastalengths.pl
# John Garbe
# February 2019 - happy 40th!
#
# Compute the length of sequences in a fasta file
#
###########################################################

=head1 NAME

fastalengths.pl

=head1 SYNOPSIS

fastalengths.pl input.fasta

=head1 DESCRIPTION

Prints out the length of each sequence in a fasta file

=cut

# See this post for a version that uses a module: http://seqanswers.com/forums/showthread.php?t=15856

$usage = "fastalengths.pl input.fastq\n";
die $usage unless ($#ARGV == 0);
$ifile = $ARGV[0];
open IFILE, "<$ifile" or die "Cannot open input file $ifile: $!\n";;
$/ = ">"; # define new line separater

my $junkfirstone = <IFILE>;

while (<IFILE>) { # one line equals one sequence entry
    chomp;
    my ($def,@seqlines) = split /\n/, $_;
    my $seq = join '', @seqlines;
    print "$def\t" . length($seq) . "\n";
}
