#!/usr/bin/perl -w

############################################
# bamflags
# John Garbe
# May 2012
#
# Count up the number of alignments with each bam flag
# Helpful for getting a detail summary of what types of alignments
# are in a bam file.
# Flag states from http://ppotato.wordpress.com/2010/08/25/samtool-bitwise-flag-paired-reads/
###########################################

=head1 NAME

bamflags.pl - Count the number of alignments with each bam flag

=head1 SYNOPSIS

bamflags.pl accepted_hits.bam

=head1 DESCRIPTION

Helpful for getting a detailed summary of what types of alignments are in a bam file. Flag states from ppotato.wordpress.com/2010/08/25/samtool-bitwise-flag-paired-reads/

=cut

die "USAGE: bamflags.pl accepted_hits.bam\n" unless ($#ARGV == 0);

$bamfile = $ARGV[0];
die "cannot open $bamfile" unless (-e $bamfile);

# read in bam file as a pipe from samtools
open IFILE, "samtools view $bamfile |" or die "cannot open $bamfile\n";

# initialize array counting multiple alignments
for $i (0..200) {
    $count[$i] = 0;
}
$count = 0;

# read through bam file, counting alignments
while ($line = <IFILE>) {
    @line = split /\t/, $line, 3;
    $count[$line[1]]++;
    $count++;
}

print "$count alignments in $bamfile\n\n";
# singleton
$singleton = 0;
print "### One of the mate is unmapped ###\n";
print "Flag: 73  pair: 1 map+ unmap. $count[73]\n";
print "Flag: 133 pair: 2 unmap. map+ $count[133]\n";

print "Flag: 89  pair: 1 map+ unmap- $count[89]\n";
print "Flag: 121 pair: 1 map- unmap- $count[121]\n";
print "Flag: 165 pair: 2 unmap+ map- $count[165]\n";
print "Flag: 181 pair: 2 unmap- map- $count[181]\n";

print "Flag: 101 pair: 1 unmap+ map- $count[101]\n";
print "Flag: 117 pair: 1 unmap- map+ $count[117]\n";
print "Flag: 153 pair: 2 map- unmap+ $count[153]\n";
print "Flag: 185 pair: 2 map- unmap- $count[185]\n";

print "Flag: 69  pair: 1 unmap+ map+ $count[69]\n";
print "Flag: 137 pair: 2 map+ unmap+ $count[137]\n";

for $i (73, 133, 89, 121, 165, 181, 101, 117, 153, 185, 69, 137) {
    $singleton += $count[$i];
    $count[$i] = 0;
}
print "$singleton singletons\n\n";

#unmapped
print "### Both unmapped ###\n";
print "Flag: 77  pair: 1 unmap+ unmap+ $count[77]\n";
print "Flag: 141 pair: 2 unmap+ unmap+ $count[141]\n";

$unmapped = 0;
for $i (77, 141) {
    $unmapped+= $count[$i];
    $count[$i] = 0;
}
print "$unmapped unmapped\n\n";

# mapped correct
print "### Mapped in correct orientation within insert size ###\n";
print "Flag: 99  pair: 1 map+ map- $count[99]\n";
print "Flag: 147 pair: 2 map- map+ $count[147]\n";

print "Flag: 83  pair: 1 map- map+ $count[83]\n";
print "Flag: 163 pair: 2 map+ map- $count[163]\n";

$correctcorrect = 0;
for $i (99, 147, 83, 163) {
    $correctcorrect += $count[$i];
    $count[$i] = 0;
}
print "$correctcorrect correct orientation and insert size\n\n";

# mapped with correct insert size but wrong orientation
print "### Mapped within the insert size but wrong orientation ###\n";
print "Flag: 67  pair: 1 map+ map+ $count[67]\n";
print "Flag: 131 pair: 2 map+ map+ $count[131]\n";

print "Flag: 115 pair: 1 map- map- $count[115]\n";
print "Flag: 179 pair: 2 map- map- $count[179]\n";

$correctswitched = 0;
for $i (67, 131, 115, 179) {
    $correctswitched += $count[$i];
    $count[$i] = 0;
}
print "$correctswitched correct insert size but wrong orientation\n\n";

# wrong insert size
print "### Wrong insert size ###\n";
print "Flag: 81  pair: 1 map- map+ $count[81]\n";
print "Flag: 161 pair: 2 map+ map- $count[161]\n";

print "Flag: 97  pair: 1 map+ map- $count[97]\n";
print "Flag: 145 pair: 2 map- map+ $count[145]\n";

print "Flag: 65  pair: 1 map+ map+ $count[65]\n";
print "Flag: 129 pair: 2 map+ map+ $count[129]\n";

print "Flag: 113 pair: 1 map- map- $count[113]\n";
print "Flag: 177 pair: 2 map- map- $count[177]\n";

$correctwrong = 0;
for $i (81, 161, 97, 145, 65, 129, 113, 177) {
    $correctwrong += $count[$i];
    $count[$i] = 0;
}
print "$correctwrong wrong insert size\n\n";

print "### Other flags ###\n";
$other = 0;
for $i (0..200) {
    if ($count[$i] != 0) {
	print "$i $count[$i]\n";
	$other+= $count[$i];
    }
}
print "$other other flags\n";
exit;

