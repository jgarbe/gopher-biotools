#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

tophatplot.pl - Generate plots from tophat align_summary.txt output files

=head1 SYNOPSIS

tophatplot.pl align_summary1.txt [align_summary2.txt ...]
tophatplot.pl -f filelist.txt

=head1 DESCRIPTION

Generate a series of plots summarizing Tophat mapping percentage for multiple samples

Options:
    -f filelist.txt : Provide a file with a list of align_summary.txt files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: alignmentsummartmetricsplotrmd.pl align_summary.txt [align_summary2.txt ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of alignment files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# For debugging:
#for $i (0..$#files) { 
#    print "$files[$i] $samples[$i]\n";
#}
#print "@files\n";
#print "@samples\n";

# Count spikein bam alignments, if provided
#$spike = 0;
#foreach $bam (@bams) {
#    if (defined($bam) and -e $bam) {
#	$spike = 1;
#	print "grepping $bam\n";
#	$count = `grep -v "^@" $bam | wc -l`;
#	chomp $count;
#	push @spikecounts, $count;
#    } else {
#	push @spikecounts, 0;
#    }
#}


# run through the alignment files, saving stats
$paired = 0;
$unpaired = 0;
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";

    # read in data
    %thisdata = ();
    $category = "";
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    if ($line =~ /picard.analysis.AlignmentSummaryMetrics/) {
		$header = <IFILE>;
		chomp $header;
		@header = split /\t/, $header;
		while ($line = <IFILE>) {
		    chomp $line;
		    last if ($line =~ /^## /);
		    next if ($line =~ /^\s*$/); # skip blank lines
		    @line = split /\t/, $line;
		    for $i (1..$#line) {
			$category = "$line[0]";
			$thisdata{$category}{$header[$i]} = $line[$i];
		    }
		}
	    }
	}
    }

    ### save data ###
    $paired++ if ($category eq "PAIR");
    $unpaired++ if ($category eq "UNPAIRED");

    $total_count = $thisdata{$category}{"TOTAL_READS"} || 1; # 1 to avoid / 0 later 
    $aligned = $thisdata{$category}{"PF_READS_ALIGNED"}; 
    if ($category eq "PAIR") {
	$concord_count = $thisdata{$category}{"READS_ALIGNED_IN_PAIRS"};
	$total_count = $total_count / 2;
	$aligned = $aligned / 2; 
	$concord_count = $concord_count / 2;
	$discord_count = $aligned - $concord_count;
	$data{$ifile}{concord_count} = $concord_count;
	$data{$ifile}{concord_pct} = &round100($concord_count / $total_count * 100);	
	$data{$ifile}{discord_count} = $discord_count;
	$data{$ifile}{discord_pct} = $discord_count / $total_count * 100;
    }

    $data{$ifile}{unmapped_count} = $total_count - $aligned;
    $data{$ifile}{unmapped_pct} = ($total_count - $aligned) / $total_count * 100;;
    $data{$ifile}{alignedreads_count} = $aligned;
    $data{$ifile}{alignedreads_pct} = &round100($aligned / $total_count * 100);
    $data{$ifile}{total_count} = $total_count;

}

if (($paired > 0) and ($unpaired > 0)) {
    die "ERROR: mix of paired and un-paired samples not supported: $paired paired-end samples and $unpaired single-end samples\n";
}
if ($paired > 0) {
    $pe = 1;
    print "Paired data\n";
} else {
    $pe = 0;
    print "Unpaired data\n";
}


### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    $total{$sample} = $data{$ifile}{alignedreads_pct};
#    print "$sample\talignedreads\t$data{$ifile}{alignedreads_count}\n";
    print "$sample\tpct_alignedreads\t$data{$ifile}{alignedreads_pct}\n";
#    print "$sample\tconcordant\t$data{$ifile}{concord_count}\n" if ($pe);
    print "$sample\tpct_concordant\t$data{$ifile}{concord_pct}\n" if ($pe);
#    print "$sample\tdiscordant\t$data{$ifile}{discord_count}\n" if ($pe);
    print "$sample\tpct_discordant\t$data{$ifile}{discord_pct}\n" if ($pe);
#    print "$sample\ttotalreads\t$data{$ifile}{total_count}\n" if ($pe);
}
print "STATS End\n";

### Print out samples ordered by alignment
@order = sort {$total{$b} <=> $total{$a}} keys %total;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tAlignment rate\t$order\n";

### Plotting ###
$name = "picardalignmentplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating $name\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample";
if ($pe) {
    @metrics = ("concord_pct", "discord_pct");
    @metricnames = ("Concordant", "Disconcordant");
} else {
    @metrics = ("alignedreads_pct");
    @metricnames = ("Aligned");
}
foreach $metric (@metrics) {
    print OFILE "\t$metric";
}
print OFILE "\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print OFILE "$sample";
    foreach $metric (@metrics) {
	print OFILE "\t$data{$ifile}{$metric}";
    }
    print OFILE "\n";
}
close OFILE;

# plot the data
$traces = "";
for $metric (1..$#metrics) {
    $traces .= qq(%>% add_trace(y=~$metrics[$metric],  name = "$metricnames[$metric]", text = paste0(datat\$sample,"<br>$metricnames[$metric] ",round(datat\$$metrics[$metric],2),"%")) );
}

$title = "Alignment Rate";
$text = "Percent of reads aligned to reference is shown. These metrics are calculated by Picard collectalignmentsummarymetrics";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$metrics[0],
 name = "$metricnames[0]",
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>$metricnames[0] ",round(datat\$$metrics[0],2),"%") ) $traces
%>% layout(xaxis = sampleorder, dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
