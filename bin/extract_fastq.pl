#!/usr/bin/perl -w

######################################
# extract_fastq.pl
# John Garbe
# April 2013
#
# Given a list of sequence IDs and a fastq file, pull sequences out of the 
# fastq file
#
#####################################

=head1 NAME

extract_fastq.pl - Given a list of read IDs and a fastq file, pull reads out of the fastq file.

=head1 SYNOPSIS

extract_fastq.pl [-discard] ids.txt reads.fastq > out.fastq

=head1 DESCRIPTION

Given a file ids.txt containing a list of read IDs (one ID per line) and a fastq file reads.fastq, print out all reads from reads.fastq where the read ID matches an ID in ids.txt. If the -discard option is used, all reads from reads.fastq where the read ID does not match an ID in ids.txt will be printed out. ID matching works no matter if the IDs in ids.txt start with "@". If 1.7 style fastq read IDs are used, the /1 and /2 is removed from the end of each read ID before IDs are compared.

=cut

# process command-line options
die "USAGE: extract_fastq.pl [-discard] ids.txt reads.fastq > out.fastq\n" unless ($#ARGV == 1 or $#ARGV == 2);

$discard = 0; 
if ($ARGV[0] eq "-discard") {
    shift @ARGV;
    $discard = 1;
}

# read in list of IDs
$idfile = $ARGV[0];
open IDFILE, "$idfile" or die "Cannot open $idfile for reading\n";
$count = `cat $idfile | sort | uniq | wc -l`;
chomp $count;
print STDERR "reading $count unique IDs from $idfile...\n";
while ($id = <IDFILE>) {
    chomp $id;
    if ($id =~ /\/[12]$/) { # chop off the /1 and /2 on 1.7 style read ids
	chop $id;
	chop $id;
    }
    $id = "\@" . $id unless ($id =~ /^@/);
    $ids{$id} = 0;
}
close IDFILE;


# read through fastq file, look for and print out matching IDs
$fastqfile = $ARGV[1];
if ($fastqfile =~ /\.gz$/) {
    open FASTQFILE, "gunzip -c $fastqfile | " or die "Cannot open $fastqfile for reading via gunzip\n";
} else {
    open FASTQFILE, "$fastqfile" or die "Cannot open $fastqfile for reading\n";    
}

print STDERR "Reading sequences from $fastqfile...\n";
$dcount = 0;
$ecount = 0;
while (<FASTQFILE>) {
    chomp;
    my ($id, $junk) = split;
    if ($id =~ /\/[12]$/) { # chop off the /1 and /2 on 1.7 style read ids
	chop $id;
	chop $id;
    }
    if (!$discard) {
	if (defined($ids{$id})) {
	    $ids{$id}++;
	    $ecount++;
	    print STDERR "\rExtracted $ecount reads" if ($ecount % 1000 == 0);
	    print "$_\n";
	    for $i (1..3) {
		$line = <FASTQFILE>;
		print $line;
	    }
	} else {
	    $dcount++;
	    for $i (1..3) {
		$line = <FASTQFILE>;
	    }
	}
    }
    if ($discard) {
	if (defined($ids{$id})) {
	    $dcount++;
	    $ids{$id}++;
	    for $i (1..3) {
		$line = <FASTQFILE>;
	    }
	} else {
	    $ecount++;
	    print STDERR "\rExtracted $ecount reads" if ($ecount % 1000 == 0);
	    print "$_\n";
	    for $i (1..3) {
		$line = <FASTQFILE>;
		print $line;
	    }
	}
    }

}

# print out IDs without match
$first = 1;
while (($id, $count) = each(%ids)) {
    if ($count == 0) {
	if ($first == 1) {
	    print STDERR "IDs not found:\n";
	    $first = 0;
	}
	print STDERR "$id\n";
    }
}

print STDERR "\rExtracted $ecount reads\n";
print STDERR "\rDiscarded $dcount reads\n";

