#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014-2015 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

expressiontableplot.pl - Given a table of expression data, generate a series of summary plots including:
 -MDS plot
 -Dendrogram
 -Expression distribution violin plots
 -Expressed genes plot

=head1 SYNOPSIS

expressiontableplot.pl data.txt

=head1 DESCRIPTION

Generate a series of plots summarizing a table of expression data. The input file should be tab delimited with a header. There should be a row for each feature (gene, transcript, exon, etc), and a column for each sample. The first row should contain sample names and the first column feature IDs.

Options:
    -n : Normalize expression values: 75% quartile normalization
    -m integer : Minimum expression value
    -t string : Feature type (gene, transcript, exon, etc)
    -h : Display usage information
    -v : Verbose output

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h, $opt_v, $opt_m, $opt_t, $opt_n);

$usage = "USAGE: expressiontableplot.pl data.txt\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('m:hvt:n');
pod2usage(q(-verbose) => 3) if ($opt_h);

# get the input arguments
my $expressionfile = $ARGV[0];
my $minexpression = $opt_m // 1;
my $featuretype = $opt_t // "features";
my $normalize = $opt_n // 0;

$dendroplotfile = "dendrogramplot.png";
$mdsplotfile = "mdsplot.png";
$expressedplotfile = "expressedplot.png";
$distributionplotfile = "expressiondistributionplot.png";

die "Cannot find file $expressionfile" unless (-e $expressionfile);

### Normalize data ###
if ($normalize) {
    print "Quaartile normalizing data\n"; 
    $result = `quartile_norm.pl -c -1 -s 1 $expressionfile -o $expressionfile.norm`;
    print $result;
    $expressionfile = "$expressionfile.norm";
}

### read in the expression table ###
open IFILE, "<$expressionfile" or die "Could not open file $expressionfile\n";
# header looks like this: tracking_id q1_0 q2_1 q3_0 ...
my $header = <IFILE>;
chomp $header;
@header = split /\t/, $header;

while (<IFILE>) {
    chomp;
    @line = split /\t/;
    $features{$line[0]} = 1;
    for $i (1..$#line) {
	$sample = $header[$i];
#	print "$i: :$sample:\n";
	$line[$i] = 0 unless ($line[$i]);
	$fpkm{$sample}{$line[0]} = $line[$i];
	$expressed{$sample}++ if ($line[$i] >= $minexpression);
	$samplecounts{$sample} += $line[$i];
    }
}
close IFILE;
@sorted_features = sort( keys(%features));

### print stats to screen ###
print "STATS Start\n";
foreach $sample (sort keys(%fpkm)) {
    $expressed{$sample} = 0 unless ($expressed{$sample});
    print "$sample\t#expressedgenes\t$expressed{$sample}\n";
}
foreach $sample (sort keys(%fpkm)) {
    $samplecounts{$sample} = sprintf("%.1f", $samplecounts{$sample});
    print "$sample\t#abundancereads\t$samplecounts{$sample}\n";
}
print "STATS End\n";

### print out data in table format
$tmpfile = "tmp.dat";
open OFILE, ">$tmpfile" or die "Could not open file $tmpfile \n";
# print out header
foreach $feature (@sorted_features) {
    print OFILE "\t$feature";
}
print OFILE "\n";

# print out data
foreach $sample (keys(%fpkm)) {
    print OFILE "$sample";
    
    foreach $feature (@sorted_features) {
	print OFILE "\t$fpkm{$sample}{$feature}";
    }
    print OFILE "\n";
}
close OFILE;

### MDS and Dendrogram Plots ###
print "Generating MDS and Dendrogram plots\n";
my $r_script = "expressiontableplot.r";
my $height = 480;
my $width = 480;
my $numsamples = keys %fpkm;
if ($numsamples > 6) {
    $width = 480 + (20 * ($numsamples-6));
}
$dendrowidth = $width / 2;

if (0) { # skip since expressiontablepca.pl does these
open RFILE, ">$r_script" or die "Cannot open $r_script\n";
print RFILE "
  datat <- read.table(\"$tmpfile\"); 
  png(filename=\"$dendroplotfile\", height = $height, width  = $dendrowidth); 
  hc = hclust(dist(datat));
  plot(hc, hang = -1);
  title(xlab= \"Samples\"); 
  title(ylab= \"Height\"); 
  dev.off();

  library(ggplot2);
  datat <- read.table(\"$tmpfile\", header=T); 
  cmd <- cmdscale(dist(datat));
  cmd2 <- data.frame(cmd);
  cmd3 <- cbind(Sample=rownames(cmd2), cmd2);
  colnames(cmd3) <- c(\"Sample\", \"Xvar\", \"Yvar\");
  png(filename=\"$mdsplotfile\"); 
#  ggplot(data=cmd3, aes(x=Xvar, y=Yvar, color=Sample, label=Sample)) + geom_point(shape=1) + geom_text(size=3);
  ggplot(data=cmd3, aes(x=Xvar, y=Yvar, color=Sample, label=Sample)) + geom_text(size=3) + xlab(\"Dimension 1\") + ylab(\"Dimension 2\") + theme(legend.position=\"none\");
  dev.off();

  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");
}

### Expressed Genes Plot ###
print "Generating Expressed $featuretype plot\n";
open OFILE, ">$tmpfile" or die "Could not open file $tmpfile \n";

# print out header
print OFILE "Value\tSample\n";

# print out data
foreach $sample (keys(%fpkm)) {
    print OFILE "$expressed{$sample}\t$sample\n";
}
close OFILE;

open RFILE, ">$r_script" or die "Cannot open $r_script\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$tmpfile\", header=T, colClasses=c(\"Sample\"=\"factor\")); 
  png(filename=\"$expressedplotfile\", height = $height, width = $width); 
  ggplot(datat, aes(x = Sample, y = Value, fill=Sample)) + geom_bar(stat = \"identity\") + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"Number of $featuretype with expression >= $minexpression\") + theme(legend.position=\"none\");
  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

### print out data in ggplot format
open OFILE, ">$tmpfile" or die "Could not open file $tmpfile \n";

# print out header
print OFILE "Value\tSample\n";

# print out data
foreach $sample (keys(%fpkm)) {
    foreach $feature (@sorted_features) {
	print OFILE "$fpkm{$sample}{$feature}\t$sample\n" unless ($fpkm{$sample}{$feature} < $minexpression);
    }
}
close OFILE;

### Expression Violin Plot ###
print "Generating Expression distribution plot\n";
open RFILE, ">$r_script" or die "Cannot open $r_script \n\n";
print RFILE "
  library(ggplot2);
  datat <- read.table(\"$tmpfile\", header=T, colClasses=c(\"Sample\"=\"factor\"))
  png(filename=\"$distributionplotfile\", height = $height, width = $width);

  p <- ggplot(datat, aes(factor(Sample), log10(Value), fill=Sample))
  p + geom_violin(scale=\"count\", adjust=.5) + geom_boxplot(width=.1, outlier.size=0) + theme(axis.text.x = element_text(angle = 90)) + xlab(\"Sample\") + ylab(\"log10(expression)\") + theme(legend.position=\"none\");

  dev.off();
  #eof" . "\n";

close RFILE;
system("R --no-restore --no-save --no-readline < $r_script > $r_script.out");

