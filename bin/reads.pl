#!/usr/bin/perl

#####################################
# reads.pl
# John Garbe
# November 2017
#
# Count number of reads in a fastq file
#
#####################################

use Getopt::Std;
our ($opt_h);

$usage = "USAGE: reads.pl file.fastq\n";

die $usage unless getopts('h');
die $usage unless ($#ARGV == 0);
die $usage if ($opt_h);

$file = shift (@ARGV);
if ($file =~ /\.gz$/) {
    $lines = `gunzip -c $file | wc -l`; 
} else {
    $lines = `wc -l < $file`;
}
chomp $lines;
$reads = $lines / 4;
print "$reads reads\n";

