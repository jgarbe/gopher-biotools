#!/usr/bin/perl -w

#####################################
# fastqcrop.pl
# John Garbe
# June 2018
#####################################

=head1 DESCRIPTION

fastqcrop.pl - Crop reads in a fastq[.gz] file down to a certain length

=head1 SYNOPSIS

fastqcrop.pl [--crop 50] [--headcrop 2] --fastq sample.fastq[.gz] > sample-crop.fastq

=head1 OPTIONS

This script handles single-end read datasets only. 

Options:

 --headcrop integer : remove bases from the beginning of each read
 --crop integer : crop read down to specified length (after applying headcrop, if specified)
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=head1 EXAMPLE

Remove the first 3 bases, keep the next 50 bases, discard everything after:
fastqcrop.pl --crop 50 --headcrop 3 sample.fastq > sample-crop.fastq

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{threads} = 10;
$args{headcrop} = 0;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "fastq=s" => \$args{fastq},
	   "crop=i" => \$args{crop},
	   "headcrop=i" => \$args{headcrop},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
$args{fastq} = abs_path($args{fastq}) if ($args{fastq});

### Handle Parameters ###

die "--fastq is required\n" unless ($args{fastq}); 

die "--crop is required\n" unless ($args{crop});

$gz = ($args{fastq} =~ /\.gz$/) ? 1 : 0; # check if input is gzip compressed

if ($gz) {
    open IFILE, "gunzip -c $args{fastq} |" or die "Can’t open gzipped input file $args{fastq}: $!\n";
} else {
    open IFILE, "$args{fastq}" or die "Cannot open input file $args{fastq}: $!\n";
}

while ($id = <IFILE>) {
    $seq = <IFILE>;
    $plus = <IFILE>;
    $quality = <IFILE>;

    chomp $seq;
    chomp $quality;
    $seq = substr($seq, $args{headcrop}, $args{crop});
    $quality = substr($quality, $args{headcrop}, $args{crop});

    print "$id$seq\n$plus$quality\n";
}


################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev {
    my($data) = @_;
    if(@$data == 1) {
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}



