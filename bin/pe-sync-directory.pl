#!/usr/bin/perl -w

###############################################
# pe-sync-directory.pl
# John Garbe
# January 2012
#
# Paired-end read file synchronization script: checks if left and right 
# reads are in the same order.
# This script checks the folder specificd on the command line. 
# It looks in the folder for paired-end reads, checks them and prints 
# the results to STDERR. The number of passed or failed files is printed 
# to STDOUT and the script returns 1 if there was an error and 0 if all 
# files passed.
# The "quick" option will not report the percent of out-of-sync reads
# for many failed files, but will run much faster.
#
# This script should run fine on linux systems or solaris 
# (which is what the project space fileserver runs)
#
# This script can handle Casava 1.8.0 style read IDs, and pre 1.8.0 style ids. # Other types of read ID formats will cause a terminal error. All files
# in a folder must have the same ID format.
#
# Fastq filename formats are defined on the lines commented PATTERN MATCHING
#
###############################################

die "Usage: pe-sync-directory.pl [quick] /path/to/fastq_dir/\n" unless ($#ARGV >= 0);

# determine if we're running as root in order to deactivate permission checks for root users
$root = 0; # set to 1 if running as root, 0 otherwise
$root = 1 if ( $< == 0 ); # real user ID
$root = 1 if ( $> == 0 ); # effective user ID
if ($root) {
#    print STDERR "Running as root\n";
} else {
#    print STDERR "Running as user " . `whoami`;
}

# determine if we're running on solaris in order to change system calls
$output = `perl -v`;
$solaris = 0;
$solaris = 1 if ($output =~ /solaris/);
#    print STDERR "Running on Solaris\n" if ($solaris);

# handle "quick" option
$quick = 0; # 1 for quickcheck, 0 for thorough check
if (defined($ARGV[0])) { 
    if ($ARGV[0] eq "quick") {
	$quick = 1;
	shift @ARGV;
    }
}
print STDERR "QUICK CHECK enabled\n" if ($quick);

# check access to folder
$dir = $ARGV[0];
if (!(-d "$dir")) {
    print STDERR "no $dir folder\n";
    print STDOUT "no $dir folder\n";
    exit 1;
}
($folder) = $dir =~ /\S+\/(\w+)\//; # grab the folder name, not the full path
$folder = $dir unless (defined($folder)); #if we failed, just use the original

# skip this checking for read access
# When run as root the folder will look unreadable, but we can still read it
if (!(-r -x "$dir") && !($root)) {
    print STDERR "$folder: no access to $dir\n";
    print STDOUT "no access to $dir\n";
    exit 1;
}

# grab list of fastq files, handle two types of filename styles
@R1files = `ls $dir/*_R1_*\.fastq 2> /dev/null`; # PATTERN MATCHING #
@R2files = `ls $dir/*_R2_*\.fastq 2> /dev/null`; # PATTERN MATCHING #

@d1files = `ls $dir/*.1.fastq 2> /dev/null`; # PATTERN MATCHING #
@d2files = `ls $dir/*.2.fastq 2> /dev/null`; # PATTERN MATCHING #

if (($#R1files < 0) && ($#d1files < 0)) { # no fastq filenames matching pattern
    @fastqfiles = `ls $dir/*.fastq 2> /dev/null`;
    if ($#fastqfiles < 0) {
	print STDERR "$folder: No fastq files\n";
	print STDOUT "No fastq files\n";
    } else {
	print STDERR "$folder: Non-standard fastq filenames\n";
	print STDOUT "Non-standard fastq filenames\n";
    }
    exit 1;
}

if (($#R2files < 0) && ($#d2files < 0)) { # no paired-end reads
    print STDERR "$folder: No paired-end files\n";
    print STDOUT "No paired-end files\n";
    exit 1;
}

# determine which fastq naming style was used
if ($#R1files >= 0) {
    @files = @R1files;
    $filenametype = 1;
} elsif ($#d1files >= 0) {
    @files = @d1files;
    $filenametype = 2;
} else {
    print STDERR "$folder: Filename matching error\n";
    print STDOUT "Filename matching error\n";
    exit 1;
}

$filenum = $#files + 1;
print STDERR "Processing $filenum paired-end read libraries in $dir\n";

# identify read id format type
if ($solaris) {
    $readid = `head -1 $files[0]`;
} else {
    $readid = `head -n 1 $files[0]`;
}
chomp $readid;
if ($readid =~ /^@\S+\/[12]$/) { # @ at the start of the line followed by non-whitespace, a /, a 1 or 2, the end of the line
    $id_type = 1;
    print STDERR "Casava 1.7 read id style\n"; # TESTING
} elsif ($readid =~ /^@\S+\W[12]\S+$/) { # @ at the start of the line followed by non-whitspace, a space, a 1 or 2, non-whitespace
    $id_type = 2;
    print STDERR "Casava 1.8 read id style\n"; # TESTING
} else {
    print STDERR "Cannot determine read id style\n";
    print STDOUT "Unknown id style: $readid\n";
    exit 1;
}

# check each pair of PE fastq files
$filecount = 0;
$failcount = 0;
foreach $filename_r1 (@files) {
    chomp $filename_r1;
    $filecount++;
    $filename_r2 = $filename_r1;
    if ($filenametype == 1) {
	$filename_r2 =~ s/_R1_/_R2_/;
    } else {
	$filename_r2 =~ s/\.1\.fastq/\.2\.fastq/;
    }
    ($junk, $short_r1) = $filename_r1 =~ /(.*)\/(.+)/;
    ($junk, $short_r2) = $filename_r2 =~ /(.*)\/(.+)/;
    print STDERR "$short_r1\t$short_r2\t";
    if ($quick) {
	$failure = &quickcheck($filename_r1, $filename_r2); # quickie check
	print STDERR "FAILED quickcheck\n" if ($failure);
    }
    # If we aren't doing the quick check, or the quick check passed, do the slow check
    if (!($quick) || !($failure)) {
	$failure = &percent($filename_r1, $filename_r2); # slow thorough check
    }
    # check return value, count failed files
    if ($failure) {
#	print STDERR "-"; # let user know something is happening
	$failcount++;
    } else {
#	print STDERR "+"; # let user know something is happening
    }
}
print STDERR "\n";
if ($failcount == 0) {
    print STDOUT "PASSED $filecount read libraries\n";
    exit 0;
} else {
    print STDOUT "FAILED $failcount failed out of $filecount read libraries\n";
    exit 1;
}

######################## subs #########################
# do a quick screen for bad files (many bad files will pass this quick check)
sub quickcheck {
    my ($file1, $file2) = @_;
    if ($solaris) {
	$result1 = `tail -10000l $file1 | head -1`; # grab the line 10,000 lines from the EOF
	$result2 = `tail -10000l $file2 | head -1`;
    } else {
	$result1 = `tail -n 10000 $file1 | head -n 1`;
	$result2 = `tail -n 10000 $file2 | head -n 1`;
    }
    # process the ids depending on id style
    if ($id_type == 1) { # 1 or 2 at end of id
	chomp $result1;
	chomp $result2;
	chop $result1;
	chop $result2;
    } else { # first word is a unique read id, second word is left/right-specific
	($result1) = split ' ', $result1;
	($result2) = split ' ', $result2;
    }
    if ($result1 eq $result2) {
#	print "pass\n";
	return 0;
    } else {
#	print "fail\n";
	return 1;
    }
}

# compare every read id from each file and determine what percent of reads are
# out of sync
sub percent {
    my($file1, $file2) = @_;
    
    open R1, '<', $file1 or die "Couldn't open $file1 for reading:$!";
    open R2, '<', $file2 or die "Couldn't open $file2 for reading:$!";
    
    my $readCount = 0;
    my $failcount = 0;
    while (!eof(R1) and !eof(R2)) {
	$id1 = <R1>;
	$id2 = <R2>;
	next unless ($. % 4 == 1); # check every sequence (4th line in the file)
	# process the ids depending on id style
	if ($id_type == 1) { # 1 or 2 at end of id
	    chomp $id1;  # newline
	    chop $id1;   # last char of the id (should be the "1" or "2")
	    chomp $id2;
	    chop $id2;
	} else {
	    ($id1) = split ' ', $id1;
	    ($id2) = split ' ', $id2;
	}
	$failcount++ unless ($id1 eq $id2);
	$readCount++;
    }
    my $percent = $failcount / $readCount;
    $prettypercent = $percent * 100; # make it percent
    $prettypercent = sprintf("%.2f", $prettypercent);
    if ($failcount > 0) {
	print STDERR "FAILED full check\t$prettypercent% of reads are out-of-sync\n";
	$exitStatus=1;
    } else {
	print STDERR "PASSED full check\n";
	$exitStatus=0;
    }

    close R1;
    close R2;

    return $exitStatus;
}
