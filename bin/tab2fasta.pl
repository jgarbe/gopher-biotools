#!/usr/bin/perl -w

############################
# John Garbe
# July 2008
#
############################

=head1 NAME

fasta2tab.pl - Convert a tab-delimited text file of sequences into a fasta file

=head1 SYNOPSIS

fasta2tab.pl file.txt > file.fasta

=head1 DESCRIPTION

Take a tab-delimited file of sequences (two columns: ID and sequence) and turn it into a FASTA file

Options:
    -h : Display usage information

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: tab2fasta.pl file.txt > file.tab\n";

#die $usage unless ($#ARGV >= 0);
die $usage unless getopts('h');
pod2usage(q(-verbose) => 3) if ($opt_h);

#$ifile = $ARGV[0];
#open "IFILE", "<$ifile" || die("cannot open $ifile\n");

while ($line = <>) {
$line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
chomp $line;
($id, $seq) = split /\t/, $line;
print "$id\n";
for ($i = 0; $i < length($seq); $i+=80) {
$end = 80;
if (($i + 80) > length($seq)) { $end = length($seq) - $i; }
print substr($seq,$i,$end);
print "\n";
}
#print "\n";
}

exit 0;
