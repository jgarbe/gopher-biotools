#!/usr/bin/perl -w

###############################################################
#
# John Garbe
# November 2017 - Happy Thanksgiving
#
###############################################################

=head1 DESCRIPTION

Perform in silico size selection on paired-end fastq files

=head1 SYNOPSIS

 sizeselect.pl --help
 sizeselect.pl --r1 sample_r1.fastq --r2 sample_r2.fastq --min 100 --max 200

=head1 OPTIONS

 --r1 file : R1 fastq file
 --r2 file : R2 fastq file
 --min integer : minimum fragment size
 --max integer : maximum fragment size
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

###############################################################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;

$args{min} = 0;
$args{max} = 1000;
$args{threads} = 8;
$args{outputfolder} = "sizeselect";
GetOptions("help" => \$args{help},
	   "min=i" => \$args{min},
	   "max=i" => \$args{max},
	   "threads=i" => \$args{threads},
	   "r1=s" => \$args{r1},
	   "r2=s" => \$args{r2},
	   "outputfolder=s" => \$args{outputfolder},
	   "verbose" => \$args{verbose},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
pod2usage unless ($#ARGV == -1);
pod2usage unless ($args{r1} and $args{r2});
die "Cannot find R1 file $file\n" unless (-e $args{r1});
die "Cannot find R2 file $file\n" unless (-e $args{r2});

mkdir $args{outputfolder} if (! -e $args{outputfolder});
($r1name) = fileparse($args{r1});
($r2name) = fileparse($args{r2});

$length = `sed '2q;d' $args{r1} | wc -c`;
$length--; # subtract newline
$length2x = $length * 2;
if ($length * 2 < $args{max}) {
    print "WARNING: Read length appears to be $length, which means actual size selection will be $args{min}-$length2x instead of $args{min}-$args{max}\n";
}

print "Selecting reads from inserts with minimum length $args{min} and maximum length $args{max}\n";

# generate random string to name temporary output files to avoid clobbering things
$random = "";
for (1..4) {
    $ascii = int(rand(52));
    if ($ascii >= 26) {
	$ascii = $ascii + 71;
    } else { 
	$ascii = $ascii+ 65;
    }
    $random .= chr($ascii);
}


### run pear to stitch reads, run through stitched reads saving IDs of reads with good and bad lengths
$count = 0;
$goodcount = 0;
print "Stitching reads...\n";


$command = "pear -f $args{r1} -r $args{r2} -o $r1name.$random --threads $args{threads}";
$result = `$command`;
print $result if ($args{verbose});
$ifile = "$r1name.$random.assembled.fastq";
$ofile = "$r1name.$random.ids.txt";
print "Identifying stitched fragments of proper length...\n";
open IFILE, $ifile or die "Cannot open $ifile: $!\n";
open OFILE, ">$ofile" or die "Cannot open $ofile: $!\n";
while ($id = <IFILE>) {
    $count++;
    my $seq = <IFILE>;
    my $plus = <IFILE>;
    my $qual = <IFILE>;
    $length = length($seq) - 1; # minus one for newline character
    if (($length >= $args{min}) and ($length <= $args{max})) {
	($id) = split ' ', $id;
	print OFILE "$id\n";
	$goodcount++;
    }
}
close IFILE;
close OFILE;

### pull out good reads from original fastq file
print "Generating fastq files with reads from fragments of proper length...\n";
$newr1 = "$args{outputfolder}/$r1name";
$newr2 = "$args{outputfolder}/$r2name";

$gzip = "";
$gzip = "| gzip "if ($args->{r1} =~ /\.gz$/);
$command = "extract_fastq.pl $ofile $args{r1} $gzip > $newr1";
$result = `$command`;
$command = "extract_fastq.pl $ofile $args{r2} $gzip > $newr2";
$result = `$command`;
$args{r1} = $newr1;
$args{r2} = $newr2;

# save stats
print "\nGood sequences\t$goodcount\n";
print "%Good sequences\t" . round100($goodcount / $count * 100) . "\n" if ($count > 0);

# clean up
`rm $r1name.$random.assembled.fastq`;
`rm $r1name.$random.ids.txt`;

exit;


# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
