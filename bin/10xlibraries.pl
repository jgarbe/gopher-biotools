#!/usr/bin/perl -w

####################################
# 10xlibraries.pl
# John Garbe
# October 2019
#
#####################################

=head1 DESCRIPTION

10xlibraries.pl - Create a libraries file for Cellranger count

=head1 SYNOPSIS

libraries.pl --projectname Smith_Project_001 [--output libraries.csv]

=head1 OPTIONS

This script looks in your data_release folder for folders associated with the specified project, and creates a Cellranger libraries file based on the fasta files found

    Options:

 --projectname Smith_Project_001 : Name of a sequencing project
 --output libraries.csv : Name of the output file
 --group group : MSI group to look in (your user account will need to be a member of the group)
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

%type = ('adt' => "Antibody Capture",
	 'hto' => "Custom",
#	 'tcr' => "",
	 'gex' => "Gene Expression",
	 'expn' => "Gene Expression",
    );

# set defaults
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "projectname=s" => \$args{projectname},
	   "output=s" => \$args{output},
	   "group=s" => \$args{group},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
$args{file} = abs_path($args{file}) if ($args{file});

### Handle Parameters ###

die "--projectname is required\n" unless ($args{projectname});

# get current group
if (! defined($args{group})) {
    $groups = `groups`;
    ($args{group}) = split ' ', $groups;
}
$datarelease = "/home/$args{group}/data_release/umgc/novaseq";
print "Looking in $datarelease for folders from project $args{projectname}...\n";

# look for project folders
@folders = `find $datarelease -mindepth 2 -maxdepth 2 -type d`;
foreach $folder (@folders) {
    chomp $folder;
    if ($folder =~ /$args{projectname}/i) {
	print "MATCH $folder\n";
    } else {
	print "No Match $folder\n" if ($args{verbose});
	next;
    }
    $folder =~ /[-_](\w\w\w)$/;
    $type = lc($1 || "");
    if (!defined($type{$type})) {
	print "Folder is not of type adt, hto, or gex, ignoring: $folder\n";
	next;
    }

    # get fastq files
    @fastqs = `find $folder -maxdepth 1 -type f -name "*.fastq.gz"`;
    foreach $fastq (@fastqs) {
	chomp $fastq;
	($fastqname) = basename($fastq);
	print "$fastqname\n";
	if ($fastqname =~ /(.*)_S[\d]{1,4}_L00([1-8])_([IR][12])_[0-9]{3}\.fastq\.gz$/) {
	    $sample = $1;
	    $data{$folder}{$sample}{files}++;
	    $data{$folder}{$sample}{type} = $type;
	} else {
#	    die "File $fastqname doesn't match required format. Missing Lane (L001) is a common problem.\n";
	    # TESTING
	    print "File $fastqname doesn't match required format. Missing Lane (L001) is a common problem.\n";
	    next;
	}
    }

}

if (! %data) {
    die "no folders or files found\n";
}

$ofile = $args{outputfile} // "$args{projectname}-libraries.csv";
open OFILE, ">$ofile" or die "Cannot open $ofile: $!\n";
print OFILE "fastqs,sample,library_type\n";
foreach $folder (sort keys %data) {
    foreach $sample (sort keys %{ $data{$folder} }) {
	if ($data{$folder}{$sample}{files} % 3 != 0) {
	    print "In folder $folder sample $sample needs R1, R2, and I1 fastq files\n";
	    next;
	}
	print OFILE "$folder,$sample,$type{$data{$folder}{$sample}{type}}\n";
    }
}
close OFILE;
