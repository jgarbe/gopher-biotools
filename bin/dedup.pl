#!/usr/bin/perl -w

#####################################
# dedup.pl
# John Garbe
# November 2019
#####################################

=head1 DESCRIPTION

dedup.pl - deduplicate a fastq file (or pair of fastq files), generating a fasta or fastq file with read counts in the sequence names

=head1 SYNOPSIS

dedup.pl --fasta OR --fastq [--minreads 1] sample_R1.fastq [sample_R2.fastq] [--prefix output] [--samplename sample]

=head1 OPTIONS

Options:

 --fastq : output fasta format
 --fastq : output fastq format
 --prefix string : Output file name prefix
 --samplename sample : add sample=sample; annotation to sequence ID line
 --minreads 1 : Minimum number of reads to include sequence in output (default is 1)
 --help : Print usage instructions and exit
 --verbose : Print more information while running

=cut

##################### Initialize ###############################

use Getopt::Long;
use Cwd 'abs_path';
use File::Basename;
use Pod::Usage;
use File::Temp qw( tempdir );

# set defaults
$args{threads} = 10;
$args{prefix} = "dedup";
$args{minreads} = 0;
GetOptions("help" => \$args{help},
	   "verbose" => \$args{verbose},
	   "threads=i" => \$args{threads},
	   "minreads=i" => \$args{minreads},
	   "fasta" => \$args{fasta},
	   "fastq" => \$args{fastq},
	   "prefix=s" => \$args{prefix},
	   "samplename=s" => \$args{samplename},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV > 1) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
if ($#ARGV < 0) {
    print "Must provide at least one fastq file\n";
    pod2usage;
}
$r1 = shift @ARGV;
$r2 = shift @ARGV // "";
$r1 = abs_path($r1);
$r2 = abs_path($r2) if ($r2);

if ($r1 =~ /\.gz$/) {
    open R1, "gunzip -c $r1 |" or die "Can’t open gzipped input file $r1: $!\n";
} else {
    open R1, "$r1" or die "Cannot open input file $r1: $!\n";
}
if ($r2) {
    if ($r2 =~ /\.gz$/) {
	open R2, "gunzip -c $r2 |" or die "Can’t open gzipped input file $r2: $!\n";
    } else {
	open R2, "$r2" or die "Cannot open input file $r2: $!\n";
    }
}

# read in sequences from fastq file(s)
print STDERR "Reading fastq file(s)...\n" if ($args{verbose});
while ($line = <R1>) {
    $seq = <R1>;
    chomp $seq;
    $line = <R1>;
    $line = <R1>;
    if ($r2) {
	$line = <R2>;
	$seq2 = <R2>;
	chomp $seq2;
	$seq = "$seq:$seq2";
	$line = <R2>;
	$line = <R2>;
    }
    $sequences{$seq}++;
}

print STDERR "Printing sequences...\n" if ($args{verbose});
if ($args{fasta}) {
    if ($r2) {
	$ofile1 = "$args{prefix}_R1.fasta";
	$ofile2 = "$args{prefix}_R2.fasta";
    } else {
	$ofile1 = "$args{prefix}.fasta";
    }
} else { 
    if ($r2) {
	$ofile1 = "$args{prefix}_R1.fastq";
	$ofile2 = "$args{prefix}_R2.fastq";
    } else {
	$ofile1 = "$args{prefix}.fastq";
    }
}

open OFILE1, ">$ofile1" or die "cannot open $ofile1: $!\n";
open OFILE2, ">$ofile2" or die "cannot open $ofile2: $!\n" if ($r2);

$count = 0;
$total = 0;
$samplename = ($args{samplename}) ? "sample=$args{samplename};" : "";
foreach $seq (keys %sequences) {
    $total++;
    next if ($sequences{$seq} < $args{minreads});
    $count++;
    if ($args{fasta}) {
	if ($r2) {
	    ($seq1, $seq2) = split /:/, $seq;
	    print OFILE1 ">seq$count;size=$sequences{$seq};$samplename\n$seq1\n";   
	    print OFILE2 ">seq$count;size=$sequences{$seq};$samplename\n$seq2\n";   
	} else {
	    print OFILE1 ">seq$count;size=$sequences{$seq};$samplename\n$seq\n";   
	}
    } else {
	if ($r2) {
	    ($seq1, $seq2) = split /:/, $seq;
	    $length = length($seq1);
	    $qual = 'F' x $length;
	    print OFILE1 "\@seq$count;size=$sequences{$seq};$samplename\n$seq1\n+\n$qual\n";   
	    $length = length($seq2);
	    $qual = 'F' x $length;
	    print OFILE2 "\@seq$count;size=$sequences{$seq};$samplename\n$seq2\n+\n$qual\n";   
	} else {
	    $length = length($seq);
	    $qual = 'F' x $length;
	    print OFILE1 "\@seq$count;size=$sequences{$seq};$samplename\n$seq\n+\n$qual\n";   
	}
    }	
}
print STDERR "$count sequences with at least $args{minreads} reads printed\n";
$skipped = $total - $count;
print STDERR "$skipped sequences with less than $args{minreads} reads not printed\n";

exit unless ($args{verbose});

$ofile = "$r1.dedup.counts";
open OFILE, ">$ofile" or die "cannot open output file $ofile: $!\n";
foreach $seq (sort { $sequences{$a} <=> $sequences{$b} } keys %sequences ) {
    $counts{$sequences{$seq}}++;
}
foreach $i (sort {$a <=> $b } keys %counts) {
    print OFILE "$i\t$counts{$i}\n";
}

# plot dupe curve
#$rfile = "dedup.r";
#open ">$rfile", 


################################ Helper subs ###############################

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

sub mean {
    my($data) = @_;
    if (not @$data) {
	die("Empty array\n");
    }
    my $total = 0;
    foreach (@$data) {
	$total += $_;
    }
    my $average = $total / @$data;
    return $average;
}

sub stdev {
    my($data) = @_;
    if(@$data == 1) {
	return 0;
    }
    my $average = &average($data);
    my $sqtotal = 0;
    foreach(@$data) {
	$sqtotal += ($average-$_) ** 2;
    }
    my $std = ($sqtotal / (@$data-1)) ** 0.5;
    return $std;
}
