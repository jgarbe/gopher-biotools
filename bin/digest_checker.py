#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 15 09:05:33 2018

@author: jone3509
@editor: guare001
"""

import multiprocessing as mp
import os, os.path
import argparse, sys
import matplotlib
matplotlib.use('agg')
from matplotlib import pyplot as plt
import numpy as np

import gzip
import re

parser = argparse.ArgumentParser()
parser.add_argument('--fastq', '-f', help="input directory path", type= str, required = True)
parser.add_argument('--rsite', '-r', help="restriction enzyme seqeuence", type= str, required = True)
parser.add_argument('--rsite2', help = "restriction sequence for second enzyme", type = str, default = "none")
parser.add_argument('--maxreads', '-n', help = "max number of reads to be digested", type = int, default = -1)
parser.add_argument('--threads', '-t', help = "number of simultaneous processes", type = int, default = 1)
args = parser.parse_args()
path = args.fastq

def Digest_Checker(input_file, n):
    '''
    Accepts input files received from pool as input and returns
    output metrics to a text file.
    '''

    #decides how to open the file
    if input_file[-2:] != 'gz':
        content = open(input_file, 'rt')
    else:
        content = gzip.open(input_file,'rt')

    data_list = []
    itr = 0

    line = next(content, False)
    while line != False and ((n != -1) and itr <= n * 4):
        if (itr - 1) % 4 == 0:
            data_list.append(line.rstrip())
        line = next(content, False)
        itr += 1

    content.close()

    a = str((input_file.split('/'))[-1])
    sample_name, b = a.split('_R')
    restrict_seq = args.rsite


    #replace ambiguous bases with corresponding nucleotides
    restrict_seq = restrict_seq.replace('R', '[AG]')
    restrict_seq = restrict_seq.replace('Y', '[CT]')
    restrict_seq = restrict_seq.replace('S', '[GC]')
    restrict_seq = restrict_seq.replace('W', '[AT]')
    restrict_seq = restrict_seq.replace('K', '[GT]')
    restrict_seq = restrict_seq.replace('M', '[AC]')
    restrict_seq = restrict_seq.replace('B', '[CGT]')
    restrict_seq = restrict_seq.replace('D', '[AGT]')
    restrict_seq = restrict_seq.replace('H', '[ACT]')
    restrict_seq = restrict_seq.replace('V', '[ACG]')
    restrict_seq = restrict_seq.replace('N', '[ACGT]')

    if args.rsite2 != "none":
        restrict_seq2 = args.rsite2
        restrict_seq2 = restrict_seq2.replace('R', '[AG]')
        restrict_seq2 = restrict_seq2.replace('Y', '[CT]')
        restrict_seq2 = restrict_seq2.replace('S', '[GC]')
        restrict_seq2 = restrict_seq2.replace('W', '[AT]')
        restrict_seq2 = restrict_seq2.replace('K', '[GT]')
        restrict_seq2 = restrict_seq2.replace('M', '[AC]')
        restrict_seq2 = restrict_seq2.replace('B', '[CGT]')
        restrict_seq2 = restrict_seq2.replace('D', '[AGT]')
        restrict_seq2 = restrict_seq2.replace('H', '[ACT]')
        restrict_seq2 = restrict_seq2.replace('V', '[ACG]')
        restrict_seq2 = restrict_seq2.replace('N', '[ACGT]')

    count_list = []
    e1_ct = 0
    e2_ct = 0
    for read in data_list:
        matches = re.findall(restrict_seq,read)
        count = len(matches)
        if args.rsite2 != "none":
            matches2 = re.findall(restrict_seq2, read)
            count += len(matches2)
        if count != 0:
            count_list.append(count)
            e1_ct += len(matches)
            e2_ct += len(matches2)

    read_lengths = []
    for ind,val in enumerate(data_list):
            read_len = len(val)
            read_lengths.append(read_len)

    res = (sample_name + '\t' + str(len(data_list)) + '\t' + str(len(count_list)) + '\t' + str(sum(read_lengths)) + '\t' + str(sum(count_list)))
    if args.rsite2 != "none":
        res += "\t" + str(e1_ct) + "\t" + str(e2_ct)
    with open("Digest_Checker_Output.txt", "a") as myfile:
            myfile.write(res + "\n")
    return res

output = open("Digest_Checker_Output.txt", 'wt')
output.write("File Name\tTotal #Reads\t#Reads w/ Intact RSites\tTotal #Bases\tTotal #Intact RSites")
if args.rsite2 != "none":
    output.write("\tIntact E1 Sites\tIntact E2 Sites")
output.write("\n")

# this is where you can adjust the number of parallel processes:
pool = mp.Pool(args.threads)

# this is where the script gets the directory from the command-line:
if os.path.isdir(path): #fastq file or folder
    dataset = []
    for file in os.listdir(path):
        current_file = os.path.join(path, file)
        dataset.append(current_file)
    results = [pool.apply_async(Digest_Checker, args = (fpath, args.maxreads)) for fpath in dataset]
    pool.close()
    pool.join()
    op = [r.get() for r in results]
else:
    op = [Digest_Checker(path, args.maxreads)]

for thing in op:
    output.write(thing + "\n")

output.close()

def per_Mb_graph(data):
    rate_list = []
    label = []
    for line in data:
        rate = (int(line[4]) / int(line[3])) * 1000000
        rate_list.append(rate)
        label.append(line[0])

    index = np.arange(len(label))
    plt.bar(index, rate_list)
    plt.xlabel('Sample Name', fontsize=8)
    plt.ylabel('Restriction Sites / Mb', fontsize=8)
    plt.xticks(index, label, fontsize=6, rotation=-30, ha = 'left')
    plt.title('Intact Restriction Enzyme Site Rates')
    plt.savefig('Encounter_Rates_Mb.png')

def per_Mb_graph_2(data):
    N = len(data)
    e1_matches = []
    for row in data:
        e1_matches.append((int(row[5]) / int(row[3])) * 1000000)
    e2_matches = []
    for row in data:
        e2_matches.append((int(row[6]) / int(row[3])) * 1000000)
    samples = []
    for row in data:
        samples.append(row[0])

    fig, ax = plt.subplots()

    ind = np.arange(N)    # the x locations for the groups
    width = 0.35         # the width of the bars
    p1 = ax.bar(ind, e1_matches, width, color='r', bottom=0)
    p2 = ax.bar(ind + width, e2_matches, width, color='b', bottom=0)

    ax.set_title('Intact Restriction Site Rates by Sample and Enzyme')
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(samples, fontsize=6, rotation=-30, ha = 'left')
    ax.set_ylabel('Restriction Sites / Mb')
    ax.legend((p1[0], p2[0]), ('Enzyme1', 'Enzyme2'))
    fig.tight_layout()
    plt.savefig('Encounter_Rates_Mb.png')

graph_data = np.genfromtxt("Digest_Checker_Output.txt", skip_header = 1, dtype = 'str')

if len(graph_data[0]) == 5:
    per_Mb_graph(graph_data)
if len(graph_data[0]) == 7:
    per_Mb_graph_2(graph_data)
