#!/usr/bin/perl -w

#######################################################################
#  Copyright 2013 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

profile.pl - profile the cpu and memory usage of the computer

=head1 SYNOPSIS

profile.pl [-s seconds] [-h] [-i] [-b bins] [-o logfile]

=head1 DESCRIPTION

This script collects total memory and cpu usage information for the computer/node it is running on, and when the script is killed it prints ASCII plots to standard output summarizing memory and cpu usage across time. After the plots is a list showing the most active process in each bin in the plots. 

Options:
    -s seconds : The number of seconds between polling cpu and memory usage
    -b bins : The number of bins in the summary histograms
    -i : Interactive mode: print update to screen after every poll
    -h : Display usage information
    -o file : Print output to file instead of STDOUT

=head1 EXAMPLE

Start profile.pl at the beginning of your pbs script (after loading the riss_util module) and put it in the background using "&". Check the standard output file (jobname.oXXXXX) for the results::

    $ profile.pl &

=cut

use POSIX;
use Getopt::Std;
use Pod::Usage;

$usage = "Usage: profile.pl [-s seconds] [-h] [-i] [-b bins] [-o logfile]\n";

# handle parameters
getopts('s:b:iho:', \%opts) or die "$usage\n";
pod2usage(q(-verbose) => 3) if ($opts{'h'});
$seconds = $opts{'s'} || 10; # delay between measurements
$bins = $opts{'b'} || 60; # number of histogram bins
$interactive = defined($opts{'i'});
$ofile = $opts{'o'} // "-";
if (defined($opts{'h'})) {
    die "$usage\n";
}

open OFILE, ">$ofile" or die "cannot open log file $ofile: $!\n";

print OFILE "Starting profiler...\n";
print OFILE "Interactive mode\n" if ($interactive);
$SIG{'INT'} = 'INT_handler'; # catch interrupt signal
$SIG{'TERM'} = 'INT_handler'; # catch interrupt signal
$SIG{'KILL'} = 'INT_handler'; # catch interrupt signal

$starttime = time();
$host = `hostname`;
chomp $host;
print OFILE "Running on computer $host\n";
$num_cpu = `cat /proc/cpuinfo | grep processor | wc -l`;
chomp $num_cpu;
print OFILE "$num_cpu CPUs available\n";

$mem_info = `head -n 1 /proc/meminfo | head -n 1`;
my ($junk, $mem_info) = split /\s+/, $mem_info;
chomp $mem_info;
$mem_info = $mem_info / 1024; # turn into mb
$mem_info = int($mem_info / 1024 * 10 + .5) / 10; # turn into gb
print OFILE "$mem_info GB total usable physical memory\n";

#$num_users = `top -b -n 1 | tail -n+8 | awk '{print \$2}' | sort | uniq | wc -l`;
#chomp $num_users;
#print "$num_users unique users on the system at startup (including you and root)\n";

# get starting cpu usage stats
$stat = `head -n 1 /proc/stat`;
($junk, $olduser, $oldnice, $oldsystem, $oldidle, $oldwait, $oldirq, $oldsoftirq) = split /\s+/, $stat;
$oldtotal = $olduser + $oldnice + $oldsystem + $oldidle + $oldwait + $oldirq + $oldsoftirq;

sleep $seconds;

while (1) { # go until an interupt signal is received
    # parse load average
    $load = `cat /proc/loadavg`;
    ($load, $junk) = split /\s+/, $load;
    $load = int($load / $num_cpu * 1000) / 10;
    push @load, $load;

    # parse cpu time
    $stat = `head -n 1 /proc/stat`;
    ($junk, $user, $nice, $system, $idle, $wait, $irq, $softirq) = split /\s+/, $stat;
    $total = $user + $nice + $system + $idle + $wait + $irq + $softirq;
    $difftotal = $total - $oldtotal;
    $diffuser = int(($user - $olduser) / $difftotal * 1000) / 10;
    $diffsystem = int(($system - $oldsystem) / $difftotal * 1000) / 10;
    $diffwait = int(($wait - $oldwait) / $difftotal * 1000) / 10;
    push @user, $diffuser; # / $num_cpu;
    push @system, $diffsystem; # / $num_cpu;
    push @wait, $diffwait; # / $num_cpu;
    $oldtotal = $total;
    $olduser = $user;
    $oldsystem = $system;
    $oldwait = $wait;

    # parse memory
    @mem = `free`;
    ($junk, $mtotal) = split /\s+/, $mem[1];
    ($junk, $junk, $junk, $mfree) = split /\s+/, $mem[2];

    $mpercent = int(($mtotal - $mfree) / $mtotal * 1000) / 10;
    push @mpercent, $mpercent;

    $string = sprintf("load: %4.1f%%  user: %4.1f%%  system: %4.1f%%  wait: %4.1f%%  memused: %4.1f%% %5.1fGB\n", $load[$#load], $user[$#user], $system[$#system], $wait[$#wait], $mpercent[$#mpercent], $mpercent[$#mpercent] / 100 * $mem_info) if ($interactive);
    print OFILE $string if ($interactive);

    # most active process (highest cpu usage)
#    $process = `ps aux | tr -s ' ' | cut -f3,11 -d' ' | sort -n | tail -n 1`;
    $process = `ps -A -o args --sort -%cpu | head -n 2 | tail -n 1`;
    chomp $process;
#    ($junk, $process) = split /\s+/, $process;
    push @processes, $process;
    
    # sleep
    sleep $seconds;
}

# function to run when an interrupt signal has been received
sub INT_handler {

    print OFILE "\n----- Profile Summary ------\n";

    &memstats;
    &cpustats;
    &processes;

    print OFILE "\n";
    $date = `date`;
    print OFILE "Completed at: $date";
    $endtime = time();
    $time = $endtime - $starttime;
    print OFILE "Run time: ";
    if ($time < 60) {
	print OFILE "$time seconds";
    } elsif ($time < 7200) { # 2 hours
	my $minutes = &round10($time / 60);
	print OFILE "$minutes minutes";
    } else {
	$hours = &round10($time / 3600);
	print OFILE "$hours hours";
    }
    print OFILE "\n";

#    print "$num_users unique users on the system at startup (including you and root)\n";
#    $num_users = `top -b -n 1 | tail -n+8 | awk '{print \$2}' | sort | uniq | wc -l`;
#    chomp $num_users;
#    print "$num_users unique users on the system at finish (including you and root)\n";
    print OFILE "\n----- End of Profile Summary ------\n";
    
    exit;
}

# print the median and max values for an array
# stats($array_ref, "text")
sub stats {
    @sorted = sort {$a<=>$b} @{ $_[0] };
    $mid = int(($#sorted+1) / 2);
    print OFILE "$_[1]: Median: $sorted[$mid] Max: $sorted[$#sorted]\n";
}

sub statsGB {
    @sorted = sort {$a<=>$b} @{ $_[0] };
    $mid = int(($#sorted+1) / 2);
    $median = &round10($sorted[$mid] / 100 * $mem_info);
    $max = &round10($sorted[$#sorted] / 100 * $mem_info);
    print OFILE "$_[1]: Median: $median Max: $max\n";
}

# print a histogram of cpu usage
sub cpustats {

    # generate the histogram
    @hist = ();
    $records = $#user + 1;
    if ($records <= $bins) {
	$bins = $records;
    }
    $perbin = ceil(($records) / ($bins));

    $usum = 0;
    $ssum = 0;
    $wsum = 0;
    $row = 0;
    $count = 0;
    for $record (0..$#user) {
	$count++;
	$usum += $user[$record];
	$ssum += $system[$record];
	$wsum += $wait[$record];
	if (($record == $#user) or ($count == $perbin)) {
	    $uave = $usum / $count;
	    $save = $ssum / $count;
	    $wave = $wsum / $count;
	    $upips = int ($uave / 5 + .5);
	    $spips = int ($save / 5 + .5);
	    $wpips = int ($wave / 5 + .5);
	    &pips($upips, "u", $row);
	    &pips($spips, "s", $row);
	    &pips($wpips, "w", $row);
	    $idle = 20 - ($upips + $spips + $wpips);
	    &pips($idle, " ", $row);	    
	    $row++;
	    $count = 0;
	    $usum = 0;
	    $ssum = 0;
	    $wsum = 0;
	}
    }
    
    # print out the histogram
    for $i (0..19) {
	for $j (0..($row-1)) {
	    print OFILE $hist[$j][19-$i];
	}
	$percent = 5 * (20 - $i);
	print OFILE " $percent%\n";
    }
    for $i (0..$row) {
	print OFILE "-";
    }
    print OFILE "0%\n";
    for $i (0..$row) {
	if (($i+1) % 10 == 0) {
	    print OFILE "|";
	} else {
	    print OFILE " ";
	}
    }
    print OFILE "\n";
    
    $binlength = $perbin * $seconds;
    $unit = "seconds";
    if ($binlength > 120) {
	$unit = "minutes";
	$binlength = int($binlength / 60 + .5);
    }

    print OFILE "CPU usage: u = user, s = system, w = waiting ($binlength $unit per bin)\n";
    print OFILE "$num_cpu CPUs available\n";
    &stats(\@load, "Load %");
    print OFILE "\n";
    
}

# print a histogram of memory usage
sub memstats {

    # generate the histogram
    @hist = ();
#    $bins = 20;
    $records = $#mpercent + 1;
    if ($records <= $bins) {
	$bins = $records;
	$bins = 1 if ($bins <= 0);
    }
    $perbin = ceil(($records) / ($bins));

    $msum = 0;
    $row = 0;
    $count = 0;
    for $record (0..$#mpercent) {
	$count++;
	$msum += $mpercent[$record];
	if (($record == $#mpercent) or ($count == $perbin)) {
	    $mave = $msum / $count;
	    $mpips = int($mave / 5 + .5);
	    &pips($mpips, "m", $row);
	    $idle = 20 - $mpips;
	    &pips($idle, " ", $row);	    
	    $row++;
	    $count = 0;
	    $msum = 0;
	}
    }
    
    # print out the histogram
    for $i (0..19) {
	for $j (0..($row-1)) {
	    print OFILE $hist[$j][19-$i];
	}
	$percent = 5 * (20 - $i);
	print OFILE " $percent%\n";
    }
    for $i (0..$row) {
	print OFILE "-";
    }
    print OFILE "0%\n";
    for $i (0..$row) {
	if (($i+1) % 10 == 0) {
	    print OFILE "|";
	} else {
	    print OFILE " ";
	}
    }
    print OFILE "\n";
    
    $binlength = $perbin * $seconds;
    $unit = "seconds";
    if ($binlength > 120) {
	$unit = "minutes";
	$binlength = int($binlength / 60 + .5);
    }

    print OFILE "Memory usage: m = used ($binlength $unit per bin)\n";
    print OFILE "$mem_info GB total usable physical memory\n";
    &stats(\@mpercent, "Memory Usage %");
    &statsGB(\@mpercent, "Memory Usage GB");
    print OFILE "\n";

}

sub pips {
    $pips = shift @_;
    $char = shift @_;
    $row = shift @_;
    foreach (1..$pips) {
	push @{$hist[$row]}, $char;
    }
}

# print out list of most active process in each bin
sub processes {
    print OFILE "Most active process in each histogram bin\n";
    print OFILE "Bin\tprocess\n";

    $records = $#processes + 1;
    if ($records <= $bins) {
	$bins = $records;
    }
    $perbin = ceil(($records) / ($bins));

    $count = 0;
    $bin = 0;
    for $record (0..$#processes) {
	$count++;
	$proccount{$processes[$record]}++;
	if (($record == $#processes) or ($count == $perbin)) {
	    $bin++;
	    $highcount = 0;
	    $highproc = "";
	    # figure out the top processes
	    foreach $process (keys %proccount) {
		if ($proccount{$process} > $highcount) {
		    $highproc = $process;
		    $highcount = $proccount{$process};
		}
	    }
	    foreach $process (keys %proccount) {
		$proccount{$process} = 0;
	    }
	    if (($bin) % 10 == 0) {
		print OFILE "-";
	    } else {
		print OFILE " ";
	    }
	    print OFILE "$bin\t$highproc\n";

	    $count = 0;
	}
    }

}

# this truncates instead of rounds, fix it
sub round10 {
    return int($_[0] * 10) / 10;
}
