#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

profiles.pl - Run profile.pl on all nodes allocated to a job

=head1 SYNOPSIS

profiles.pl [-s seconds] [-h] [-i] [-b bins]

=head1 DESCRIPTION

Generates memory and cpu usage information for multiple nodes. One nodeXXXX.log file is created in your current working directory for each node allocated to the current job.

Options:
    -s seconds : The number of seconds between polling cpu and memory usage
    -b bins : The number of bins in the summary histograms
    -h : Display usage information

=head1 EXAMPLE

Start profiles.pl at the beginning of your pbs script (after loading the riss_util module) and put it in the background using "&". ::

    $ profiles.pl &
    
=cut

use POSIX;
use Getopt::Std;
use FindBin qw($Bin);
use Pod::Usage;

$usage = "Usage: profiles.pl [-s seconds] [-h] [-b bins]\n";

$SIG{'INT'} = 'INT_handler'; # catch interrupt signal
$SIG{'TERM'} = 'INT_handler'; # catch interrupt signal
$SIG{'KILL'} = 'INT_handler'; # catch interrupt signal

# handle parameters
getopts('s:b:h', \%opts) or die "$usage\n";
pod2usage(q(-verbose) => 3) if ($opts{'h'});
$seconds = $opts{'s'} || 10; # delay between measurements
$bins = $opts{'b'} || 60; # number of histogram bins
#$interactive = defined($opts{'i'});
if (defined($opts{'h'})) {
    die "$usage\n";
}

print "Starting parallel profiler...\n";

# Get the node list, launch profile.pl on each node
$ifile = $ENV{PBS_NODEFILE};
open IFILE, $ifile or die "Cannot open nodefile $ifile: $!\n";
@nodefile = <IFILE>;
close IFILE;
foreach $node (@nodefile) {
    chomp $node;
    next if ($node eq "");
    next if $seen{$node};
    $seen{$node}++;
    print "Launching profile.pl on node $node\n";
#    system "ssh $node \"screen -d -m $Bin/profile.pl -s $seconds -b $bins -o $ENV{PWD}/$node.log\" &";
    system "ssh $node \"nohup $Bin/profile.pl -s $seconds -b $bins -o $ENV{PWD}/$node.log\" &";
}

while (1) { # waiting to die...
    sleep 60;
}

# function to run when an interrupt signal has been received
sub INT_handler {

    # kill the profiler running on each node
    foreach $node (keys %seen) {
	`\\ssh $node killall profile.pl`;
    }
    exit;
}
