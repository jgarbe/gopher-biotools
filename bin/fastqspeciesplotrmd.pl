#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastqspeciesplotrmd.pl - Generate plots from fastq-species-bowtie2 output files

=head1 SYNOPSIS

fastqspeciesplotrmd.pl -f filelist

=head1 DESCRIPTION

Generate a plot summarizing stats from multiple fastq-species-bowtie2.pl output files

Options:
    -f file : file with list of samples and sample output files
    -t type : Type of screen: contamination or species
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;
use File::Basename;

our ($opt_h, $opt_f, $opt_t);

$usage = "USAGE: fastqspeciesplotrmd.pl -f filelist\n";

die $usage unless getopts('hf:t:');
pod2usage(q(-verbose) => 3) if ($opt_h);
pod2usage unless ($opt_f);
$type = $opt_t || "species";

# Get list of output files
open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
while (<IFILE>) {
    chomp;
    next unless length;
    @line = split /\t/;
    if (not -e $line[0]) { # skip files that don't exist
	print "File $line[0] not found, skipping\n";
	next;
    }
    push @files, $line[0];
    push @samples, $line[1] // $line[0];
} 

for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    open IFILE, $ifile or print "Cannot open file $ifile: $!\n";
    while ($line = <IFILE>) {
	last if ($line =~ /Hits\tPercent\tSpecies/);
    }
#    $species{"NoHit"} = 1;
    while ($line = <IFILE>) {
	chomp $line;
	last if ($line eq ""); # quit at blank line
	my ($reads, $percent, $species, $speciesname) = split ' ', $line;
#	next if ($species eq "NoHit");
	$data{$sample}{$species} = $reads;
	$data{$sample}{"pct-$species"} = $percent;
	$total{$sample}+= $percent;
	$species{$species} = 1;
    }
}
close IFILE;

### Print out summary stats ###
print "STATS Start\n";
foreach $sample (keys %data) {
    foreach $metric (keys %{$data{$sample}}) {
	next unless ($metric =~ /^pct/); # only report percent stats
	print "$sample\t$metric\t$data{$sample}{$metric}\n";
    }
}
print "STATS End\n";

### Print out samples ordered by alignment
@order = sort {$total{$b} <=> $total{$a}} keys %total;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tSequence content\t$order\n";

# generate reads per sample plot
$name = "fastq${type}plot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
#print "Generating fastq plot\n" if ($verbose);
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# print out the data
delete $species{NoHit}; # crude way to omit NoHit from the species plot
@species = sort keys %species;
print OFILE "sample";
foreach $species (@species) {
    print OFILE "\t$species";
}
print OFILE "\ttotal\n";
foreach $sample (sort keys %data) {
    print OFILE "$sample";
    foreach $species (@species) {
	print OFILE "\t$data{$sample}{\"pct-$species\"}";
    }
    print OFILE "\t$total{$sample}\n";
}
close OFILE;


# plot the data
$traces = "";
for $species (1..$#species) {
    $traces .= qq(%>% add_trace(y=~$species[$species],  name = "$species[$species]", text = paste0(datat\$sample,"<br>$species[$species] ",round(datat\$$species[$species],2),"%")) );
}

if ($type eq "species") {
    $title = "Sequence Content";
} else {
    $title = "Sequence Content";
}
$text = "The first 10,000 R1 reads from each sample are aligned to a selection of common reference genomes and sequence contaminants, and the number of reads aligning to each reference is shown. Reads aligning to multiple references are assigned to the reference with the most alignments. This plot provides a simple method to verify the sequence content of each library.";

$updatemenues = "updatemenus = updatemenus,";
$updatemenues = ""; #disable sort buttons

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10), 
               categoryarray = sort(c(as.character(datat\$sample))))

data2 <- datat[rev(order(datat\$total)),]
valueorder <- list(title="Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
              categoryarray = c(as.character(data2\$sample)))

updatemenus <- list(
  list(
    active = 0,
    font = list(size = 10),
    type = 'buttons',
    xanchor = 'right',
    buttons = list(
      
      list(
        label = "Sort by<br>Sample",
        method = "relayout",
        args = list(list(xaxis = sampleorder))),
      
      list(
        label = "Sort by<br>Value",
        method = "relayout",
        args = list(list(xaxis = valueorder)))
    )
  )
)

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$species[0],
 name = "$species[0]",
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>$species[0] ",round(datat\$$species[0],2),"%") ) $traces
%>% layout(xaxis = sampleorder, $updatemenues dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;

# TODO: generate plot if not running in rmd mode

########## subs ###############

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
}
