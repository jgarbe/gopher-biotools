#!/usr/bin/perl -w

###############################################################
# fasta2tab.pl
# John Garbe
# July 2008
#
##############################################################

=head1 NAME

fasta2tab.pl - Convert a fasta file to a tab-delimited text file

=head1 SYNOPSIS

fasta2tab.pl file.fasta > file.txt

=head1 DESCRIPTION

Convert a fasta file to a tab-delimited flatfile with two columns: sequence ID and sequence

Options:
    -h : Display usage information

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: fasta2tab.pl file.fasta > file.txt\n";

#die $usage unless ($#ARGV >= 0);
die $usage unless getopts('h');
pod2usage(q(-verbose) => 3) if ($opt_h);

#$ifile = $ARGV[0];
#open "IFILE", "<$ifile" || die("cannot open $ifile\n");

#$line = <IFILE>;
#$line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
#$chomp $line;

@line = ("junkname");
$seq = "junkseq";
$id = "junk";
$first = 1;

while ($line = <>) {
 $line =~ s/\r\n?/\n/g; # get rid of stupid windows newlines
 chomp $line;

 if ($line =~ "^>") {
  print "$id\t$seq\n" unless ($first);
  $first = 0;
#  @line = split /\|/, $line;
#  ($id) = split / /, $line[4];
#   $id = $line[0];
#  ($junk, $id) = split />/, $id;
  $line =~ tr/\t/_/; # convert tabs to _
  $id = $line;
  $seq = "";

 } else {
  $seq = "$seq$line";
 }

}
print "$id\t$seq\n" unless ($first);

exit 0;
