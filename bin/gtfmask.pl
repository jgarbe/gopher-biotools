#!/usr/bin/perl -w

#################################
# gtfmask.pl
# John Garbe
# May 2015
#
##################################

$usage = "USAGE: gtfmask.pl genome.fa annotation.gtf cutoff > masked.gtf\n";
die $usage unless ($#ARGV == 2);

($genomefile, $gtffile, $cutoff) = @ARGV;

$verbose = 1;

# check that gffread is available
$result = `which gffread 2>&1`;
print STDERR "Generating cDNA sequences...\n" if ($verbose);
# Generate a cDNA file using gffread
$transcriptfile = "transcripts.fa";
die "Cannot find gffread (try loading the cufflinsk module)\n" if ($result =~ "no gffread in");
`gffread -w $transcriptfile -g $genomefile $gtffile`;

# calculate cDNA lengths
print STDERR "Calculating cDNA lengths...\n" if ($verbose);
$lengthfile = "lengths.txt";
`/panfs/roc/itascasoft/rnaseqQC/1.1/bin/fastalength.pl $transcriptfile > $lengthfile`;

# read in lengths
my $temp;
open IFILE, $lengthfile or die "Cannot open length file $lengthfile: $!\n";
while ($line = <IFILE>) {
    chomp $line;
    # my original method
#    ($transcript, $gene, $length, $length2) = split ' ', $line;
#    $length = $length2 if ($length =~ /CDS=/);
    # Christy's method that works with UCSC gtf files
    ($fullname, $length) = split '\t', $line;
    ($transcript, $gene, $temp) = split ' ', $fullname;
    $gene = $transcript if !defined($gene);
    $gene = $transcript if ($gene =~ /CDS=/);

    $gene =~ s/gene=//;
#    print "transcript: $transcript gene: $gene length: $length\n";
#    exit;
    $lengths{$gene}{min} = 1000000 unless (defined($lengths{$gene}{min}));
    $lengths{$gene}{max} = 0 unless (defined($lengths{$gene}{max}));
    $lengths{$gene}{min} = $length if ($length < $lengths{$gene}{min});
    $lengths{$gene}{max} = $length if ($length > $lengths{$gene}{max});
    $lengths{$gene}{$transcript} = $length;
}
close IFILE;

# read in gtf file, spit out short genes
print STDERR "Generating masked gtffile...\n" if ($verbose);
open IFILE, $gtffile or die "Cannot open gtf file $gtffile: $!\n";
while ($line = <IFILE>) {
    next if ($line =~ /^#/);
    chomp $line;
    next if ($line =~ /^\s*$/);
    $line =~ /gene_id "([^"]+)";/;
    $gene_id = $1;
    if ($line =~ /gene_name "([^"]+)";/) {
	$gene_id = $1;
    }
    $line =~ /transcript_id "([^"]+)";/;
    my $transcript_id = $1;
    if (!defined($lengths{$gene_id})) {
	print STDERR "No length for gene $gene_id\n";
	next;
    }
    print $line unless ($lengths{$gene_id}{max} > $cutoff);
}
