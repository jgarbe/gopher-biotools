#!/usr/bin/perl -w

################################################################

=head1 NAME

pcaplot.pl - Create a pca plot from a table of data

=head1 SYNOPSIS

pcaplot.pl --datafile data.txt [--samplesheet file.txt] [--name name]

=head1 DESCRIPTION

Generate a 3d pca plot from a table of data

Options:
    --datafile file : data table file
    --samplesheet file : samplesheet file (ideally with metadata)
    --name string : Name for plot files
    --help : Display usage information
    --text string : Text describing the plot to include in the help section
    --verbose : Verbose output

=cut

##################### Initialize ###############################

use Getopt::Long;
use Pod::Usage;

$args{name} = "pcaplot";
GetOptions("help" => \$args{help},
           "verbose" => \$args{verbose},
           "datafile=s" => \$args{datafile},
           "samplesheet=s" => \$args{samplesheet},
           "name=s" => \$args{name},
           "text=s" => \$args{text},
    ) or pod2usage;
pod2usage(-verbose => 99, -sections => [qw(DESCRIPTION|SYNOPSIS|OPTIONS)]) if ($args{help});
if ($#ARGV >= 0) {
    print "Unknown commandline parameters: @ARGV\n";
    pod2usage;
}
pod2usage unless ($args{datafile});

$name = $args{name};
$ofile = "$name.dat";
$rfile = "$name.rmd";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

# read in samplesheet
%metacats = ();
if ($args{samplesheet}) {
    open IFILE, $args{samplesheet} or die "Cannot open samplesheet $args{samplesheet}: $!\n";
    $header = <IFILE>;
    chomp $header;
    @header = split /\t/, lc($header);
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$sample = $line[0];
	push @samples, $sample;
	for $i (1..$#line) {
	    next if ($header[$i] eq "fastqr1"); 
	    next if ($header[$i] eq "fastqr2"); 
	    next if ($header[$i] eq "description"); 
	    $value = $line[$i] // "undef";
	    $value = "undef" if ($value eq "");
	    $metadata{$sample}{$header[$i]} = $value;
	    $metacats{$header[$i]}{$value} = 1;
	}
    }
    close IFILE;
}

### read in data
open IFILE, $args{datafile} or die "cannot open $args{datafile}: $!\n";

$header = <IFILE>;
while ($line = <IFILE>) {
    chomp $line;
    next if ($line eq "");
    last if ($line =~ /^"eigvals"/);
    @line = split /\t/, $line;
    $sample = $line[0];
    $sample =~ s/"//g;
    $pca{$sample} = [ $line[1], $line[2], $line[3] ];
}

$line = <IFILE>; # % variation explained
chomp $line;
@explained = split /\t/, $line;
for $i (1..$#explained) {
    $explained[$i] = round100($explained[$i]);
    $explained[$i] = "$explained[$i]%";
}
close IFILE;

### write out data
print OFILE "sample\tpca1\tpca2\tpca3";
foreach $key (sort keys %metacats) {
    print OFILE "\t$key";
}
print OFILE "\n";
foreach $sample (keys %pca) {
    print OFILE "$sample\t$pca{$sample}[0]\t$pca{$sample}[1]\t$pca{$sample}[2]";
    foreach $key (sort keys %metacats) {
	print OFILE "\t$metadata{$sample}{$key}";
    }
    print OFILE "\n";    
}
close OFILE;

# generate metadata color lists
@colors = ('#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'); # standard plotly color set
foreach $cat (sort keys %metacats) {
    @levels = sort keys %{$metacats{$cat}};
    for $i (0..$#levels) {
	$imod = $i % 10;
	$colors{$cat}{$levels[$i]} = $colors[$imod];
    }
    $colorarray{$cat} = "";
    foreach $sample (@samples) {
	$colorarray{$cat} .= "\"$colors{$cat}{$metadata{$sample}{$cat}}\",";
    }
    chop $colorarray{$cat};
}

if ($#samples > 20) {
    $type = "marker";
    $typename = "markers";
} else {
    $type = "textfont";
    $typename = "text";
}

$buttonlist = "";
$firstcolorarray = "";
if (keys %metacats) {
    foreach $cat (sort keys %metacats) {
	$firstcolorarray = $colorarray{$cat} unless ($firstcolorarray);
	$buttonlist .= "list(label = \"Color by:<br>$cat\", method = \"restyle\", args = list(\"$type.color\", list($colorarray{$cat}))),";
    }
    # add "none"
    $colorarray = "";
    foreach $sample (@samples) {
	$colorarray .= "\"#1f77b4\","; # blue
    }
    chop $colorarray;
    $buttonlist .= "list(label = \"Color by:<br>None\", method = \"restyle\", args = list(\"$type.color\", list($colorarray)))";
}
$firstcolorarray = $colorarray unless ($firstcolorarray);

$text = $args{text} // "This is a Principal Components (PCA) plot. The first three principal components are shown, and the percent of total variation explained by each component is shown in the axis titles. Samples with similar characteristics appear close to each other, and samples with dissimilar characteristics are farther apart. Ideally samples will cluster by experimental condition and not by batch or other technical effects.";

### generate pca plot
open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">PCA plot
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name, out.height=500, warning=FALSE}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c(\"sample\"=\"factor\"));

updatemenus <- list(
  list(
    x = 1,
    y = 1,
    xanchor = 'right',
    yanchor = 'top',
    font = list(size = 10),
    buttons = list(
      $buttonlist
    )
  )
)

config(plot_ly(
  height = 500,
  data=datat,
  x = ~pca1,
  y = ~pca2,
  z = ~pca3,
 type = "scatter3d",
 mode = '$typename', 
 color = ~sample,
$type = list(color = c($firstcolorarray)),
hoverinfo='text', text=paste0(datat\$sample))
%>% layout(updatemenus=updatemenus, scene = list(xaxis = list(title = "Axis 1 ($explained[1])", showspikes=FALSE), yaxis = list(title = "Axis 2 ($explained[2])", showspikes=FALSE), zaxis=list(title="Axis 3 ($explained[3])", showspikes=FALSE)), showlegend = FALSE), dragmode='tableRotation', collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosest3d','hoverCompareCartesian','lasso2d','zoom3d','select3d','toggleSpikelines','pan3d', 'resetCameraLastSave3d', 'orbitRotation', 'tableRotation', 'resetCameraDefault3d'))
```
);
close RFILE;



# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

