#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

subsample.pl - subsample a fastq or fastq.gz file

=head1 SYNOPSIS

subsample.pl input.fastq S [L] > output.fastq

=head1 DESCRIPTION

Subsample S reads from a fastq file with L lines. This script will select every (L/4)/S read from the fastq file, which means reads are evenly selected throughout the fastq file. The same reads will always be selected as long as L and S are the same, so this script can be used to subsample from paired-end datasets, and the same reads will be selected from the R1 and R2 files.

=cut

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "subsample.pl input.fastq 100 > output.fastq\n";
die $usage unless getopts('h');
pod2usage(q(-verbose) => 3) if ($opt_h);
die $usage unless (($#ARGV == 1) or ($#ARGV == 2));
$ifile = $ARGV[0];
$N = $ARGV[1];
$gz = ($ifile =~ /\.gz$/) ? 1 : 0; # check if input is gzip compressed

if ($#ARGV == 2) {
    $lines = $ARGV[2];
} else {
    # count number of lines in file
    my $junk;
    if ($gz) {
	$lines = `gunzip -c $ifile | wc -l`;
    } else {
	$lines = `wc -l < $ifile`;
    }
    chomp $lines;
}
$interval = $lines / 4 / ($N+1);
$pick = $interval;

#print STDERR "lines $lines interval $interval\n";

if ($gz) {
    open IFILE, "gunzip -c $ifile |" or die "Can’t open gzipped input file $ifile: $!\n";
} else {
    open IFILE, "$ifile" or die "Cannot open input file $ifile: $!\n";
}
$printcount = 0;
$readcount = 0;
while ($printcount < $N) {
    $readcount++;
    if ($readcount >= $pick) {
	$line = <IFILE>; # ID
	$line .= <IFILE>; # sequence
	$line .= <IFILE>; # +
	$line .= <IFILE>; # quality
	print $line;
	
	$pick += $interval;
	$printcount++;
    } else {
	$line = <IFILE>; # ID
	$line = <IFILE>; # sequence
	$line = <IFILE>; # +
	$line = <IFILE>; # quality
    }
}
#print STDERR "Final count: $printcount\n";
