#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

rnaseqmetricsplotrmd.pl - Generate a summary plot from Picard rnaseqmetrics output files

=head1 SYNOPSIS

rnaseqmetricsplotrmd.pl align_summary1.txt [align_summary2.txt ...]
rnaseqmetricsplotrmd.pl -f filelist.txt

=head1 DESCRIPTION

Options:
    -f filelist.txt : Provide a file with a list of metrics.txt files, one per line. A second tab-delimited column may be included containing sample names.
    -h : Display usage information

=cut

##################### Initialize ###############################

use Getopt::Std;
use Pod::Usage;

our ($opt_h);

$usage = "USAGE: rnaseqmetricsplotrmd.pl metrics.txt [metrics2.txt ...]\n";

die $usage unless ($#ARGV >= 0);
die $usage unless getopts('hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);

# Get list of alignment files
if (defined($opt_f)) {
    open IFILE, "$opt_f" or die "Cannot open file list $opt_f: $!\n";
    while (<IFILE>) {
	chomp;
	next unless length;
	@line = split /\t/;
	if (not -e $line[0]) { # skip files that don't exist
	    print "File $line[0] not found, skipping\n";
	    next;
	}
	push @files, $line[0];
	push @samples, $line[1] // $line[0];
#	push @bams, $line[2] // undef;
    } 
} else { 
    @files = @ARGV; 
    @samples = @ARGV;
}

# For debugging:
#for $i (0..$#files) { 
#    print "$files[$i] $samples[$i]\n";
#}
#print "@files\n";
#print "@samples\n";

# Count spikein bam alignments, if provided
#$spike = 0;
#foreach $bam (@bams) {
#    if (defined($bam) and -e $bam) {
#	$spike = 1;
#	print "grepping $bam\n";
#	$count = `grep -v "^@" $bam | wc -l`;
#	chomp $count;
#	push @spikecounts, $count;
#    } else {
#	push @spikecounts, 0;
#    }
#}


# run through the metrics files, saving stats
for $i (0..$#files) {
    $ifile = $files[$i];
    open IFILE, "$ifile" or die "Cannot open input file $ifile $!\n";

    # read in data
    %thisdata = ();
    while ($line = <IFILE>) {
	if ($line =~ /## METRICS CLASS/) {
	    if ($line =~ /picard.analysis.RnaSeqMetrics/) {
		$header = <IFILE>;
		chomp $header;
		@header = split /\t/, $header;
		while ($line = <IFILE>) {
		    chomp $line;
		    last if ($line =~ /^## /);
		    next if ($line =~ /^\s*$/); # skip blank lines
		    @line = split /\t/, $line;
		    for $i (0..$#line) {
			$thisdata{$header[$i]} = $line[$i];
		    }
		}
	    }
	}
    }

    ### save data ###
    $data{$ifile}{ribosomal} = $thisdata{"PCT_RIBOSOMAL_BASES"} * 100;
    $data{$ifile}{intergenic} = $thisdata{"PCT_INTERGENIC_BASES"} * 100;
    $data{$ifile}{intronic} = $thisdata{"PCT_INTRONIC_BASES"} * 100;
    $data{$ifile}{coding} = $thisdata{"PCT_CODING_BASES"} * 100;
    $data{$ifile}{utr} = $thisdata{"PCT_UTR_BASES"} * 100;
    $total{$ifile} = $data{$ifile}{coding} + $data{$ifile}{utr};
}

### Print out summary stats ###
print "STATS Start\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print "$sample\tpct_ribosomal\t$data{$ifile}{ribosomal}\n";
    print "$sample\tpct_intergenic\t$data{$ifile}{intergenic}\n";
    print "$sample\tpct_intronic\t$data{$ifile}{intronic}\n";
    print "$sample\tpct_coding\t$data{$ifile}{coding}\n";
    print "$sample\tpct_utr\t$data{$ifile}{utr}\n";
}
print "STATS End\n";

### Print out samples ordered by alignment
@order = sort {$total{$b} <=> $total{$a}} keys %total;
foreach $sample (@order) {
    $order .= "\"$sample\",";
}
chop $order; # remove last ,
print "ORDER\tRNAseq metrics\t$order\n";

### Plotting ###
$name = "rnaseqmetricsplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
print "Generating $name\n";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 
# print out the data
print OFILE "sample";
@metrics = ("coding", "utr", "ribosomal", "intronic", "intergenic");
@metricnames = ("coding", "utr", "ribosomal", "intronic", "intergenic");
foreach $metric (@metrics) {
    print OFILE "\t$metric";
}
print OFILE "\n";
for $i (0..$#files) {
    $ifile = $files[$i];
    $sample = $samples[$i];
    print OFILE "$sample";
    foreach $metric (@metrics) {
	print OFILE "\t$data{$ifile}{$metric}";
    }
    print OFILE "\n";
}
close OFILE;

# plot the data
$traces = "";
for $metric (1..$#metrics) {
    $traces .= qq(%>% add_trace(y=~$metrics[$metric],  name = "$metricnames[$metric]", text = paste0(datat\$sample,"<br>$metricnames[$metric] ",round(datat\$$metrics[$metric],2),"%")) );
}

$title = "RNA-seq Metrics";
$text = "Percent of reads aligning to intergenic, intronic, coding, utr, and ribosomal regions of the genome is shown. These metrics are calculated by Picard rnaseqmetrics";

open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">$title
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
$text
</div>

```{r $name}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c("sample"="factor"));

sampleorder <- list(title = "Sample", automargin = TRUE, categoryorder = "array", tickmode = "linear", tickfont = list(size = 10),
               categoryarray = sort(c(as.character(datat\$sample))))

config(plot_ly(
  data=datat,
  x = ~sample,
  y = ~$metrics[0],
 name = "$metricnames[0]",
 type = "bar",
hoverinfo="text",
text = paste0(datat\$sample,"<br>$metricnames[0] ",round(datat\$$metrics[0],2),"%") ) $traces
%>% layout(xaxis = sampleorder, dragmode='pan', legend = list(orientation = 'h'), barmode='stack', xaxis = list(title = "Sample"), yaxis = list(title = "Percent of reads", range = c(0, 100), fixedrange = TRUE)), collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosestCartesian','hoverCompareCartesian','lasso2d','zoom2d','select2d','toggleSpikelines','pan2d'))
```
);
close RFILE;


# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}
