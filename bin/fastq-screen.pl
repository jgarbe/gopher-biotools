#!/usr/bin/perl -w

#######################################################################
#  Copyright 2016 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

=head1 NAME

fastq-species-bowtie2.pl - Given a fastq file, align some or all of the sequences to all risdb/ensembl species and count how many hits there are to each species.

=head1 SYNOPSIS

fastq-species-bowtie2.pl R1.fastq [ R2.fastq ]

Options:
 -n integer : number of reads from the fastq file to align (default is 10000)
 -t integer : number of bowtie jobs to run at a time (default = 1) (Each bowtie job runs 8 threads)
 -v         : verbose
 -h         : help

=head1 DESCRIPTION

fastq-species-bowtie2.pl can be used to align fastq reads against all ensembl Bowtie2 indexes in /home/umii/public/ensembl in order to determine what species the fastq file contains, and if there are significant amounts of contaminating sequence from other species. The -n option is used to specify how many reads from the input fastq file should be aligned (default is 10,000). The -t option specifies how many processor cores to use (default is 1, this script uses GNU parallel so it can run across multiple nodes). 

=head1 EXAMPLE

fastq-species-bowtie2.pl /home/msistaff/public/garbe/sampledata/RNAseq/Hsap/fastq/heart-1_R1.fastq

    Hits Percent Species
    2058 79.98 Homo_sapiens
    1683 65.41 Pan_troglodytes
    1468 57.05 Pongo_abelii
    1229 47.77 Papio_anubis
    1222 47.49 Macaca_mulatta
    .
    .
    .

=cut

use Getopt::Std;
use Pod::Usage;

$usage = "Usage: fastq-species-bowtie2.pl [-n number_of_sequences_to_align] [-t num_threads] input_R1.fastq [ input_R2.fastq ]\n";

die $usage if ($#ARGV < 0);

# die gracefully: catch interrupt signals
$SIG{'INT'} = sub { &countprint; exit; };
$SIG{'TERM'} = sub { &countprint; exit; };
$SIG{'KILL'} = sub { &countprint; exit; };

# parameters 
getopts('n:t:hv', \%opts) or die "$usage\n";;
pod2usage(q(-verbose) => 3) if ($opts{'h'});
die "ERROR: Fastq file must be listed after command-line options\n$usage" if ($#ARGV > 1);
$R1 = $ARGV[0]; # input fastq file
$R2 = $ARGV[1] // "";
$seqs = $opts{'n'} // 10000; # number of sequences to align
$threads = $opts{'t'} || 1; # number of cores to run on
$verbose = $opts{'v'} // 0;
$commandfile = "tmp.command.txt";
# end parameters

die "Compressed fastq files not supported\n" if ($R1 =~ /\.gz/);

$lines = $seqs * 4; # number of lines to pull out of fastq file
$totalreads = `wc -l < $R1`;
chomp $totalreads;
$totalreads = $totalreads / 4;

if (($totalreads > $seqs) && ($seqs != 0)) {
    print "Creating temp file with $seqs reads\n";
    `head -n $lines $R1 > R1.fsb.tmp`;
    $R1 = "R1.fsb.tmp";
    if ($R2) {
	`head -n $lines $R2 > R2.fsb.tmp`;
	$R2 = "R2.fsb.tmp";
    }
    $totalreads = $seqs;
}	

if ($threads > 1) { 
    open COMMAND, ">$commandfile" or die "Cannot open temp command file $commandfile $!\n";
}

### Get list of reference genomes ###
@species = `find /home/umii/public/ensembl -maxdepth 1 -type d`;
#@species = `find /home/umgc-staff/public/genomes -maxdepth 1 -type d`;
foreach $species (sort @species) {
    chomp $species;
    if (! -e "$species/current/bowtie2/") {
	print STDERR "No Bowtie2 index found in $species\n" if ($verbose);
	next;
    }
    $bowtie2index = "$species/current/bowtie2/genome";
    $species =~ /.*\/(.*)/;
    $name = $1;
    print STDERR "Running Bowtie2 on $name\n";
    $species{$name} = 1;
    $commonfile = "$species/common_name.txt";
    if (-e $commonfile) {
	$commonname = `head -n 1 $commonfile`;
	chomp $commonname;
	$commonnames{$name} = $commonname;
    }

    ### Alig with Bowtie2 ###
    $grepcommand = "grep -v ^\@ | cut -f1 | sort | uniq | wc -l";

    if ($R2) {
	$command = "echo -ne '$name\t'; bowtie2 -p 8 -k 1 --very-fast-local --no-unal -x $bowtie2index -1 $R1 -2 $R2 | $grepcommand";
    } else {
	$command = "echo -ne '$name\t'; bowtie2 -p 8 -k 1 --very-fastq-local --no-unal -x $bowtie2index -U $R1 | $grepcommand";
    } 

    if ($threads == 1) {
	$line = `module load bowtie2; $command`;
	chomp $line;
	($myname, $count) = split /\t/, $line;
	$percent = &round100($count / $totalreads * 100);
	print "ERROR: $myname != $name\n" if ($myname ne $name);
	$data{$myname}{count} = $count;
	$data{$myname}{percent} = $percent;
    } else {
	print COMMAND "$command\n";
    }
}

if ($threads > 1) {
    close COMMAND;
    open GP, "module load parallel; module load bowtie2; cat $commandfile | parallel -j $threads |" or die "Cannot start GNU parallel\n";
    while ($line = <GP>) {
	chomp $line;
#	print "GP: $line\n";
	chomp $line;
	($myname, $count) = split /\t/, $line;
	$percent = &round100($count / $totalreads * 100);
	$data{$myname}{count} = $count;
	$data{$myname}{percent} = $percent;
    }
    close GP;
}

&countprint;

exit 0;

############################### subs ############################

# print out the counts per species
sub countprint {
    print "Hits\tPercent\tSpecies\n";
    foreach $name (sort { $data{$b}{count} <=> $data{$a}{count} } keys %data) {
	print "$data{$name}{count}\t$data{$name}{percent}\t$name";
	print "\t$commonnames{$name}" if ($commonnames{$name});
	print "\n";
    }
}

# Round to neaerest tenth
sub round10 {
    return sprintf("%.1f", $_[0]);
#    return int($_[0] * 10 + 0.5) / 10;
}

# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

### die gracefully ###
sub diegracefully {
# catch interrupt signals
    $SIG{'INT'} = &countprint;
    $SIG{'TERM'} = &countprint;
    $SIG{'KILL'} = &countprint;
}
