#!/usr/bin/env python3

########################################################
# sequel-demultiplex.py
# Lindsay Guare 
# 5/23/18
# Updated 5/31/18#
#
# Streamlines demultiplexing subreads from Sequel machines
#
#######################################################

import subprocess as sub
import numpy as np
import os
import sys
import argparse as ap
import string as st
import re

# replaces integer indices with barcode identifiers
def subBarcodeIDs(prefix, sd, barcodefile):
    fasta = np.genfromtxt(barcodefile, dtype = 'str')
    barcodes = []
    for i in range(len(fasta)):
        if fasta[i][0] == '>':
            barcodes.append(fasta[i][1:])

    # cleans barcode name by replacing whitespace and problem characters with '.'
    for barcode in barcodes:
        for char in st.whitespace:
            barcode.replace(char, ".")
        for char in "#_>":
            barcode.replace(char, ".")

    # grabs all directory files containing the output prefix
    outputstr0 = sub.run("ls -dq *" + prefix + "*", shell = True, stdout = sub.PIPE)
    outputstr = outputstr0.stdout.decode('utf-8')
    outputNames = outputstr.split()

    # removes those directory files not fitting the pattern prefix.number_number.fast(a/q).gz
    p = re.compile(prefix + "." + r'[0-9]+' + "_" + r'[0-9]+' + r'(\.fasta\.gz|\.fastq\.gz|\.bam)')
    it = 0
    while it < len(outputNames):
        m = re.search(p, outputNames[it])
        if m == None:
            outputNames.remove(outputNames[it])
        else:
            it += 1

    # iterates through remaining file names and renames them
    for filename in outputNames:
        renameFastFile(prefix, filename, barcodes, sd)

# parses original filename and generates new one from barcode list
def renameFastFile(prefix, filename, barcodes, sd):
    if filename.endswith('.bam'):
        extension = ".bam"
    elif filename.endswith('fasta.gz'):
        extension = ".fasta.gz"
    elif filename.endswith('fastq.gz'):
        extension = ".fastq.gz"

    IDs = getIDSfromIntFilename(prefix, filename, barcodes)

    # if single-indexed remove files with two different barcodes
    if sd:
        if IDs[0] != IDs[1]:
            sub.run("rm " + filename, shell = True)
        else:
            os.rename(filename, prefix + "." + IDs[0] + extension)
    # dual indexed
    else:
          os.rename(filename, prefix + "." + IDs[0] + "_" + IDs[1] + extension)

# parses the filename for the barcode indices and returns a duplet with the barcode names
def getIDSfromIntFilename(prefix, filename, barcodes):
    prefixLength = len(prefix)
    IDs = filename[prefixLength + 1 : -9]
    underscoreSplit = IDs.find("_");
    ID1 = int(IDs[: underscoreSplit])
    ID2 = int(IDs[underscoreSplit + 1 :])
    return[barcodes[ID1], barcodes[ID2]]

# parses the filename for barcode names on either side of the underscore
def getIDsfromBarcodeFilename(prefix, filename):
    prefixLength = len(prefix)
    IDs = filename[prefixLength + 1 : -9]
    underscoreSplit = IDs.find("_");
    ID1 = IDs[: underscoreSplit]
    ID2 = IDs[underscoreSplit + 1 :]
    return[ID1, ID2]

# writes a file with the count of each barcode combination
def createLogFile(prefix, barcodefile):
    fasta = np.genfromtxt(barcodefile, dtype = 'str')
    barcodes = []
    for i in range(len(fasta)):
        if i % 2 == 0:
            barcodes.append(fasta[i][1:])

    outputstr0 = sub.run("ls -dq *" + prefix + ".*_*.fast*.gz", shell = True, stdout = sub.PIPE)
    outputstr = outputstr0.stdout.decode('utf-8')
    outputNames = outputstr.split()

    counts = np.genfromtxt(args.prefix + ".lima.counts", dtype = str, usecols = [0, 1, 4], skip_header = 1)

    f = open(prefix + ".log", 'w')
    f.write("Barcode\tCount\n")
    for row in counts:
        if row[0] == row[1]:
            f.write(barcodes[int(row[0])] + "\t" + row[2] + "\n")
        else:
            f.write(barcodes[int(row[0])] + "-" + barcodes[int(row[1])] + "\t" + row[2] + "\n")

    f.close()

# tests permissions of specified path, returns true if path exists and is readable to user
def isReadableFile(path):
    if not os.path.exists(path):
        print("Argument Error: File at " + path + "does not exist")
        sys.exit()
    if not os.access(path, os.R_OK):
        print("Argument Error: File at " + path + "is not readable")
        sys.exit()

##################################### MAIN #########################################

# Define program arguments
parser = ap.ArgumentParser(description = 'Demultiplex a sequel bam file, generate bam, fasta and/or fastq files')
parser.add_argument('--subreadbam', action = 'store', dest = 'subreadbam', type = str, required = True)
parser.add_argument('--barcodefasta', action = 'store', dest = 'barcodefasta', type = str, required = True)
parser.add_argument('--prefix', action = 'store', dest = 'prefix', type = str, default = 'sequel-demultiplex', help = 'Prefix for output file names')
parser.add_argument('--threads', action = 'store', dest = 'threads', type = int, default = 0, help = 'Number of threads to use, 0 means autodetection')
parser.add_argument('--windowsize', action = 'store', dest = 'windowsize', type = int, default = 0, help = 'Number of bp to search for barcode, 0 means default of 1.5x barcode length')

singleDual = parser.add_mutually_exclusive_group()
singleDual.add_argument('--single', action = 'store_true', dest = 'sd', default = False, help='Ignore reads with different barcodes')
singleDual.add_argument('--dual', action = 'store_false', dest = 'sd', default = False, help='Dual indexing')

fastaq = parser.add_argument_group()
fastaq.add_argument('--fasta', action = 'store_true', dest = 'fastaOP', default = False)
fastaq.add_argument('--fastq', action = 'store_true', dest = 'fastqOP', default = False)
fastaq.add_argument('--bam', action = 'store_true', dest = 'bamOP', default = False)
fastaq.add_argument('--bams', action = 'store_true', dest = 'bamsOP', default = False)

args = parser.parse_args()

# Check program arguments
if args.prefix.find("_") >= 0:
    print("Argument Error: Prefix cannot contain an underscore")
    sys.exit()
isReadableFile(args.subreadbam)
isReadableFile(args.barcodefasta)
if not (args.fastaOP or args.fastqOP or args.bamOP):
    print("Argument Error: Must use at least one of --fasta, --fastq, or --bam")
    sys.exit()

# Convert DOS barcode file to unix
sub.run("sed 's/\\r\\n/\\n/g' " + args.barcodefasta  + " > " + args.prefix + ".barcode.fasta", shell = True);
args.barcodefasta = args.prefix + ".barcode.fasta";

# Add barcode info to bam file with lima
if args.windowsize > 0:
    windowsize = "-W " + str(args.windowsize)
else:
    windowsize = ""
if args.bamsOP:
    bams = "--split-bam"
else :
    bams = ""
sub.run("module load smrtlink; lima " + bams + " " + windowsize + " --num-threads " + str(args.threads) + " " + args.subreadbam + " " + args.barcodefasta + " " + args.prefix + ".bam", shell = True)

# Generate fasta files
if args.fastaOP:
    sub.run("module load smrtlink; bam2fasta --split-barcodes --output " + args.prefix + " " + args.prefix + ".bam", shell = True)

# Generate fastq files
if args.fastqOP:
    sub.run("module load smrtlink; bam2fastq --split-barcodes --output " + args.prefix + " " + args.prefix + ".bam", shell = True)

# Rename fasta/fastq/bam files 
subBarcodeIDs(args.prefix, args.sd, args.barcodefasta)

# Generate table of reads per barcode
createLogFile(args.prefix, args.barcodefasta)

# Clean up
if not (args.bamOP or args.bamsOP) : 
    sub.run("rm " + args.prefix + ".bam", shell = True)
    sub.run("rm " + args.prefix + ".bam.pbi", shell = True)

#sub.run("rm " + args.prefix + ".lima.report", shell = True)
#sub.run("rm " + args.prefix + ".subreadset.xml", shell = True)
#sub.run("rm " + args.prefix + ".lima.summary", shell = True)
#sub.run("rm " + args.prefix + ".lima.counts", shell = True)

# Done
sys.exit(0);
