#!/usr/bin/perl -w

######################################
# extract_fasta.pl
# John Garbe
# April 2013
#
# Based on extract_fastq.pl
#
# TODO: add command-line option specifying character (delimeter)  
# to split fasta ID line
#
#####################################

=head1 NAME

extract_fasta.pl - Given a list of sequence IDs and a fasta file, pull sequences out of the fasta file.

=head1 SYNOPSIS

extract_fasta.pl [-discard] ids.txt sequence.fasta > out.fasta

-q : quiet - don't print info to STDERR
-h : print help info

=head1 DESCRIPTION

Given a file ids.txt containing a list of sequence IDs (one ID per line) and a fasta file sequence.fasta, print out all sequences from sequence.fasta where the sequence ID matches an ID in ids.txt. If the -discard option is used, all sequences from sequence.fasta where the sequence ID does not match an ID in ids.txt will be printed out. ID matching works no matter if the IDs in ids.txt start with ">". IDs in the fasta file are trimmed after the first white space (tab or space) before comparing to IDs in ids.txt.  

=cut

# process command-line options
use Pod::Usage;
use Getopt::Long;

$usage = "USAGE: extract_fasta.pl [-discard] ids.txt sequence.fasta > out.fasta\n";

GetOptions("discard" => \$discard,
	   "quiet" => \$quiet,
	   "help" => \$help) or die $usage;

pod2usage(q(-verbose) => 3) if ($help);
die $usage unless ($#ARGV == 1);

# read in list of IDs
$idfile = $ARGV[0];
open IDFILE, "$idfile" or die "Cannot open $idfile for reading\n";
$count = `cat $idfile | sort | uniq | wc -l`;
chomp $count;
print STDERR "reading $count unique IDs from $idfile...\n" unless ($quiet);
while ($id = <IDFILE>) {
    chomp $id;
    $id = ">" . $id unless ($id =~ /^>/);
    $ids{$id} = 0;
}
close IDFILE;

# read through fasta file, look for and print out matching IDs
$fastafile = $ARGV[1];
if ($fastafile =~ /\.gz/) {
    open FASTAFILE, "gunzip -c $fastafile |" or die "Cannot open gz pipe of $fastafile for reading\n";
} else {
    open FASTAFILE, "$fastafile" or die "Cannot open $fastafile for reading\n";
}
print STDERR "Reading sequences from $fastafile...\n" unless ($quiet);
$dcount = 0;
$ecount = 0;
$print = 0;
while (<FASTAFILE>) {
    chomp;
    my ($id, $junk) = split; # add the ability to split the ID 
#    $id = $_;
    if ($id =~ m/^>/) { # if this is the sequence id line
	if (!$discard) {
	    if (defined($ids{$id})) {
		$ids{$id}++;
		$ecount++;
		print STDERR "\rExtracted $ecount reads" if (($ecount % 1000 == 0) and (!$quiet));
		print "$_\n";
		$print = 1;
	    } else {
		$dcount++;
		$print = 0;
	    }
	}
	if ($discard) {
	    if (defined($ids{$id})) {
		$dcount++;
		$ids{$id}++;
		print STDERR "\rDiscarded $ecount reads" if (($dcount % 1000 == 0) and (!$quiet));
		$print = 0;
	    } else {
		$ecount++;
		print "$_\n";
		$print = 1;
	    }
	}	    

    } else {
	print "$_\n" if ($print);
    }
}

# print out IDs without match
if (!$quiet) {
    $first = 1;
    while (($id, $count) = each(%ids)) {
	if ($count == 0) {
	    if ($first == 1) {
		print STDERR "IDs not found:\n";
		$first = 0;
	    }
	    print STDERR "$id\n";
	}
    }
    
    print STDERR "\rExtracted $ecount sequences\n";
    print STDERR "\rDiscarded $dcount sequences\n";
}
