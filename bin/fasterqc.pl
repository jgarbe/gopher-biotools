#!/usr/bin/perl -w

#######################################################################
#  Copyright 2014 John Garbe
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#######################################################################

# TODO: generate an over-represented sequence bar plot with 
# total % overrepresented sequences per sample
# Create multiple images for very large sample sizes (> 50)

=head1 NAME

fasterqc.pl - Combine FastQC output images

=head1 SYNOPSIS

fasterqc.pl [-s 100] [-o fasterqc.png] [-f folder]

=head1 DESCRIPTION

This script combines FastQC output images into one large png image to make it easy to quickly assess the FastQC output from many samples. When FastQC is run it generates a zip file named SAMPLENAME_fastqc.zip. Run this script in a folder containing one or more of these SAMPLENAME_fastqc.zip files and it will generate a single image containing all of the FastQC images from all samples. It also prints out the "overrepresented sequences" for each sample to the file fasterqc.overrep.txt. Recommended maximum number of fastqc folders is 50. This script works with older and newer versions of FastQC, but won't work with a mix of old and new version FastQC output files.

Note: This script doesn't work will if FastQC is run with the --nogroup option

Options:
    -s percent : Scale the final image by the specified percent (valid range 5-100, default 100). Files larger than 5000 pixels wide are automatically scaled to 5000 pixels wide
    -o file : Save the final image in the specified file (default fasterqc.png)
    -f folder: Folder containing FastQC output files

=head1 EXAMPLE

Consolidate the results from 12 FastQC runs into one tiny image::

    $ cd /home/msistaff/public/garbe/sampledata/RNAseq/Hsap/fastq/fastqc
    $ fasterqc.pl -s 10 -o fasterqc-sample.png

=cut

use Getopt::Std;
use GD;
use File::Temp qw/ tempdir /;
use Pod::Usage;
use File::Basename;

our ($opt_h, $opt_f);

$scratchfolder = tempdir( DIR => "/scratch", CLEANUP => 1) || die "unable to create temporary directory $!\n";; 
#$scratchfolder = tempdir( DIR => "/scratch") || die "unable to create temporary directory $!\n";; 
$plotwidth = 800; # height of a fastqc plot
$plotheight = 600; # width of a fastqc plot
$topmargin = 20; # margin for printing sample name(s) at top of each column
#@oldplots = ("per_base_quality.png", "per_sequence_quality.png", "per_base_sequence_content.png", "per_base_gc_content.png", "per_sequence_gc_content.png", "per_base_n_content.png", "sequence_length_distribution.png", "duplication_levels.png", "kmer_profiles.png"); 
@oldplots = ("per_base_quality.png", "per_sequence_quality.png", "per_base_sequence_content.png", "per_base_gc_content.png", "per_sequence_gc_content.png", "per_base_n_content.png", "duplication_levels.png"); 
#@newplots = ("per_base_quality.png", "per_tile_quality.png", "per_sequence_quality.png", "per_base_sequence_content.png", "per_sequence_gc_content.png", "per_base_n_content.png", "sequence_length_distribution.png", "duplication_levels.png", "adapter_content.png", "kmer_profiles.png"); 
@newplots = ("per_base_quality.png", "per_sequence_quality.png", "per_base_sequence_content.png", "per_sequence_gc_content.png", "per_base_n_content.png", "duplication_levels.png", "adapter_content.png"); 

# get parameters
$opt_s = 100;
$opt_o = "fasterqc.png";
$ok = getopts('s:o:hf:');
pod2usage(q(-verbose) => 3) if ($opt_h);
$filename = $opt_o;
$opt_s = 100 if ($opt_s > 100);
$opt_s = 5 if ($opt_s < 5);
print "Scaling to $opt_s percent\n" if ($opt_s < 100);
$toobig = 5000; # maximum width of final image
$fastqfolder = $opt_f // ".";

die "USAGE: fasterqc.pl [-s PERCENT] [-o FILENAME] [-f folder]\n" unless (($#ARGV == -1) && $ok);

# get list of fastqc output zips, uncompress in scratch folder
@zips = <$fastqfolder/*_fastqc.zip>;
die "no *_fastqc.zip output files found\n"if ($#zips < 0);
#print "Unzipping fastqc output files to temporary directory $scratchfolder\n";
foreach $zip (@zips) {
    `unzip $zip -d $scratchfolder`;
}
@samples = <$scratchfolder/*_fastqc>;
die "no *_fastqc.zip output files found\n"if ($#samples < 0);
$numsamples = $#samples + 1;
print STDERR "$numsamples *_fastqc.zip output files found, which is more than the maximum recommended number of 50, the output image may be difficult to read\n" if ($numsamples > 50);

# determine FastQC version:
if (-e "$samples[0]/Images/per_base_gc_content.png") { # old style
    @plots = @oldplots; 
} elsif (-e "$samples[0]/Images/adapter_content.png") { # new style
    @plots = @newplots;
} else {
    print `ls $samples[0]/Images/per_base_gc_content.png`;
    print `ls $samples[0]/Images/per_sequence_gc_content.png`;
    die "Cannot determine version of FastQC used\n";
}
 
# create composite image
$compimage = GD::Image->new($plotwidth*($#samples+1),$plotheight*($#plots+1)+$topmargin);
$white = $compimage->colorAllocate(255,255,255);
$black = $compimage->colorAllocate(0,0,0);
$compimage->filledRectangle(0,0,$plotwidth*($#samples+1),$plotheight*($#plots+1)+$topmargin,$white);

# read in each plot from each sample and add to composite
for $i (0..$#samples) { # columns
    print STDERR "Processing sample $samples[$i]\n";
    for $j (0..$#plots) { # rows
	$file = $samples[$i] . "/Images/" . $plots[$j];
	if (-e $file) {
	    $image = GD::Image->newFromPng($file);
	    $compimage->copy($image,$i*$plotwidth,$j*$plotheight+$topmargin,0,0,$plotwidth,$plotwidth);
	} else {
	    print STDERR "File $file does not exist\n";
	}
    }
    $samplename = basename($samples[$i]);
    $samplename =~ s/_fastqc//;
    @array = $compimage->stringFT($black,"/usr/share/fonts/liberation/LiberationSans-Regular.ttf",28,0,$i*$plotwidth + $plotwidth/2 - 380, 31, $samplename);
    if ($#array < 0) { # fallback method for printing sample name
	$compimage->string(gdGiantFont,$i*$plotwidth + $plotwidth/2 - 100, 3, $samplename, $black);
    }
}

# scale image
$oldw = $plotwidth*($#samples+1);
$oldh = $plotheight*($#plots+1)+$topmargin;
# if the image is too big autoscale it
if (($opt_s == 100) and ($oldw > $toobig)) {
    $opt_s = int(($toobig / $oldw) * 100);
    print "Autoscaling by $opt_s percent\n";
}
if ($opt_s < 100) {
    $neww = $oldw * ($opt_s / 100);
    $newh = $oldh * ($opt_s / 100);
    print STDERR "original dimensions: $oldw $oldh\n";
    print STDERR "Scaled dimensions: $neww $newh\n";

    $newcompimage = newTrueColor GD::Image($neww, $newh);
    $newcompimage->copyResampled($compimage,0,0,0,0,$neww,$newh,$oldw,$oldh);
    $compimage = $newcompimage;
}

# print out image
open my $fh,">","$filename" or die "$!";
print $fh $compimage->png;
close($fh);

# print out overrepresented sequences for each sample
open OFILE, ">fasterqc.overrep.txt" or die "Cannot open file fasterqc.overrep.txt, overrepresented sequences will not be printed: $!\n";
print OFILE "### Overrepresented Sequences ###\n";
for $i (0..$#samples) { # columns
    $file = $samples[$i] . "/fastqc_data.txt";
    $output = `sed -n '/Overrepresented sequences/,/END_MODULE/p' $file`;
    print OFILE "Sample: $samples[$i]\n";
    print OFILE $output;
}

exit;
