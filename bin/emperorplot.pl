#!/usr/bin/perl -w

$name = "emperorplot";
$ofile = "$name.dat";
$rfile = "$name.rmd";
open OFILE, ">$ofile" or die "cannot open temporary file $ofile for writing: $!\n"; 

die "USAGE: emperorplot.pl ordination.txt [samplesheet.txt]\n" if ($#ARGV < 0);
$ifile = shift @ARGV;
if (defined($ARGV[0])) {
    $sfile = shift @ARGV;
} else {
    $sfile = "";
}

# read in samplesheet
%metacats = ();
if ($sfile) {
    open IFILE, $sfile or die "Cannot open samplesheet $sfile: $!\n";
    $header = <IFILE>;
    chomp $header;
    @header = split /\t/, lc($header);
    while ($line = <IFILE>) {
	chomp $line;
	@line = split /\t/, $line;
	$sample = $line[0];
	for $i (1..$#header) {
	    next if ($header[$i] eq "fastqr1"); 
	    next if ($header[$i] eq "fastqr2"); 
	    next if ($header[$i] eq "description"); 
	    $value = $line[$i] // "undef";
	    $value = "undef" if ($value eq "");
	    $metadata{$sample}{$header[$i]} = $value;
	    $metacats{$header[$i]}{$value} = 1;
	}
    }
    close IFILE;
}

### read in data
if ($ifile =~ /\.qza$/) {
    `module load qiime2/2019.1; qiime tools export --input-path $ifile --output-path .`;
    $ifile = "ordination.txt";
}
open IFILE, $ifile or die "cannot open $ifile: $!\n";

$line = <IFILE>; # eigenvals
$line = <IFILE>; # eigenvals data
$line = <IFILE>; # space

$line = <IFILE>; # proportion explained
$line = <IFILE>; # proportion explained data
chomp $line;
@explained = split /\t/, $line;
for $i (0..$#explained) {
    $explained[$i] = round100($explained[$i]) * 100;
    $explained[$i] = "$explained[$i]%";
}
$line = <IFILE>; # space

$line = <IFILE>; # species
$line = <IFILE>; # space
$line = <IFILE>; # site

### write out data while reading in rest of data
print OFILE "sample\tpca1\tpca2\tpca3";
foreach $key (sort keys %metacats) {
    print OFILE "\t$key";
}
print OFILE "\n";
while ($line = <IFILE>) {
    chomp $line;
    last if ($line eq "");
    @line = split /\t/, $line;
    $sample = shift @line;
    push @samples, $sample;
    print OFILE "$sample\t$line[0]\t$line[1]\t$line[2]";
    foreach $key (sort keys %metacats) {
	if (! defined($metadata{$sample}{$key})) {
	    print "no metadata for $sample and $key\n";
	}
	print OFILE "\t$metadata{$sample}{$key}";
    }
    print OFILE "\n";    
}
close OFILE;

# generate metadata color lists
@colors = ('#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'); # standard plotly color set
foreach $cat (sort keys %metacats) {
    @levels = sort keys %{$metacats{$cat}};
    for $i (0..$#levels) {
	$imod = $i % 10;
	$colors{$cat}{$levels[$i]} = $colors[$imod];
    }
    $colorarray{$cat} = "";
    foreach $sample (@samples) {
	$colorarray{$cat} .= "\"$colors{$cat}{$metadata{$sample}{$cat}}\",";
    }
    chop $colorarray{$cat};
}

if ($#samples > 20) {
    $type = "marker";
    $typename = "markers";
} else {
    $type = "textfont";
    $typename = "text";
}

$buttonlist = "";
if (keys %metacats) {
    $colorarray = "";
    foreach $sample (@samples) {
	$colorarray .= "\"#1f77b4\","; # blue
    }
    chop $colorarray;
    $buttonlist = "list(label = \"Color by:<br>None\", method = \"restyle\", args = list(\"$type.color\", list($colorarray))),";
}
foreach $cat (sort keys %metacats) {
    $buttonlist .= "list(label = \"Color by:<br>$cat\", method = \"restyle\", args = list(\"$type.color\", list($colorarray{$cat}))),";
}
chop $buttonlist;

### generate pca plot
open RFILE, ">$rfile" or die "Cannot open $rfile\n";
print RFILE qq(
<div class="plottitle">PCoA plot
<a href="#$name" class="icon" data-toggle="collapse"><i class="fa fa-question-circle" aria-hidden="true" style="color:grey;font-size:75%;"></i></a></div>
<div id="$name" class="collapse">
This is a Principal Coordinates Analysis (PCoA) plot, generated from a weighted UniFrac distance matrix. The first three principal components are shown, and the percent of total variation explained by each component is shown in the axis titles. Samples with similar biological communities appear close to each other, and samples with dissimilar communities are farther apart. Ideally samples will cluster by experimental condition and not by batch or other technical effects. This metric is calculated by Qiime2.
</div>

```{r $name, out.height=500, warning=FALSE}

library(plotly)

datat <- read.table("$ofile", header=T, colClasses=c(\"sample\"=\"factor\"));

updatemenus <- list(
  list(
    x = 1,
    y = 1,
    xanchor = 'right',
    yanchor = 'top',
    font = list(size = 10),
    buttons = list(
      $buttonlist
    )
  )
)

config(plot_ly(
  height = 500,
  data=datat,
  x = ~pca1,
  y = ~pca2,
  z = ~pca3,
 type = "scatter3d",
 mode = '$typename', 
 color = ~sample,
$type = list(color = c($colorarray)),
hoverinfo='text', text=paste0(datat\$sample))
%>% layout(updatemenus=updatemenus, scene = list(xaxis = list(title = "Axis 1 ($explained[0])", showspikes=FALSE), yaxis = list(title = "Axis 2 ($explained[1])", showspikes=FALSE), zaxis=list(title="Axis 3 ($explained[2])", showspikes=FALSE)), showlegend = FALSE), dragmode='tableRotation', collaborate = FALSE, displaylogo = FALSE, modeBarButtonsToRemove = list('sendDataToCloud','toImage','autoScale2d','hoverClosest3d','hoverCompareCartesian','lasso2d','zoom3d','select3d','toggleSpikelines','pan3d', 'resetCameraLastSave3d', 'orbitRotation', 'tableRotation', 'resetCameraDefault3d'))
```
);
close RFILE;



# Round to neaerest hundreth
sub round100 {
    return sprintf("%.2f", $_[0]);
#    return int($_[0] * 100 + 0.5) / 100;
}

